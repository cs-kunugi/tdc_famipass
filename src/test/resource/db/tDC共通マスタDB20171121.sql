
/* Drop Indexes */

DROP INDEX haiken_ix_show_code;
DROP INDEX haiken_ix_t_entry_code;
DROP INDEX haiken_sales_player_ix_haiken_code;
DROP INDEX haiken_seat_ix_haiken_code;
DROP INDEX haiken_seat_ix_show_seat_code;
DROP INDEX organizer_sales_player_ix_sales_player_code;
DROP INDEX organizer_sales_player_ix_organizer_code;
DROP INDEX place_block_ix_place_code;
DROP INDEX place_block_ix_place_layer_code;
DROP INDEX place_block_info_ix_place_code;
DROP INDEX place_join_group_ix_place_code;
DROP INDEX place_layer_ix_place_code;
DROP INDEX place_mst_ix_pref_code;
DROP INDEX place_object_ix_place_code;
DROP INDEX place_object_ix_place_layer_code;
DROP INDEX place_seat_ix_place_block_code;
DROP INDEX place_seat_info_ix_place_code;
DROP INDEX place_seat_info_ix_place_join_group_code;
DROP INDEX seat_variation_mst_ix_t_content_code;
DROP INDEX seat_variation_mst_ix_show_code;
DROP INDEX series_mst_ix_genre_code;
DROP INDEX show_block_ix_show_code;
DROP INDEX show_block_ix_show_layer_code;
DROP INDEX show_block_info_ix_show_code;
DROP INDEX show_group_mst_ix_show_code;
DROP INDEX show_join_group_ix_show_code;
DROP INDEX show_layer_ix_show_code;
DROP INDEX show_mst_ix_place_code;
DROP INDEX show_mst_ix_series_code;
DROP INDEX show_object_ix_show_code;
DROP INDEX show_object_ix_show_layer_code;
DROP INDEX show_seat_ix_show_code;
DROP INDEX show_seat_info_ix_show_code;
DROP INDEX system_role_setting_ix_system_role_code;
DROP INDEX system_role_setting_ix_system_role_setting_code;
DROP INDEX system_role_setting_mst_ix_system_user_code;
DROP INDEX system_user_role_ix_system_role_code;
DROP INDEX system_user_role_ix_system_user_code;
DROP INDEX ticket_set_details_ix_t_content_code;
DROP INDEX ticket_set_details_ix_show_code;
DROP INDEX ticket_set_details_ix_ticket_set_code;
DROP INDEX ticket_set_details_ix_seat_variation_code;
DROP INDEX ticket_set_details_ix_t_variation_code;
DROP INDEX ticket_set_mst_ix_series_code;
DROP INDEX t_content_layout_ix_t_content_code;
DROP INDEX t_content_template_layout_ix_t_content_template_code;
DROP INDEX t_entry_mst_ix_series_code;
DROP INDEX t_entry_receive_way_ix_receive_way_code;
DROP INDEX t_entry_receive_way_ix_t_entry_code;
DROP INDEX t_entry_sales_player_ix_sales_player_code;
DROP INDEX t_entry_sales_player_ix_t_entry_code;
DROP INDEX t_entry_settlement_way_ix_settlement_way_code;
DROP INDEX t_entry_settlement_way_ix_t_entry_code;
DROP INDEX t_entry_show_ix_show_code;
DROP INDEX t_entry_show_ix_t_entry_code;
DROP INDEX t_entry_t_variation_ix_seat_variation_code;
DROP INDEX t_entry_t_variation_ix_t_variation_code;
DROP INDEX t_entry_t_variation_ix_t_entry_code;
DROP INDEX t_stub_details_ix_t_stub_code;
DROP INDEX t_stub_details_ix_t_content_code;
DROP INDEX t_variation_mst_ix_t_stub_code;
DROP INDEX t_variation_mst_ix_t_content_code;
DROP INDEX t_variation_mst_ix_show_code;
DROP INDEX t_variation_mst_ix_seat_variation_code;



/* Drop Triggers */

DROP TRIGGER TRI_artist_mst_artist_code;
DROP TRIGGER TRI_genre_mst_genre_code;
DROP TRIGGER TRI_haiken_haiken_code;
DROP TRIGGER TRI_organizer_mst_organizer_code;
DROP TRIGGER TRI_organizer_sales_player_organizer_sales_player_code;
DROP TRIGGER TRI_place_block_place_block_code;
DROP TRIGGER TRI_place_join_group_place_join_group_code;
DROP TRIGGER TRI_place_mst_place_code;
DROP TRIGGER TRI_place_object_place_object_code;
DROP TRIGGER TRI_place_object_template_mst_place_object_template_code;
DROP TRIGGER TRI_pref_mst_pref_code;
DROP TRIGGER TRI_receive_way_mst_receive_way_code;
DROP TRIGGER TRI_remote_system_auth_log_remote_system_auth_log_id;
DROP TRIGGER TRI_remote_system_mst_remote_system_code;
DROP TRIGGER TRI_sales_player_mst_sales_player_code;
DROP TRIGGER TRI_seat_variation_mst_seat_variation_code;
DROP TRIGGER TRI_series_mst_series_code;
DROP TRIGGER TRI_settlement_way_mst_settlement_way_code;
DROP TRIGGER TRI_show_block_show_block_code;
DROP TRIGGER TRI_show_group_mst_show_group_code;
DROP TRIGGER TRI_show_join_group_show_join_group_code;
DROP TRIGGER TRI_show_layer_show_layer_code;
DROP TRIGGER TRI_show_mst_show_code;
DROP TRIGGER TRI_show_object_show_object_code;
DROP TRIGGER TRI_system_role_mst_system_role_code;
DROP TRIGGER TRI_system_user_mst_system_user_code;
DROP TRIGGER TRI_system_user_role_system_user_role_code;
DROP TRIGGER TRI_ticket_set_details_ticket_set_details_code;
DROP TRIGGER TRI_ticket_set_mst_ticket_set_code;
DROP TRIGGER TRI_t_content_layout_t_content_layout_code;
DROP TRIGGER TRI_t_content_mst_t_content_code;
DROP TRIGGER TRI_t_content_template_layout_t_content_template_layout_code;
DROP TRIGGER TRI_t_content_template_mst_t_content_template_code;
DROP TRIGGER TRI_t_entry_mst_t_entry_code;
DROP TRIGGER TRI_t_stub_details_t_stub_details_code;
DROP TRIGGER TRI_t_stub_mst_t_stub_code;
DROP TRIGGER TRI_t_variation_mst_t_variation_code;



/* Drop Tables */

DROP TABLE place_seat CASCADE CONSTRAINTS;
DROP TABLE place_seat_info CASCADE CONSTRAINTS;
DROP TABLE area_mst CASCADE CONSTRAINTS;
DROP TABLE show_artist CASCADE CONSTRAINTS;
DROP TABLE artist_mst CASCADE CONSTRAINTS;
DROP TABLE block_mst CASCADE CONSTRAINTS;
DROP TABLE show_genre CASCADE CONSTRAINTS;
DROP TABLE genre_sub_mst CASCADE CONSTRAINTS;
DROP TABLE genre_mst CASCADE CONSTRAINTS;
DROP TABLE haiken_content CASCADE CONSTRAINTS;
DROP TABLE haiken_sales_player CASCADE CONSTRAINTS;
DROP TABLE haiken_seat CASCADE CONSTRAINTS;
DROP TABLE lottery_count_info CASCADE CONSTRAINTS;
DROP TABLE lottery_hit_count_info CASCADE CONSTRAINTS;
DROP TABLE haiken CASCADE CONSTRAINTS;
DROP TABLE organizer_sales_player CASCADE CONSTRAINTS;
DROP TABLE organizer_mst CASCADE CONSTRAINTS;
DROP TABLE place_block_info CASCADE CONSTRAINTS;
DROP TABLE place_join_group CASCADE CONSTRAINTS;
DROP TABLE place_block CASCADE CONSTRAINTS;
DROP TABLE place_object CASCADE CONSTRAINTS;
DROP TABLE place_layer CASCADE CONSTRAINTS;
DROP TABLE show_seat CASCADE CONSTRAINTS;
DROP TABLE show_seat_info CASCADE CONSTRAINTS;
DROP TABLE ticket_set_details CASCADE CONSTRAINTS;
DROP TABLE t_entry_t_variation CASCADE CONSTRAINTS;
DROP TABLE t_variation_mst CASCADE CONSTRAINTS;
DROP TABLE seat_variation_mst CASCADE CONSTRAINTS;
DROP TABLE show_group_mst CASCADE CONSTRAINTS;
DROP TABLE t_entry_show CASCADE CONSTRAINTS;
DROP TABLE show_mst CASCADE CONSTRAINTS;
DROP TABLE place_mst CASCADE CONSTRAINTS;
DROP TABLE place_object_template_info CASCADE CONSTRAINTS;
DROP TABLE place_object_template_mst CASCADE CONSTRAINTS;
DROP TABLE pref_mst CASCADE CONSTRAINTS;
DROP TABLE t_entry_receive_way CASCADE CONSTRAINTS;
DROP TABLE receive_way_mst CASCADE CONSTRAINTS;
DROP TABLE remote_system_auth_log CASCADE CONSTRAINTS;
DROP TABLE remote_system_mst CASCADE CONSTRAINTS;
DROP TABLE t_entry_sales_player CASCADE CONSTRAINTS;
DROP TABLE sales_player_mst CASCADE CONSTRAINTS;
DROP TABLE t_entry_settlement_way CASCADE CONSTRAINTS;
DROP TABLE t_entry_mst CASCADE CONSTRAINTS;
DROP TABLE series_mst CASCADE CONSTRAINTS;
DROP TABLE settlement_way_mst CASCADE CONSTRAINTS;
DROP TABLE show_block_info CASCADE CONSTRAINTS;
DROP TABLE show_join_group CASCADE CONSTRAINTS;
DROP TABLE show_block CASCADE CONSTRAINTS;
DROP TABLE show_object CASCADE CONSTRAINTS;
DROP TABLE show_layer CASCADE CONSTRAINTS;
DROP TABLE system_role_setting CASCADE CONSTRAINTS;
DROP TABLE system_user_role CASCADE CONSTRAINTS;
DROP TABLE system_role_mst CASCADE CONSTRAINTS;
DROP TABLE system_user_group_mst CASCADE CONSTRAINTS;
DROP TABLE system_user_auth_log CASCADE CONSTRAINTS;
DROP TABLE system_user_mst CASCADE CONSTRAINTS;
DROP TABLE system_role_setting_mst CASCADE CONSTRAINTS;
DROP TABLE ticket_set_mst CASCADE CONSTRAINTS;
DROP TABLE t_content_layout CASCADE CONSTRAINTS;
DROP TABLE t_content_mst CASCADE CONSTRAINTS;
DROP TABLE t_content_template_layout CASCADE CONSTRAINTS;
DROP TABLE t_content_template_mst CASCADE CONSTRAINTS;
DROP TABLE t_stub_details CASCADE CONSTRAINTS;
DROP TABLE t_stub_mst CASCADE CONSTRAINTS;



/* Drop Sequences */

DROP SEQUENCE SEQ_artist_mst_artist_code;
DROP SEQUENCE SEQ_genre_mst_genre_code;
DROP SEQUENCE SEQ_haiken_haiken_code;
DROP SEQUENCE SEQ_organizer_mst_organizer_code;
DROP SEQUENCE SEQ_organizer_sales_player_organizer_sales_player_code;
DROP SEQUENCE SEQ_place_block_place_block_code;
DROP SEQUENCE SEQ_place_join_group_place_join_group_code;
DROP SEQUENCE SEQ_place_mst_place_code;
DROP SEQUENCE SEQ_place_object_place_object_code;
DROP SEQUENCE SEQ_place_object_template_mst_place_object_template_code;
DROP SEQUENCE SEQ_pref_mst_pref_code;
DROP SEQUENCE SEQ_receive_way_mst_receive_way_code;
DROP SEQUENCE SEQ_remote_system_auth_log_remote_system_auth_log_id;
DROP SEQUENCE SEQ_remote_system_mst_remote_system_code;
DROP SEQUENCE SEQ_sales_player_mst_sales_player_code;
DROP SEQUENCE SEQ_seat_variation_mst_seat_variation_code;
DROP SEQUENCE SEQ_series_mst_series_code;
DROP SEQUENCE SEQ_settlement_way_mst_settlement_way_code;
DROP SEQUENCE SEQ_show_block_show_block_code;
DROP SEQUENCE SEQ_show_group_mst_show_group_code;
DROP SEQUENCE SEQ_show_join_group_show_join_group_code;
DROP SEQUENCE SEQ_show_layer_show_layer_code;
DROP SEQUENCE SEQ_show_mst_show_code;
DROP SEQUENCE SEQ_show_object_show_object_code;
DROP SEQUENCE SEQ_system_role_mst_system_role_code;
DROP SEQUENCE SEQ_system_user_mst_system_user_code;
DROP SEQUENCE SEQ_system_user_role_system_user_role_code;
DROP SEQUENCE SEQ_ticket_set_details_ticket_set_details_code;
DROP SEQUENCE SEQ_ticket_set_mst_ticket_set_code;
DROP SEQUENCE SEQ_t_content_layout_t_content_layout_code;
DROP SEQUENCE SEQ_t_content_mst_t_content_code;
DROP SEQUENCE SEQ_t_content_template_layout_t_content_template_layout_code;
DROP SEQUENCE SEQ_t_content_template_mst_t_content_template_code;
DROP SEQUENCE SEQ_t_entry_mst_t_entry_code;
DROP SEQUENCE SEQ_t_stub_details_t_stub_details_code;
DROP SEQUENCE SEQ_t_stub_mst_t_stub_code;
DROP SEQUENCE SEQ_t_variation_mst_t_variation_code;




/* Create Sequences */

CREATE SEQUENCE SEQ_artist_mst_artist_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_genre_mst_genre_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_haiken_haiken_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_organizer_mst_organizer_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_organizer_sales_player_organizer_sales_player_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_place_block_place_block_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_place_join_group_place_join_group_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_place_mst_place_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_place_object_place_object_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_place_object_template_mst_place_object_template_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_pref_mst_pref_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_receive_way_mst_receive_way_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_remote_system_auth_log_remote_system_auth_log_id INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_remote_system_mst_remote_system_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_sales_player_mst_sales_player_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_seat_variation_mst_seat_variation_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_series_mst_series_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_settlement_way_mst_settlement_way_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_show_block_show_block_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_show_group_mst_show_group_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_show_join_group_show_join_group_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_show_layer_show_layer_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_show_mst_show_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_show_object_show_object_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_system_role_mst_system_role_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_system_user_mst_system_user_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_system_user_role_system_user_role_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_ticket_set_details_ticket_set_details_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_ticket_set_mst_ticket_set_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_t_content_layout_t_content_layout_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_t_content_mst_t_content_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_t_content_template_layout_t_content_template_layout_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_t_content_template_mst_t_content_template_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_t_entry_mst_t_entry_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_t_stub_details_t_stub_details_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_t_stub_mst_t_stub_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_t_variation_mst_t_variation_code INCREMENT BY 1 START WITH 1;



/* Create Tables */

CREATE TABLE area_mst
(
	area_code number(19,0) NOT NULL,
	area_name varchar2(200),
	-- 削除フラグ
	deleted number(1),
	PRIMARY KEY (area_code)
);


CREATE TABLE artist_mst
(
	-- アーティストシステムコード
	artist_code number(19,0) NOT NULL,
	-- アーティストシステムID
	artist_id varchar2(20 char) NOT NULL,
	-- アーティスト名
	artist_name nvarchar2(200) NOT NULL,
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT artist_mst_pkc PRIMARY KEY (artist_code)
);


CREATE TABLE block_mst
(
	block_code number(19,0) NOT NULL,
	block_name nvarchar2(200),
	-- 削除フラグ
	deleted number(1),
	PRIMARY KEY (block_code)
);


CREATE TABLE genre_mst
(
	-- ジャンルシステムコード
	genre_code number(19,0) NOT NULL,
	-- ジャンルシステムID
	genre_id varchar2(20 char),
	-- ジャンル名
	genre_name nvarchar2(200),
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT genre_mst_pkc PRIMARY KEY (genre_code)
);


CREATE TABLE genre_sub_mst
(
	-- ジャンルシステムコード
	genre_sub_code number(19,0) NOT NULL,
	-- ジャンルシステムID
	genre_sub_id varchar2(20),
	-- ジャンルシステムコード
	genre_code number(19,0) NOT NULL,
	-- ジャンル名
	genre_sub_name nvarchar2(200),
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT genre_sub_mst_pkc PRIMARY KEY (genre_sub_code)
);


CREATE TABLE haiken
(
	-- 配券システムコード
	haiken_code number(19,0) NOT NULL,
	-- 配券ID
	haiken_id varchar2(20 char),
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 窓口販売設定
	box_office_sales_setting number(1) DEFAULT 0 NOT NULL,
	-- 配券種別
	haiken_type number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT haiken_pkc PRIMARY KEY (haiken_code)
);


CREATE TABLE haiken_content
(
	-- 配券システムコード
	haiken_code number(19,0) DEFAULT 0 NOT NULL,
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演座席システムコード
	show_seat_code number(19,0) DEFAULT 0 NOT NULL,
	-- 席種システムコード
	seat_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- 券種システムコード
	t_variation_code number(19,0) DEFAULT 0 NOT NULL,
	area_code number(19,0) DEFAULT 0 NOT NULL,
	block_code number(19,0) DEFAULT 0 NOT NULL,
	ticket_price number(10,0) DEFAULT 0 NOT NULL,
	ticket_count number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT haiken_content_pkc PRIMARY KEY (haiken_code, t_entry_code, show_seat_code, seat_variation_code, t_variation_code, area_code, block_code)
);


CREATE TABLE haiken_sales_player
(
	-- 配券システムコード
	haiken_code number(19,0) DEFAULT 0 NOT NULL,
	-- 販売事業者システムコード
	sales_player_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT haiken_sales_player_pkc PRIMARY KEY (haiken_code, sales_player_code)
);


CREATE TABLE haiken_seat
(
	-- 配券システムコード
	haiken_code number(19,0) DEFAULT 0 NOT NULL,
	-- 席種システムコード
	seat_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演座席コード
	show_seat_code number(19,0) DEFAULT 0 NOT NULL,
	-- 枚数
	count number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT haiken_seat_pkc PRIMARY KEY (haiken_code, show_seat_code)
);


CREATE TABLE lottery_count_info
(
	-- 配券システムコード
	haiken_code number(19,0) DEFAULT 0 NOT NULL,
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 販売事業者システムコード
	sales_player_code number(19,0) DEFAULT 0 NOT NULL,
	lottery_count number(10,0) DEFAULT 0 NOT NULL,
	lottery_hit_setting_count number(10,0) DEFAULT 0 NOT NULL,
	lottery_hit_confirm_count number(10,0) DEFAULT 0 NOT NULL,
	PRIMARY KEY (haiken_code, t_entry_code, show_code, sales_player_code)
);


CREATE TABLE lottery_hit_count_info
(
	-- 配券システムコード
	haiken_code number(19,0) DEFAULT 0 NOT NULL,
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 販売事業者システムコード
	sales_player_code number(19,0) DEFAULT 0 NOT NULL,
	set_count number(10,0) DEFAULT 0 NOT NULL,
	set_count_kensu number(10,0) DEFAULT 0 NOT NULL,
	PRIMARY KEY (haiken_code, t_entry_code, show_code, sales_player_code)
);


CREATE TABLE organizer_mst
(
	-- 興行主システムコード
	organizer_code number(19,0) NOT NULL,
	-- 興行主ID
	organizer_id varchar2(20 char),
	-- 興行主名
	organizer_name nvarchar2(200),
	CONSTRAINT organizer_mst_pkc PRIMARY KEY (organizer_code)
);


CREATE TABLE organizer_sales_player
(
	-- 興行主利用販売事業者システムコード
	organizer_sales_player_code number(19,0) NOT NULL,
	-- 興行主利用販売事業者システムID
	organizer_sales_player_id varchar2(20 char),
	-- 興行主システムコード
	organizer_code number(19,0) DEFAULT 0 NOT NULL,
	-- 販売事業者コード
	sales_player_code number(19,0) DEFAULT 0 NOT NULL,
	-- 表示文字
	display_char nvarchar2(20),
	-- 表示色
	display_color varchar2(100 char),
	CONSTRAINT organizer_sales_player_pkc PRIMARY KEY (organizer_sales_player_code)
);


CREATE TABLE place_block
(
	-- 会場システムコード
	place_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場レイヤーシステムコード
	place_layer_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場ブロックシステムコード
	place_block_code number(19,0) NOT NULL,
	-- 会場ブロック名
	place_block_name nvarchar2(200),
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	-- 会場ブロック優先順位
	priority number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT place_block_pkc PRIMARY KEY (place_block_code)
);


CREATE TABLE place_block_info
(
	-- 会場システムコード
	place_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場ブロックシステムコード
	place_block_code number(19,0) DEFAULT 0 NOT NULL,
	-- 親ブロックシステムコード
	parent_block_code number(19,0) DEFAULT 0 NOT NULL,
	-- 親ブロック座標X1
	parent_block_disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 親ブロック座標Y1
	parent_block_disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 親ブロック座標X2
	parent_block_disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 親ブロック座標Y2
	parent_block_disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT place_block_info_pkc PRIMARY KEY (place_block_code)
);


CREATE TABLE place_join_group
(
	-- 会場システムコード
	place_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場ブロックシステムコード
	place_block_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場結合グループシステムコード
	place_join_group_code number(19,0) NOT NULL,
	-- 座標X1
	disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y1
	disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X2
	disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y2
	disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT place_join_group_pkc PRIMARY KEY (place_join_group_code)
);


CREATE TABLE place_layer
(
	-- 会場システムコード
	place_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場レイヤーシステムコード
	place_layer_code number(19,0) NOT NULL,
	-- 会場レイヤー名
	place_layer_name nvarchar2(200),
	-- 選択可否
	select_flg number(1) DEFAULT 0 NOT NULL,
	-- 会場ブロック優先順位
	zoom_level number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT place_layer_pkc PRIMARY KEY (place_layer_code)
);


CREATE TABLE place_mst
(
	-- 会場システムコード
	place_code number(19,0) NOT NULL,
	-- 会場ID
	place_id varchar2(20 char),
	-- 会場名
	place_name nvarchar2(200),
	-- 会場名（印刷用）
	place_name_print nvarchar2(200),
	-- 郵便番号
	zip varchar2(10 char) DEFAULT '0' NOT NULL,
	-- 市区町村
	city nvarchar2(200),
	-- 建物名
	building nvarchar2(200),
	-- 電話番号
	tel nvarchar2(20),
	-- 緯度
	latitude nvarchar2(20),
	-- 経度
	longitude nvarchar2(20),
	-- 都道府県システムコード
	pref_code number(19,0) NOT NULL,
	-- 番地
	address nvarchar2(200),
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT place_mst_pkc PRIMARY KEY (place_code)
);


CREATE TABLE place_object
(
	-- 会場システムコード
	place_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場レイヤーシステムコード
	place_layer_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場オブジェクトシステムコード
	place_object_code number(19,0) NOT NULL,
	-- オブジェクト種別
	object_type number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X1
	disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y1
	disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X2
	disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y2
	disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT place_object_pkc PRIMARY KEY (place_object_code)
);


CREATE TABLE place_object_template_info
(
	-- 会場オブジェクトテンプレートシステムコード
	place_object_template_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場オブジェクトテンプレート部品システムコード
	parts_place_object_template_code number(19,0) DEFAULT 0 NOT NULL,
	-- オブジェクト種別
	object_type number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X1
	disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y1
	disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X2
	disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y2
	disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT place_object_template_info_pkc PRIMARY KEY (place_object_template_code)
);


CREATE TABLE place_object_template_mst
(
	-- 会場オブジェクトテンプレートシステムコード
	place_object_template_code number(19,0) NOT NULL,
	-- 会場オブジェクトテンプレート名
	place_object_template_name nvarchar2(200),
	-- テンプレート種別
	template_type number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT place_object_template_mst_pkc PRIMARY KEY (place_object_template_code)
);


CREATE TABLE place_seat
(
	-- 会場システムコード
	place_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場座席システムコード
	place_seat_code number(19,0) NOT NULL,
	-- 会場ブロックシステムコード
	place_block_code number(19,0) NOT NULL,
	-- 表示タイプ
	display_type number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X1
	disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y1
	disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X2
	disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y2
	disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT place_seat_pkc PRIMARY KEY (place_seat_code)
);


CREATE TABLE place_seat_info
(
	-- 会場システムコード
	place_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場座席システムコード
	place_seat_code number(19,0) DEFAULT 0 NOT NULL,
	-- 結合グループシステムコード
	place_join_group_code number(19,0) NOT NULL,
	-- 階
	seat_floor nvarchar2(20),
	-- 列
	seat_row nvarchar2(20),
	-- 番
	seat_number nvarchar2(20),
	-- 整理券番号
	numbered_ticket_number nvarchar2(20),
	-- エリア
	show_area clob,
	-- ブロック
	show_block clob,
	-- その他座席属性1
	other_attribute1 nvarchar2(20),
	-- その他座席属性2
	other_attribute2 nvarchar2(20),
	-- その他座席属性3
	other_attribute3 nvarchar2(20),
	-- その他座席属性4
	other_attribute4 nvarchar2(20),
	-- その他座席属性5
	other_attribute5 nvarchar2(20),
	-- 連席番号
	seat_line_number number(10,0) DEFAULT 0 NOT NULL,
	area_code number(19,0) NOT NULL,
	block_code number(19,0) NOT NULL,
	CONSTRAINT place_seat_info_pkc PRIMARY KEY (place_seat_code)
);


CREATE TABLE pref_mst
(
	-- 都道府県システムコード
	pref_code number(19,0) NOT NULL,
	-- 都道府県システムID
	pref_id varchar2(20 char),
	-- 都道府県名称
	pref_name nvarchar2(10),
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT pref_mst_pkc PRIMARY KEY (pref_code)
);


CREATE TABLE receive_way_mst
(
	-- 受取方法システムコード
	receive_way_code number(19,0) NOT NULL,
	-- 受取方法ID
	receive_way_id varchar2(20 char),
	-- 受取方法名称
	receive_way_name nvarchar2(200),
	CONSTRAINT receive_way_mst_pkc PRIMARY KEY (receive_way_code)
);


CREATE TABLE remote_system_auth_log
(
	-- 接続元認証ログID
	remote_system_auth_log_id number(19,0) NOT NULL,
	-- 接続元コード
	remote_system_code number(19,0),
	-- トークンキー
	token_key varchar2(100 char),
	-- 認証日時
	auth_date date,
	-- 認証結果　失敗:0、成功:1
	auth_result number(10,0) NOT NULL,
	-- 理由
	reason varchar2(300 char),
	ip_address varchar2(39 char),
	-- ユーザーエージェント
	user_agent varchar2(300 char),
	PRIMARY KEY (remote_system_auth_log_id)
);


CREATE TABLE remote_system_mst
(
	-- 接続元コード
	remote_system_code number(19,0) NOT NULL,
	-- 販売事業者システムコード
	sales_player_code number(19,0),
	-- 接続元名
	remote_system_name varchar2(100 char),
	-- トークンキー
	token_key varchar2(100 char),
	-- 1=票券、2=販売ASP、3=プレイガイド
	authority_level number(10,0),
	-- 接続元のIPアドレス。現在は使用していない。
	ip_address varchar2(39 char),
	regstered_at date DEFAULT SYSDATE,
	updated_at date,
	-- 削除日時
	deleted_at date,
	PRIMARY KEY (remote_system_code)
);


CREATE TABLE sales_player_mst
(
	-- 販売事業者システムコード
	sales_player_code number(19,0) NOT NULL,
	-- 販売事業者システムID
	sales_player_id varchar2(20 char),
	-- 販売事業者名
	sales_player_name nvarchar2(200),
	CONSTRAINT sales_player_mst_pkc PRIMARY KEY (sales_player_code)
);


CREATE TABLE seat_variation_mst
(
	-- 席種システムコード
	seat_variation_code number(19,0) NOT NULL,
	-- 席種ID
	seat_variation_id varchar2(20 char),
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 席種名
	seat_variation_name nvarchar2(200),
	-- 印刷用席種名
	seat_variation_name_print nvarchar2(200),
	-- 種別
	seat_variation_type number(10,0),
	-- 席種表示色
	seat_variation_display_color number(19,0) DEFAULT 0 NOT NULL,
	-- 券面システムコード
	t_content_code number(19,0),
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT seat_variation_mst_pkc PRIMARY KEY (seat_variation_code)
);


CREATE TABLE series_mst
(
	-- 興行システムコード
	series_code number(19,0) NOT NULL,
	-- 興行システムID
	series_id varchar2(20 char),
	-- 興行名
	series_name nvarchar2(200),
	-- イベント案内文
	series_info_text nvarchar2(2000),
	-- イベント画像(要検討)
	series_image blob,
	-- 直販サイトURL
	sale_site_url nvarchar2(2000),
	-- ジャンルシステムコード
	genre_code number(19,0),
	-- シーズン年度
	season_fiscal_year number(10,0) DEFAULT 0 NOT NULL,
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT series_mst_pkc PRIMARY KEY (series_code)
);


CREATE TABLE settlement_way_mst
(
	-- 決済方法システムコード
	settlement_way_code number(19,0) NOT NULL,
	-- 決済方法ID
	settlement_way_id varchar2(20 char),
	-- 決済方法名称
	settlement_way_name nvarchar2(200),
	CONSTRAINT settlement_way_mst_pkc PRIMARY KEY (settlement_way_code)
);


CREATE TABLE show_artist
(
	-- アーティストシステムコード
	artist_code number(19,0) NOT NULL,
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	PRIMARY KEY (artist_code, show_code)
);


CREATE TABLE show_block
(
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演レイヤーシステムコード
	show_layer_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演ブロックシステムコード
	show_block_code number(19,0) NOT NULL,
	-- 公演ブロック名
	show_block_name nvarchar2(200),
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	-- 公演ブロック優先順位
	priority number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT show_block_pkc PRIMARY KEY (show_block_code)
);


CREATE TABLE show_block_info
(
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演ブロックシステムコード
	show_block_code number(19,0) DEFAULT 0 NOT NULL,
	-- 親ブロックシステムコード
	parent_block_code number(19,0) DEFAULT 0 NOT NULL,
	-- 親ブロック座標X1
	parent_block_disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 親ブロック座標Y1
	parent_block_disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 親ブロック座標X2
	parent_block_disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 親ブロック座標Y2
	parent_block_disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT show_block_info_pkc PRIMARY KEY (show_block_code)
);


CREATE TABLE show_genre
(
	-- 公演システムコード
	show_code number(19,0) NOT NULL,
	-- ジャンルシステムコード
	genre_code number(19,0) NOT NULL,
	-- ジャンルシステムコード
	genre_sub_code number(19,0) NOT NULL,
	PRIMARY KEY (show_code, genre_code, genre_sub_code)
);


CREATE TABLE show_group_mst
(
	-- 公演グループシステムコード
	show_group_code number(19,0) NOT NULL,
	-- 公演グループID
	show_group_id varchar2(20 char),
	-- 公演グループ名
	show_group_name nvarchar2(200) DEFAULT '0' NOT NULL,
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT show_group_mst_pkc PRIMARY KEY (show_group_code)
);


CREATE TABLE show_join_group
(
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演ブロックシステムコード
	show_block_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演結合グループシステムコード
	show_join_group_code number(19,0) NOT NULL,
	-- 座標X1
	disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y1
	disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X2
	disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y2
	disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT show_join_group_pkc PRIMARY KEY (show_join_group_code)
);


CREATE TABLE show_layer
(
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演レイヤーシステムコード
	show_layer_code number(19,0) NOT NULL,
	-- 公演レイヤー名
	show_layer_name nvarchar2(200),
	-- 選択可否
	select_flg number(1) DEFAULT 0 NOT NULL,
	-- 公演ブロック優先順位
	zoom_level number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT show_layer_pkc PRIMARY KEY (show_layer_code)
);


CREATE TABLE show_mst
(
	-- 公演システムコード
	show_code number(19,0) NOT NULL,
	-- 公演ID
	show_id varchar2(20 char),
	-- 興行システムコード
	series_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会場システムコード
	place_code number(19,0) NOT NULL,
	-- 公演名
	show_name nvarchar2(200),
	-- 公演日付
	show_date date NOT NULL,
	-- 開場日付
	door_open_date date NOT NULL,
	-- 終演日付
	show_end_date date NOT NULL,
	-- 公演案内文
	show_info_str nvarchar2(2000),
	-- 公演問い合わせ先
	show_contact_address nvarchar2(2000),
	-- 直販サイトURL
	sale_site_url nvarchar2(2000),
	-- 公演担当者情報
	show_user_info nvarchar2(2000),
	-- 窓口販売開始日付
	box_office_sale_start_date date NOT NULL,
	-- 窓口販売終了日付
	box_office_sale_end_date date NOT NULL,
	-- 公演日時
	show_time number(10,0) NOT NULL,
	-- 開場時間
	door_open_time number(10,0) NOT NULL,
	-- 終演日時
	show_end_time number(10,0) NOT NULL,
	-- 窓口販売開始時間
	box_office_sale_start_time number(10,0) NOT NULL,
	-- 窓口販売終了時間
	box_office_sale_end_time number(10,0) NOT NULL,
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT show_mst_pkc PRIMARY KEY (show_code)
);


CREATE TABLE show_object
(
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演レイヤーシステムコード
	show_layer_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演オブジェクトシステムコード
	show_object_code number(19,0) NOT NULL,
	-- オブジェクト種別
	object_type number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X1
	disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y1
	disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X2
	disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y2
	disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	-- 角度
	angle number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT show_object_pkc PRIMARY KEY (show_object_code)
);


CREATE TABLE show_seat
(
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演座席システムコード
	show_seat_code number(19,0) NOT NULL,
	-- 公演ブロックシステムコード
	show_block_code number(19,0) DEFAULT 0 NOT NULL,
	-- 表示タイプ
	display_type number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X1
	disp_x1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y1
	disp_y1 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標X2
	disp_x2 number(10,0) DEFAULT 0 NOT NULL,
	-- 座標Y2
	disp_y2 number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT show_seat_pkc PRIMARY KEY (show_seat_code)
);


CREATE TABLE show_seat_info
(
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演座席システムコード
	show_seat_code number(19,0) DEFAULT 0 NOT NULL,
	-- 席種システムコード
	seat_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- 結合グループシステムコード
	show_join_group_code number(19,0) DEFAULT 0 NOT NULL,
	-- 階
	seat_floor nvarchar2(20),
	-- 列
	seat_row nvarchar2(20),
	-- 番
	seat_number nvarchar2(20),
	-- 整理券番号
	numbered_ticket_number nvarchar2(20),
	-- エリア
	show_area clob,
	-- ブロック
	show_block clob,
	-- その他座席属性1
	other_attribute1 nvarchar2(20),
	-- その他座席属性2
	other_attribute2 nvarchar2(20),
	-- その他座席属性3
	other_attribute3 nvarchar2(20),
	-- その他座席属性4
	other_attribute4 nvarchar2(20),
	-- その他座席属性5
	other_attribute5 nvarchar2(20),
	-- 連席番号
	seat_line_number number(10,0) DEFAULT 0 NOT NULL,
	area_code number(19,0),
	block_code number(19,0),
	CONSTRAINT show_seat_info_pkc PRIMARY KEY (show_seat_code)
);


CREATE TABLE system_role_mst
(
	-- システム権限システムコード
	system_role_code number(19,0) NOT NULL,
	-- システム権限ID
	system_role_id varchar2(20 char),
	-- システム権限名
	system_role_name nvarchar2(200),
	CONSTRAINT system_role_mst_pkc PRIMARY KEY (system_role_code)
);


CREATE TABLE system_role_setting
(
	-- system_authority_set_code
	system_role_setting_code number(19,0) DEFAULT 0 NOT NULL,
	-- system_authority_code
	system_role_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT system_role_setting_pkc PRIMARY KEY (system_role_setting_code, system_role_code)
);


CREATE TABLE system_role_setting_mst
(
	-- システム権限設定システムコード
	system_role_setting_code number(19,0) NOT NULL,
	-- system_authority_set_name
	system_role_setting_name nvarchar2(200),
	-- SystemUserMst_SystemUserCode
	system_user_code number(19,0),
	CONSTRAINT system_role_setting_mst_pkc PRIMARY KEY (system_role_setting_code)
);


CREATE TABLE system_user_auth_log
(
	system_user_auth_log_id number(19,0) NOT NULL,
	-- システムユーザーシステムコード
	system_user_code number(19,0) DEFAULT 0 NOT NULL,
	-- 認証日時
	auth_date date DEFAULT SYSDATE,
	-- 失敗:0、成功:1
	auth_result number(10,0) NOT NULL,
	reason varchar2(300),
	-- 接続元のIPアドレス
	ip_address varchar2(39),
	-- ユーザーエージェント
	user_agent varchar2(300 char),
	PRIMARY KEY (system_user_auth_log_id)
);


CREATE TABLE system_user_group_mst
(
	system_user_group_code number(19,0) DEFAULT 0 NOT NULL,
	system_user_group_id varchar2(20),
	system_user_group_name nvarchar2(200),
	-- システム権限設定システムコード
	system_role_setting_code number(19,0) DEFAULT 0 NOT NULL,
	-- 削除フラグ
	deleted number(1),
	PRIMARY KEY (system_user_group_code)
);


CREATE TABLE system_user_mst
(
	-- システム担当者システムコード
	system_user_code number(19,0) NOT NULL,
	-- システム担当者システムID
	system_user_id varchar2(20 char),
	-- システム担当者名
	system_user_name nvarchar2(200),
	-- 興行主システムコード
	promoter_code number(19,0) DEFAULT 0 NOT NULL,
	-- ログインID
	login_id varchar2(20 char),
	-- ログインパスワード
	login_password varchar2(20 char),
	-- メールアドレス
	email nvarchar2(255),
	-- ユーザーエージェント
	user_agent varchar2(300 char),
	-- システム権限設定システムコード
	system_role_setting_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT system_user_mst_pkc PRIMARY KEY (system_user_code)
);


CREATE TABLE system_user_role
(
	-- システム担当者権限設定システムコード
	system_user_role_code number(19,0) NOT NULL,
	-- システム担当者システムコード
	system_user_code number(19,0) DEFAULT 0 NOT NULL,
	-- システム権限システムコード
	system_role_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT system_user_role_pkc PRIMARY KEY (system_user_role_code)
);


CREATE TABLE ticket_set_details
(
	-- セット券詳細システムコード
	ticket_set_details_code number(19,0) NOT NULL,
	-- セット券詳細ID
	ticket_set_details_id varchar2(20 char),
	-- セット券システムコード
	ticket_set_code number(19,0) DEFAULT 0 NOT NULL,
	-- セット券詳細名
	ticket_set_details_name nvarchar2(200),
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 席種システムコード
	seat_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- 券種システムコード
	t_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- 枚数
	count number(10,0) DEFAULT 0 NOT NULL,
	-- 公演計上金額
	show_appropriation_amount number(10,0) DEFAULT 0 NOT NULL,
	-- 公演計上精算金額
	show_appropriation_seisan_amount number(10,0) DEFAULT 0 NOT NULL,
	-- 券面システムコード
	t_content_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT ticket_set_details_pkc PRIMARY KEY (ticket_set_details_code)
);


CREATE TABLE ticket_set_mst
(
	-- セット券システムコード
	ticket_set_code number(19,0) NOT NULL,
	-- セット券ID
	ticket_set_id varchar2(20 char),
	-- 興行システムコード
	series_code number(19,0) DEFAULT 0 NOT NULL,
	-- セット券名
	ticket_set_name nvarchar2(200),
	-- セット券名印刷用
	ticket_set_name_print nvarchar2(200),
	-- 金額
	ticket_set_price number(10,0) DEFAULT 0 NOT NULL,
	-- 券面金額
	ticket_set_t_content_price number(10,0) DEFAULT 0 NOT NULL,
	-- 精算金額
	ticket_set_seisan_amount number(10,0) DEFAULT 0 NOT NULL,
	-- 金額印字設定
	price_print_setting number(1) DEFAULT 0 NOT NULL,
	CONSTRAINT ticket_set_mst_pkc PRIMARY KEY (ticket_set_code)
);


CREATE TABLE t_content_layout
(
	-- 券面レイアウトシステムコード
	t_content_layout_code number(19,0) NOT NULL,
	-- 券面レイアウトID
	t_content_layout_id varchar2(20 char),
	-- 券面システムコード
	t_content_code number(19,0) DEFAULT 0 NOT NULL,
	-- 項目種別
	item_type number(10,0) DEFAULT 0 NOT NULL,
	-- 出力文字列
	output_str nvarchar2(200),
	-- 項目開始位置X
	disp_x1 binary_double DEFAULT 0 NOT NULL,
	-- 項目開始位置Y
	disp_y1 binary_double DEFAULT 0 NOT NULL,
	-- 項目終了位置X
	disp_x2 binary_double DEFAULT 0 NOT NULL,
	-- 項目終了位置Y
	disp_y2 binary_double DEFAULT 0 NOT NULL,
	-- フォントサイズX
	font_size_x binary_double DEFAULT 0 NOT NULL,
	-- フォントサイズY
	font_size_y binary_double DEFAULT 0 NOT NULL,
	-- 太字設定
	bold_flg number(1) DEFAULT 0 NOT NULL,
	-- 斜体設定
	italic_flg number(1) DEFAULT 0 NOT NULL,
	-- 下線設定
	underline_flg number(1) DEFAULT 0 NOT NULL,
	-- 項目揃え設定
	alignment_setting number(10,0) DEFAULT 0 NOT NULL,
	-- 倍角設定
	double_size number(10,0) DEFAULT 0 NOT NULL,
	-- 画像コード
	image_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT t_content_layout_pkc PRIMARY KEY (t_content_layout_code)
);


CREATE TABLE t_content_mst
(
	-- 券面システムコード
	t_content_code number(19,0) NOT NULL,
	-- 券面ID
	t_content_id varchar2(20 char),
	-- 券面名
	t_content_name nvarchar2(200),
	-- 券面種別
	t_content_type number(10,0) DEFAULT 0 NOT NULL,
	-- 券面テンプレートシステムコード
	t_content_template_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT t_content_mst_pkc PRIMARY KEY (t_content_code)
);


CREATE TABLE t_content_template_layout
(
	-- 券面テンプレートレイアウトシステムコード
	t_content_template_layout_code number(19,0) NOT NULL,
	-- 券面テンプレートシステムコード
	t_content_template_code number(19,0) DEFAULT 0 NOT NULL,
	-- 項目種別
	item_type number(10,0) DEFAULT 0 NOT NULL,
	-- 出力文字列
	output_str nvarchar2(200),
	-- 項目開始位置X
	disp_x1 binary_double DEFAULT 0 NOT NULL,
	-- 項目開始位置Y
	disp_y1 binary_double DEFAULT 0 NOT NULL,
	-- 項目終了位置X
	disp_x2 binary_double DEFAULT 0 NOT NULL,
	-- 項目終了位置Y
	disp_y2 binary_double DEFAULT 0 NOT NULL,
	-- フォントサイズX
	font_size_x binary_double DEFAULT 0 NOT NULL,
	-- フォントサイズY
	font_size_y binary_double DEFAULT 0 NOT NULL,
	-- 太字設定
	bold_flg number(1) DEFAULT 0 NOT NULL,
	-- 斜体設定
	italic_flg number(1) DEFAULT 0 NOT NULL,
	-- 下線設定
	underline_flg number(1) DEFAULT 0 NOT NULL,
	-- 項目揃え設定
	alignment_setting number(10,0) DEFAULT 0 NOT NULL,
	-- 倍角設定
	double_size number(10,0) DEFAULT 0 NOT NULL,
	-- 画像コード
	image_code number(19,0),
	CONSTRAINT t_content_template_layout_pkc PRIMARY KEY (t_content_template_layout_code)
);


CREATE TABLE t_content_template_mst
(
	-- 券面テンプレートシステムコード
	t_content_template_code number(19,0) NOT NULL,
	-- 券面テンプレートシステムID
	t_content_template_id varchar2(20 char),
	-- 券面テンプレート名
	t_content_name nvarchar2(200),
	CONSTRAINT t_content_template_mst_pkc PRIMARY KEY (t_content_template_code)
);


CREATE TABLE t_entry_mst
(
	-- 受付システムコード
	t_entry_code number(19,0) NOT NULL,
	-- 受付ID
	t_entry_id varchar2(20 char) NOT NULL,
	-- 受付名
	t_entry_name nvarchar2(200) NOT NULL,
	-- 興行システムコード
	series_code number(19,0) DEFAULT 0 NOT NULL,
	-- 受付種別
	t_entry_type number(10,0) DEFAULT 0 NOT NULL,
	-- 当選種別
	lottery_hit_type number(10,0) DEFAULT 0 NOT NULL,
	-- 公開設定
	display_setting number(10,0) DEFAULT 0 NOT NULL,
	-- 会員認証種別
	member_auth_type number(10,0) DEFAULT 0 NOT NULL,
	-- 公開開始日時
	display_start_date date NOT NULL,
	-- 公開終了日時
	display_end_date date NOT NULL,
	-- 受付開始日時
	sale_start_date date NOT NULL,
	-- 受付終了日時
	sale_end_date date NOT NULL,
	-- 直販サイト受付URL
	sale_site_url nvarchar2(2000),
	-- 受付案内文
	info_str nvarchar2(2000),
	-- 座席選択設定
	seat_select_setting number(10,0) DEFAULT 0 NOT NULL,
	-- キャンセル待ち設定
	cancel_wait_setting number(10,0) DEFAULT 0 NOT NULL,
	-- 申込希望数
	order_request_number number(10,0) DEFAULT 0 NOT NULL,
	-- 申込上限枚数
	max_request_maisu number(10,0) DEFAULT 0 NOT NULL,
	-- 申込上限回数
	max_request_count number(10,0) DEFAULT 0 NOT NULL,
	-- 抽選日
	lottery_date date NOT NULL,
	-- 抽選結果発表日
	lottery_result_date date NOT NULL,
	-- 入金終了日時種別
	charge_received_end_date_type number(10,0) DEFAULT 0 NOT NULL,
	-- 入金開始日時
	charge_received_start_date date,
	-- 入金終了日時
	charge_received_end_date date,
	-- 入金終了日数
	charge_received_end_days number(10,0),
	-- 配送開始日
	delivery_start_date date,
	-- 発券開始日時
	ticketing_start_date date,
	-- 発券終了日時
	ticketing_end_date date,
	-- システム利用料要否
	system_charge_flg number(1) DEFAULT 0 NOT NULL,
	-- システム利用料名
	system_charge_name varchar2(100 char),
	-- システム利用料
	system_charge number(10,0) DEFAULT 0 NOT NULL,
	-- サービス利用料要否
	service_charge_flg number(1) DEFAULT 0 NOT NULL,
	-- サービス利用料名
	service_charge_name varchar2(100 char),
	-- サービス利用料
	service_charge number(10,0) DEFAULT 0 NOT NULL,
	-- 配券予定日
	haiken_scheduled_date date,
	-- 入金確定日
	charge_received_fixed_date date,
	-- 原券要否
	genken_flg number(1) DEFAULT 0 NOT NULL,
	-- 原券納品日
	genken_delivery_date date,
	CONSTRAINT t_entry_mst_pkc PRIMARY KEY (t_entry_code)
);


CREATE TABLE t_entry_receive_way
(
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 受取方法システムコード
	receive_way_code number(19,0) DEFAULT 0 NOT NULL,
	-- 受取方法名
	receive_way_name nvarchar2(200),
	-- 決済手数料
	settlement_fee number(10,0) DEFAULT 0 NOT NULL,
	-- 必要日数
	need_days number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT t_entry_receive_way_pkc PRIMARY KEY (t_entry_code, receive_way_code)
);


CREATE TABLE t_entry_sales_player
(
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 販売事業者システムコード
	sales_player_code number(19,0) DEFAULT 0 NOT NULL,
	-- 販売手数料
	sale_fee varchar2(20 char),
	CONSTRAINT t_entry_sales_player_pkc PRIMARY KEY (t_entry_code, sales_player_code)
);


CREATE TABLE t_entry_settlement_way
(
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 決済方法システムコード
	settlement_way_code number(19,0) DEFAULT 0 NOT NULL,
	-- 決済方法名
	settlement_way_name nvarchar2(200),
	-- 決済手数料
	settlement_fee number(10,0) DEFAULT 0 NOT NULL,
	-- 決済手数料変動要否
	settlement_fee_change_flg number(10,0) DEFAULT 0 NOT NULL,
	-- 変動閾値金額
	change_threshold_amount number(10,0) DEFAULT 0 NOT NULL,
	-- 変動時決済手数料
	settlement_fee_range number(10,0) DEFAULT 0 NOT NULL,
	-- 必要日数
	need_days number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT t_entry_settlement_way_pkc PRIMARY KEY (t_entry_code, settlement_way_code)
);


CREATE TABLE t_entry_show
(
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 受付開始日時
	sale_start_date date NOT NULL,
	-- 受付終了日時
	sale_end_date date NOT NULL,
	CONSTRAINT t_entry_show_pkc PRIMARY KEY (t_entry_code, show_code)
);


CREATE TABLE t_entry_t_variation
(
	-- 受付システムコード
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 席種システムコード
	seat_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- 券種システムコード
	t_variation_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT t_entry_t_variation_pkc PRIMARY KEY (show_code)
);


CREATE TABLE t_stub_details
(
	-- 副券詳細システムコード
	t_stub_details_code number(19,0) NOT NULL,
	-- 副券詳細ID
	t_stub_details_id varchar2(20 char),
	-- 副券システムコード
	t_stub_code number(19,0) DEFAULT 0 NOT NULL,
	-- 副券枚数
	t_stub_count number(10,0) DEFAULT 0 NOT NULL,
	-- 券面システムコード
	t_content_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT t_stub_details_pkc PRIMARY KEY (t_stub_details_code)
);


CREATE TABLE t_stub_mst
(
	-- 副券システムコード
	t_stub_code number(19,0) NOT NULL,
	-- 副券ID
	t_stub_id varchar2(20 char),
	-- 副券名
	t_stub_name nvarchar2(200),
	CONSTRAINT t_stub_mst_pkc PRIMARY KEY (t_stub_code)
);


CREATE TABLE t_variation_mst
(
	-- 券種システムコード
	t_variation_code number(19,0) NOT NULL,
	-- 券種ID
	t_variation_id varchar2(20 char),
	-- 公演システムコード
	show_code number(19,0) DEFAULT 0 NOT NULL,
	-- 席種システムコード
	seat_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- 券種名
	t_variation_name nvarchar2(200),
	-- 印刷用券種名
	t_variation_name_print nvarchar2(200),
	-- 金額
	ticket_price number(10,0) DEFAULT 0 NOT NULL,
	-- 精算金額
	seisan_amount number(10,0) DEFAULT 0 NOT NULL,
	-- 販売単位枚数
	sales_unit_count number(10,0) DEFAULT 0 NOT NULL,
	-- 副券システムコード
	t_stub_code number(19,0) NOT NULL,
	-- 券面システムコード
	t_content_code number(19,0) NOT NULL,
	-- 金額印字設定
	price_print_setting number(1),
	-- 券面金額
	ticket_t_content_price number(10,0) DEFAULT 0 NOT NULL,
	-- 削除フラグ
	deleted number(1),
	CONSTRAINT t_variation_mst_pkc PRIMARY KEY (t_variation_code)
);



/* Create Foreign Keys */

ALTER TABLE place_seat_info
	ADD FOREIGN KEY (area_code)
	REFERENCES area_mst (area_code)
;


ALTER TABLE show_artist
	ADD FOREIGN KEY (artist_code)
	REFERENCES artist_mst (artist_code)
;


ALTER TABLE place_seat_info
	ADD FOREIGN KEY (block_code)
	REFERENCES block_mst (block_code)
;


ALTER TABLE genre_sub_mst
	ADD FOREIGN KEY (genre_code)
	REFERENCES genre_mst (genre_code)
;


ALTER TABLE show_genre
	ADD FOREIGN KEY (genre_code)
	REFERENCES genre_mst (genre_code)
;


ALTER TABLE show_genre
	ADD FOREIGN KEY (genre_sub_code)
	REFERENCES genre_sub_mst (genre_sub_code)
;


ALTER TABLE haiken_content
	ADD FOREIGN KEY (haiken_code)
	REFERENCES haiken (haiken_code)
;


ALTER TABLE haiken_sales_player
	ADD FOREIGN KEY (haiken_code)
	REFERENCES haiken (haiken_code)
;


ALTER TABLE haiken_seat
	ADD FOREIGN KEY (haiken_code)
	REFERENCES haiken (haiken_code)
;


ALTER TABLE lottery_count_info
	ADD FOREIGN KEY (haiken_code)
	REFERENCES haiken (haiken_code)
;


ALTER TABLE lottery_hit_count_info
	ADD FOREIGN KEY (haiken_code)
	REFERENCES haiken (haiken_code)
;


ALTER TABLE organizer_sales_player
	ADD FOREIGN KEY (organizer_code)
	REFERENCES organizer_mst (organizer_code)
;


ALTER TABLE place_block_info
	ADD FOREIGN KEY (place_block_code)
	REFERENCES place_block (place_block_code)
;


ALTER TABLE place_join_group
	ADD FOREIGN KEY (place_block_code)
	REFERENCES place_block (place_block_code)
;


ALTER TABLE place_seat
	ADD FOREIGN KEY (place_block_code)
	REFERENCES place_block (place_block_code)
;


ALTER TABLE place_seat_info
	ADD FOREIGN KEY (place_join_group_code)
	REFERENCES place_join_group (place_join_group_code)
;


ALTER TABLE place_block
	ADD FOREIGN KEY (place_layer_code)
	REFERENCES place_layer (place_layer_code)
;


ALTER TABLE place_object
	ADD FOREIGN KEY (place_layer_code)
	REFERENCES place_layer (place_layer_code)
;


ALTER TABLE place_seat_info
	ADD FOREIGN KEY (place_code)
	REFERENCES place_mst (place_code)
;


ALTER TABLE show_mst
	ADD FOREIGN KEY (place_code)
	REFERENCES place_mst (place_code)
;


ALTER TABLE place_object_template_info
	ADD FOREIGN KEY (place_object_template_code)
	REFERENCES place_object_template_mst (place_object_template_code)
;


ALTER TABLE place_seat
	ADD FOREIGN KEY (place_seat_code)
	REFERENCES place_seat_info (place_seat_code)
;


ALTER TABLE place_mst
	ADD FOREIGN KEY (pref_code)
	REFERENCES pref_mst (pref_code)
;


ALTER TABLE t_entry_receive_way
	ADD FOREIGN KEY (receive_way_code)
	REFERENCES receive_way_mst (receive_way_code)
;


ALTER TABLE haiken_sales_player
	ADD FOREIGN KEY (sales_player_code)
	REFERENCES sales_player_mst (sales_player_code)
;


ALTER TABLE lottery_count_info
	ADD FOREIGN KEY (sales_player_code)
	REFERENCES sales_player_mst (sales_player_code)
;


ALTER TABLE lottery_hit_count_info
	ADD FOREIGN KEY (sales_player_code)
	REFERENCES sales_player_mst (sales_player_code)
;


ALTER TABLE organizer_sales_player
	ADD FOREIGN KEY (sales_player_code)
	REFERENCES sales_player_mst (sales_player_code)
;


ALTER TABLE remote_system_mst
	ADD FOREIGN KEY (sales_player_code)
	REFERENCES sales_player_mst (sales_player_code)
;


ALTER TABLE t_entry_sales_player
	ADD FOREIGN KEY (sales_player_code)
	REFERENCES sales_player_mst (sales_player_code)
;


ALTER TABLE show_seat_info
	ADD FOREIGN KEY (seat_variation_code)
	REFERENCES seat_variation_mst (seat_variation_code)
;


ALTER TABLE ticket_set_details
	ADD FOREIGN KEY (seat_variation_code)
	REFERENCES seat_variation_mst (seat_variation_code)
;


ALTER TABLE t_entry_t_variation
	ADD FOREIGN KEY (seat_variation_code)
	REFERENCES seat_variation_mst (seat_variation_code)
;


ALTER TABLE t_variation_mst
	ADD FOREIGN KEY (seat_variation_code)
	REFERENCES seat_variation_mst (seat_variation_code)
;


ALTER TABLE show_mst
	ADD FOREIGN KEY (series_code)
	REFERENCES series_mst (series_code)
;


ALTER TABLE t_entry_mst
	ADD FOREIGN KEY (series_code)
	REFERENCES series_mst (series_code)
;


ALTER TABLE t_entry_settlement_way
	ADD FOREIGN KEY (settlement_way_code)
	REFERENCES settlement_way_mst (settlement_way_code)
;


ALTER TABLE show_block_info
	ADD FOREIGN KEY (show_block_code)
	REFERENCES show_block (show_block_code)
;


ALTER TABLE show_join_group
	ADD FOREIGN KEY (show_block_code)
	REFERENCES show_block (show_block_code)
;


ALTER TABLE show_seat
	ADD FOREIGN KEY (show_block_code)
	REFERENCES show_block (show_block_code)
;


ALTER TABLE show_seat_info
	ADD FOREIGN KEY (show_join_group_code)
	REFERENCES show_join_group (show_join_group_code)
;


ALTER TABLE show_block
	ADD FOREIGN KEY (show_layer_code)
	REFERENCES show_layer (show_layer_code)
;


ALTER TABLE show_object
	ADD FOREIGN KEY (show_layer_code)
	REFERENCES show_layer (show_layer_code)
;


ALTER TABLE haiken
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE lottery_count_info
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE lottery_hit_count_info
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE seat_variation_mst
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE show_artist
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE show_genre
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE show_group_mst
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE show_seat_info
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE t_entry_show
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE t_entry_t_variation
	ADD FOREIGN KEY (show_code)
	REFERENCES show_mst (show_code)
;


ALTER TABLE show_seat
	ADD FOREIGN KEY (show_seat_code)
	REFERENCES show_seat_info (show_seat_code)
;


ALTER TABLE system_user_role
	ADD FOREIGN KEY (system_role_code)
	REFERENCES system_role_mst (system_role_code)
;


ALTER TABLE system_role_setting
	ADD FOREIGN KEY (system_role_code)
	REFERENCES system_role_setting_mst (system_role_setting_code)
;


ALTER TABLE system_user_group_mst
	ADD FOREIGN KEY (system_role_setting_code)
	REFERENCES system_role_setting_mst (system_role_setting_code)
;


ALTER TABLE system_user_mst
	ADD FOREIGN KEY (system_role_setting_code)
	REFERENCES system_role_setting_mst (system_role_setting_code)
;


ALTER TABLE system_user_auth_log
	ADD FOREIGN KEY (system_user_code)
	REFERENCES system_user_mst (system_user_code)
;


ALTER TABLE system_user_role
	ADD FOREIGN KEY (system_user_code)
	REFERENCES system_user_mst (system_user_code)
;


ALTER TABLE system_role_setting
	ADD FOREIGN KEY (system_role_setting_code)
	REFERENCES system_user_role (system_user_role_code)
;


ALTER TABLE ticket_set_details
	ADD FOREIGN KEY (ticket_set_code)
	REFERENCES ticket_set_mst (ticket_set_code)
;


ALTER TABLE t_content_layout
	ADD FOREIGN KEY (t_content_code)
	REFERENCES t_content_mst (t_content_code)
;


ALTER TABLE t_variation_mst
	ADD FOREIGN KEY (t_content_code)
	REFERENCES t_content_mst (t_content_code)
;


ALTER TABLE t_content_mst
	ADD FOREIGN KEY (t_content_template_code)
	REFERENCES t_content_template_mst (t_content_template_code)
;


ALTER TABLE t_content_template_layout
	ADD FOREIGN KEY (t_content_template_code)
	REFERENCES t_content_template_mst (t_content_template_code)
;


ALTER TABLE haiken
	ADD FOREIGN KEY (t_entry_code)
	REFERENCES t_entry_mst (t_entry_code)
;


ALTER TABLE lottery_count_info
	ADD FOREIGN KEY (t_entry_code)
	REFERENCES t_entry_mst (t_entry_code)
;


ALTER TABLE lottery_hit_count_info
	ADD FOREIGN KEY (t_entry_code)
	REFERENCES t_entry_mst (t_entry_code)
;


ALTER TABLE t_entry_receive_way
	ADD FOREIGN KEY (t_entry_code)
	REFERENCES t_entry_mst (t_entry_code)
;


ALTER TABLE t_entry_sales_player
	ADD FOREIGN KEY (t_entry_code)
	REFERENCES t_entry_mst (t_entry_code)
;


ALTER TABLE t_entry_settlement_way
	ADD FOREIGN KEY (t_entry_code)
	REFERENCES t_entry_mst (t_entry_code)
;


ALTER TABLE t_entry_show
	ADD FOREIGN KEY (t_entry_code)
	REFERENCES t_entry_mst (t_entry_code)
;


ALTER TABLE t_entry_t_variation
	ADD FOREIGN KEY (t_entry_code)
	REFERENCES t_entry_mst (t_entry_code)
;


ALTER TABLE t_stub_details
	ADD FOREIGN KEY (t_stub_code)
	REFERENCES t_stub_mst (t_stub_code)
;


ALTER TABLE t_variation_mst
	ADD FOREIGN KEY (t_stub_code)
	REFERENCES t_stub_mst (t_stub_code)
;


ALTER TABLE ticket_set_details
	ADD FOREIGN KEY (t_variation_code)
	REFERENCES t_variation_mst (t_variation_code)
;


ALTER TABLE t_entry_t_variation
	ADD FOREIGN KEY (t_variation_code)
	REFERENCES t_variation_mst (t_variation_code)
;



/* Create Triggers */

CREATE OR REPLACE TRIGGER TRI_artist_mst_artist_code BEFORE INSERT ON artist_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_artist_mst_artist_code.nextval
	INTO :new.artist_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_genre_mst_genre_code BEFORE INSERT ON genre_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_genre_mst_genre_code.nextval
	INTO :new.genre_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_haiken_haiken_code BEFORE INSERT ON haiken
FOR EACH ROW
BEGIN
	SELECT SEQ_haiken_haiken_code.nextval
	INTO :new.haiken_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_organizer_mst_organizer_code BEFORE INSERT ON organizer_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_organizer_mst_organizer_code.nextval
	INTO :new.organizer_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_organizer_sales_player_organizer_sales_player_code BEFORE INSERT ON organizer_sales_player
FOR EACH ROW
BEGIN
	SELECT SEQ_organizer_sales_player_organizer_sales_player_code.nextval
	INTO :new.organizer_sales_player_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_place_block_place_block_code BEFORE INSERT ON place_block
FOR EACH ROW
BEGIN
	SELECT SEQ_place_block_place_block_code.nextval
	INTO :new.place_block_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_place_join_group_place_join_group_code BEFORE INSERT ON place_join_group
FOR EACH ROW
BEGIN
	SELECT SEQ_place_join_group_place_join_group_code.nextval
	INTO :new.place_join_group_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_place_mst_place_code BEFORE INSERT ON place_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_place_mst_place_code.nextval
	INTO :new.place_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_place_object_place_object_code BEFORE INSERT ON place_object
FOR EACH ROW
BEGIN
	SELECT SEQ_place_object_place_object_code.nextval
	INTO :new.place_object_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_place_object_template_mst_place_object_template_code BEFORE INSERT ON place_object_template_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_place_object_template_mst_place_object_template_code.nextval
	INTO :new.place_object_template_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_pref_mst_pref_code BEFORE INSERT ON pref_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_pref_mst_pref_code.nextval
	INTO :new.pref_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_receive_way_mst_receive_way_code BEFORE INSERT ON receive_way_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_receive_way_mst_receive_way_code.nextval
	INTO :new.receive_way_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_remote_system_auth_log_remote_system_auth_log_id BEFORE INSERT ON remote_system_auth_log
FOR EACH ROW
BEGIN
	SELECT SEQ_remote_system_auth_log_remote_system_auth_log_id.nextval
	INTO :new.remote_system_auth_log_id
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_remote_system_mst_remote_system_code BEFORE INSERT ON remote_system_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_remote_system_mst_remote_system_code.nextval
	INTO :new.remote_system_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_sales_player_mst_sales_player_code BEFORE INSERT ON sales_player_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_sales_player_mst_sales_player_code.nextval
	INTO :new.sales_player_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_seat_variation_mst_seat_variation_code BEFORE INSERT ON seat_variation_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_seat_variation_mst_seat_variation_code.nextval
	INTO :new.seat_variation_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_series_mst_series_code BEFORE INSERT ON series_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_series_mst_series_code.nextval
	INTO :new.series_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_settlement_way_mst_settlement_way_code BEFORE INSERT ON settlement_way_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_settlement_way_mst_settlement_way_code.nextval
	INTO :new.settlement_way_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_show_block_show_block_code BEFORE INSERT ON show_block
FOR EACH ROW
BEGIN
	SELECT SEQ_show_block_show_block_code.nextval
	INTO :new.show_block_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_show_group_mst_show_group_code BEFORE INSERT ON show_group_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_show_group_mst_show_group_code.nextval
	INTO :new.show_group_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_show_join_group_show_join_group_code BEFORE INSERT ON show_join_group
FOR EACH ROW
BEGIN
	SELECT SEQ_show_join_group_show_join_group_code.nextval
	INTO :new.show_join_group_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_show_layer_show_layer_code BEFORE INSERT ON show_layer
FOR EACH ROW
BEGIN
	SELECT SEQ_show_layer_show_layer_code.nextval
	INTO :new.show_layer_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_show_mst_show_code BEFORE INSERT ON show_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_show_mst_show_code.nextval
	INTO :new.show_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_show_object_show_object_code BEFORE INSERT ON show_object
FOR EACH ROW
BEGIN
	SELECT SEQ_show_object_show_object_code.nextval
	INTO :new.show_object_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_system_role_mst_system_role_code BEFORE INSERT ON system_role_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_system_role_mst_system_role_code.nextval
	INTO :new.system_role_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_system_user_mst_system_user_code BEFORE INSERT ON system_user_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_system_user_mst_system_user_code.nextval
	INTO :new.system_user_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_system_user_role_system_user_role_code BEFORE INSERT ON system_user_role
FOR EACH ROW
BEGIN
	SELECT SEQ_system_user_role_system_user_role_code.nextval
	INTO :new.system_user_role_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_ticket_set_details_ticket_set_details_code BEFORE INSERT ON ticket_set_details
FOR EACH ROW
BEGIN
	SELECT SEQ_ticket_set_details_ticket_set_details_code.nextval
	INTO :new.ticket_set_details_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_ticket_set_mst_ticket_set_code BEFORE INSERT ON ticket_set_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_ticket_set_mst_ticket_set_code.nextval
	INTO :new.ticket_set_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_t_content_layout_t_content_layout_code BEFORE INSERT ON t_content_layout
FOR EACH ROW
BEGIN
	SELECT SEQ_t_content_layout_t_content_layout_code.nextval
	INTO :new.t_content_layout_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_t_content_mst_t_content_code BEFORE INSERT ON t_content_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_t_content_mst_t_content_code.nextval
	INTO :new.t_content_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_t_content_template_layout_t_content_template_layout_code BEFORE INSERT ON t_content_template_layout
FOR EACH ROW
BEGIN
	SELECT SEQ_t_content_template_layout_t_content_template_layout_code.nextval
	INTO :new.t_content_template_layout_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_t_content_template_mst_t_content_template_code BEFORE INSERT ON t_content_template_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_t_content_template_mst_t_content_template_code.nextval
	INTO :new.t_content_template_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_t_entry_mst_t_entry_code BEFORE INSERT ON t_entry_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_t_entry_mst_t_entry_code.nextval
	INTO :new.t_entry_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_t_stub_details_t_stub_details_code BEFORE INSERT ON t_stub_details
FOR EACH ROW
BEGIN
	SELECT SEQ_t_stub_details_t_stub_details_code.nextval
	INTO :new.t_stub_details_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_t_stub_mst_t_stub_code BEFORE INSERT ON t_stub_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_t_stub_mst_t_stub_code.nextval
	INTO :new.t_stub_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_t_variation_mst_t_variation_code BEFORE INSERT ON t_variation_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_t_variation_mst_t_variation_code.nextval
	INTO :new.t_variation_code
	FROM dual;
END;

/




/* Create Indexes */

CREATE INDEX haiken_ix_show_code ON haiken (show_code);
CREATE INDEX haiken_ix_t_entry_code ON haiken (t_entry_code);
CREATE INDEX haiken_sales_player_ix_haiken_code ON haiken_sales_player (haiken_code);
CREATE INDEX haiken_seat_ix_haiken_code ON haiken_seat (haiken_code);
CREATE INDEX haiken_seat_ix_show_seat_code ON haiken_seat (show_seat_code);
CREATE INDEX organizer_sales_player_ix_sales_player_code ON organizer_sales_player (sales_player_code);
CREATE INDEX organizer_sales_player_ix_organizer_code ON organizer_sales_player (organizer_code);
CREATE INDEX place_block_ix_place_code ON place_block (place_code);
CREATE INDEX place_block_ix_place_layer_code ON place_block (place_layer_code);
CREATE INDEX place_block_info_ix_place_code ON place_block_info (place_code);
CREATE INDEX place_join_group_ix_place_code ON place_join_group (place_code);
CREATE INDEX place_layer_ix_place_code ON place_layer (place_code);
CREATE INDEX place_mst_ix_pref_code ON place_mst (pref_code);
CREATE INDEX place_object_ix_place_code ON place_object (place_code);
CREATE INDEX place_object_ix_place_layer_code ON place_object (place_layer_code);
CREATE INDEX place_seat_ix_place_block_code ON place_seat (place_block_code);
CREATE INDEX place_seat_info_ix_place_code ON place_seat_info (place_code);
CREATE INDEX place_seat_info_ix_place_join_group_code ON place_seat_info (place_join_group_code);
CREATE INDEX seat_variation_mst_ix_t_content_code ON seat_variation_mst (t_content_code);
CREATE INDEX seat_variation_mst_ix_show_code ON seat_variation_mst (show_code);
CREATE INDEX series_mst_ix_genre_code ON series_mst (genre_code);
CREATE INDEX show_block_ix_show_code ON show_block (show_code);
CREATE INDEX show_block_ix_show_layer_code ON show_block (show_layer_code);
CREATE INDEX show_block_info_ix_show_code ON show_block_info (show_code);
CREATE INDEX show_group_mst_ix_show_code ON show_group_mst (show_code);
CREATE INDEX show_join_group_ix_show_code ON show_join_group (show_code);
CREATE INDEX show_layer_ix_show_code ON show_layer (show_code);
CREATE INDEX show_mst_ix_place_code ON show_mst (place_code);
CREATE INDEX show_mst_ix_series_code ON show_mst (series_code);
CREATE INDEX show_object_ix_show_code ON show_object (show_code);
CREATE INDEX show_object_ix_show_layer_code ON show_object (show_layer_code);
CREATE INDEX show_seat_ix_show_code ON show_seat (show_code);
CREATE INDEX show_seat_info_ix_show_code ON show_seat_info (show_code);
CREATE INDEX system_role_setting_ix_system_role_code ON system_role_setting (system_role_code);
CREATE INDEX system_role_setting_ix_system_role_setting_code ON system_role_setting (system_role_setting_code);
CREATE INDEX system_role_setting_mst_ix_system_user_code ON system_role_setting_mst (system_user_code);
CREATE INDEX system_user_role_ix_system_role_code ON system_user_role (system_role_code);
CREATE INDEX system_user_role_ix_system_user_code ON system_user_role (system_user_code);
CREATE INDEX ticket_set_details_ix_t_content_code ON ticket_set_details (t_content_code);
CREATE INDEX ticket_set_details_ix_show_code ON ticket_set_details (show_code);
CREATE INDEX ticket_set_details_ix_ticket_set_code ON ticket_set_details (ticket_set_code);
CREATE INDEX ticket_set_details_ix_seat_variation_code ON ticket_set_details (seat_variation_code);
CREATE INDEX ticket_set_details_ix_t_variation_code ON ticket_set_details (t_variation_code);
CREATE INDEX ticket_set_mst_ix_series_code ON ticket_set_mst (series_code);
CREATE INDEX t_content_layout_ix_t_content_code ON t_content_layout (t_content_code);
CREATE INDEX t_content_template_layout_ix_t_content_template_code ON t_content_template_layout (t_content_template_code);
CREATE INDEX t_entry_mst_ix_series_code ON t_entry_mst (series_code);
CREATE INDEX t_entry_receive_way_ix_receive_way_code ON t_entry_receive_way (receive_way_code);
CREATE INDEX t_entry_receive_way_ix_t_entry_code ON t_entry_receive_way (t_entry_code);
CREATE INDEX t_entry_sales_player_ix_sales_player_code ON t_entry_sales_player (sales_player_code);
CREATE INDEX t_entry_sales_player_ix_t_entry_code ON t_entry_sales_player (t_entry_code);
CREATE INDEX t_entry_settlement_way_ix_settlement_way_code ON t_entry_settlement_way (settlement_way_code);
CREATE INDEX t_entry_settlement_way_ix_t_entry_code ON t_entry_settlement_way (t_entry_code);
CREATE INDEX t_entry_show_ix_show_code ON t_entry_show (show_code);
CREATE INDEX t_entry_show_ix_t_entry_code ON t_entry_show (t_entry_code);
CREATE INDEX t_entry_t_variation_ix_seat_variation_code ON t_entry_t_variation (seat_variation_code);
CREATE INDEX t_entry_t_variation_ix_t_variation_code ON t_entry_t_variation (t_variation_code);
CREATE INDEX t_entry_t_variation_ix_t_entry_code ON t_entry_t_variation (t_entry_code);
CREATE INDEX t_stub_details_ix_t_stub_code ON t_stub_details (t_stub_code);
CREATE INDEX t_stub_details_ix_t_content_code ON t_stub_details (t_content_code);
CREATE INDEX t_variation_mst_ix_t_stub_code ON t_variation_mst (t_stub_code);
CREATE INDEX t_variation_mst_ix_t_content_code ON t_variation_mst (t_content_code);
CREATE INDEX t_variation_mst_ix_show_code ON t_variation_mst (show_code);
CREATE INDEX t_variation_mst_ix_seat_variation_code ON t_variation_mst (seat_variation_code);



/* Comments */

COMMENT ON COLUMN area_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN artist_mst.artist_code IS 'アーティストシステムコード';
COMMENT ON COLUMN artist_mst.artist_id IS 'アーティストシステムID';
COMMENT ON COLUMN artist_mst.artist_name IS 'アーティスト名';
COMMENT ON COLUMN artist_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN block_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN genre_mst.genre_code IS 'ジャンルシステムコード';
COMMENT ON COLUMN genre_mst.genre_id IS 'ジャンルシステムID';
COMMENT ON COLUMN genre_mst.genre_name IS 'ジャンル名';
COMMENT ON COLUMN genre_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN genre_sub_mst.genre_sub_code IS 'ジャンルシステムコード';
COMMENT ON COLUMN genre_sub_mst.genre_sub_id IS 'ジャンルシステムID';
COMMENT ON COLUMN genre_sub_mst.genre_code IS 'ジャンルシステムコード';
COMMENT ON COLUMN genre_sub_mst.genre_sub_name IS 'ジャンル名';
COMMENT ON COLUMN genre_sub_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN haiken.haiken_code IS '配券システムコード';
COMMENT ON COLUMN haiken.haiken_id IS '配券ID';
COMMENT ON COLUMN haiken.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN haiken.show_code IS '公演システムコード';
COMMENT ON COLUMN haiken.box_office_sales_setting IS '窓口販売設定';
COMMENT ON COLUMN haiken.haiken_type IS '配券種別';
COMMENT ON COLUMN haiken_content.haiken_code IS '配券システムコード';
COMMENT ON COLUMN haiken_content.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN haiken_content.show_seat_code IS '公演座席システムコード';
COMMENT ON COLUMN haiken_content.seat_variation_code IS '席種システムコード';
COMMENT ON COLUMN haiken_content.t_variation_code IS '券種システムコード';
COMMENT ON COLUMN haiken_sales_player.haiken_code IS '配券システムコード';
COMMENT ON COLUMN haiken_sales_player.sales_player_code IS '販売事業者システムコード';
COMMENT ON COLUMN haiken_seat.haiken_code IS '配券システムコード';
COMMENT ON COLUMN haiken_seat.seat_variation_code IS '席種システムコード';
COMMENT ON COLUMN haiken_seat.show_seat_code IS '公演座席コード';
COMMENT ON COLUMN haiken_seat.count IS '枚数';
COMMENT ON COLUMN lottery_count_info.haiken_code IS '配券システムコード';
COMMENT ON COLUMN lottery_count_info.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN lottery_count_info.show_code IS '公演システムコード';
COMMENT ON COLUMN lottery_count_info.sales_player_code IS '販売事業者システムコード';
COMMENT ON COLUMN lottery_hit_count_info.haiken_code IS '配券システムコード';
COMMENT ON COLUMN lottery_hit_count_info.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN lottery_hit_count_info.show_code IS '公演システムコード';
COMMENT ON COLUMN lottery_hit_count_info.sales_player_code IS '販売事業者システムコード';
COMMENT ON COLUMN organizer_mst.organizer_code IS '興行主システムコード';
COMMENT ON COLUMN organizer_mst.organizer_id IS '興行主ID';
COMMENT ON COLUMN organizer_mst.organizer_name IS '興行主名';
COMMENT ON COLUMN organizer_sales_player.organizer_sales_player_code IS '興行主利用販売事業者システムコード';
COMMENT ON COLUMN organizer_sales_player.organizer_sales_player_id IS '興行主利用販売事業者システムID';
COMMENT ON COLUMN organizer_sales_player.organizer_code IS '興行主システムコード';
COMMENT ON COLUMN organizer_sales_player.sales_player_code IS '販売事業者コード';
COMMENT ON COLUMN organizer_sales_player.display_char IS '表示文字';
COMMENT ON COLUMN organizer_sales_player.display_color IS '表示色';
COMMENT ON COLUMN place_block.place_code IS '会場システムコード';
COMMENT ON COLUMN place_block.place_layer_code IS '会場レイヤーシステムコード';
COMMENT ON COLUMN place_block.place_block_code IS '会場ブロックシステムコード';
COMMENT ON COLUMN place_block.place_block_name IS '会場ブロック名';
COMMENT ON COLUMN place_block.angle IS '角度';
COMMENT ON COLUMN place_block.priority IS '会場ブロック優先順位';
COMMENT ON COLUMN place_block_info.place_code IS '会場システムコード';
COMMENT ON COLUMN place_block_info.place_block_code IS '会場ブロックシステムコード';
COMMENT ON COLUMN place_block_info.parent_block_code IS '親ブロックシステムコード';
COMMENT ON COLUMN place_block_info.parent_block_disp_x1 IS '親ブロック座標X1';
COMMENT ON COLUMN place_block_info.parent_block_disp_y1 IS '親ブロック座標Y1';
COMMENT ON COLUMN place_block_info.parent_block_disp_x2 IS '親ブロック座標X2';
COMMENT ON COLUMN place_block_info.parent_block_disp_y2 IS '親ブロック座標Y2';
COMMENT ON COLUMN place_block_info.angle IS '角度';
COMMENT ON COLUMN place_join_group.place_code IS '会場システムコード';
COMMENT ON COLUMN place_join_group.place_block_code IS '会場ブロックシステムコード';
COMMENT ON COLUMN place_join_group.place_join_group_code IS '会場結合グループシステムコード';
COMMENT ON COLUMN place_join_group.disp_x1 IS '座標X1';
COMMENT ON COLUMN place_join_group.disp_y1 IS '座標Y1';
COMMENT ON COLUMN place_join_group.disp_x2 IS '座標X2';
COMMENT ON COLUMN place_join_group.disp_y2 IS '座標Y2';
COMMENT ON COLUMN place_join_group.angle IS '角度';
COMMENT ON COLUMN place_layer.place_code IS '会場システムコード';
COMMENT ON COLUMN place_layer.place_layer_code IS '会場レイヤーシステムコード';
COMMENT ON COLUMN place_layer.place_layer_name IS '会場レイヤー名';
COMMENT ON COLUMN place_layer.select_flg IS '選択可否';
COMMENT ON COLUMN place_layer.zoom_level IS '会場ブロック優先順位';
COMMENT ON COLUMN place_mst.place_code IS '会場システムコード';
COMMENT ON COLUMN place_mst.place_id IS '会場ID';
COMMENT ON COLUMN place_mst.place_name IS '会場名';
COMMENT ON COLUMN place_mst.place_name_print IS '会場名（印刷用）';
COMMENT ON COLUMN place_mst.zip IS '郵便番号';
COMMENT ON COLUMN place_mst.city IS '市区町村';
COMMENT ON COLUMN place_mst.building IS '建物名';
COMMENT ON COLUMN place_mst.tel IS '電話番号';
COMMENT ON COLUMN place_mst.latitude IS '緯度';
COMMENT ON COLUMN place_mst.longitude IS '経度';
COMMENT ON COLUMN place_mst.pref_code IS '都道府県システムコード';
COMMENT ON COLUMN place_mst.address IS '番地';
COMMENT ON COLUMN place_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN place_object.place_code IS '会場システムコード';
COMMENT ON COLUMN place_object.place_layer_code IS '会場レイヤーシステムコード';
COMMENT ON COLUMN place_object.place_object_code IS '会場オブジェクトシステムコード';
COMMENT ON COLUMN place_object.object_type IS 'オブジェクト種別';
COMMENT ON COLUMN place_object.disp_x1 IS '座標X1';
COMMENT ON COLUMN place_object.disp_y1 IS '座標Y1';
COMMENT ON COLUMN place_object.disp_x2 IS '座標X2';
COMMENT ON COLUMN place_object.disp_y2 IS '座標Y2';
COMMENT ON COLUMN place_object.angle IS '角度';
COMMENT ON COLUMN place_object_template_info.place_object_template_code IS '会場オブジェクトテンプレートシステムコード';
COMMENT ON COLUMN place_object_template_info.parts_place_object_template_code IS '会場オブジェクトテンプレート部品システムコード';
COMMENT ON COLUMN place_object_template_info.object_type IS 'オブジェクト種別';
COMMENT ON COLUMN place_object_template_info.disp_x1 IS '座標X1';
COMMENT ON COLUMN place_object_template_info.disp_y1 IS '座標Y1';
COMMENT ON COLUMN place_object_template_info.disp_x2 IS '座標X2';
COMMENT ON COLUMN place_object_template_info.disp_y2 IS '座標Y2';
COMMENT ON COLUMN place_object_template_info.angle IS '角度';
COMMENT ON COLUMN place_object_template_mst.place_object_template_code IS '会場オブジェクトテンプレートシステムコード';
COMMENT ON COLUMN place_object_template_mst.place_object_template_name IS '会場オブジェクトテンプレート名';
COMMENT ON COLUMN place_object_template_mst.template_type IS 'テンプレート種別';
COMMENT ON COLUMN place_seat.place_code IS '会場システムコード';
COMMENT ON COLUMN place_seat.place_seat_code IS '会場座席システムコード';
COMMENT ON COLUMN place_seat.place_block_code IS '会場ブロックシステムコード';
COMMENT ON COLUMN place_seat.display_type IS '表示タイプ';
COMMENT ON COLUMN place_seat.disp_x1 IS '座標X1';
COMMENT ON COLUMN place_seat.disp_y1 IS '座標Y1';
COMMENT ON COLUMN place_seat.disp_x2 IS '座標X2';
COMMENT ON COLUMN place_seat.disp_y2 IS '座標Y2';
COMMENT ON COLUMN place_seat_info.place_code IS '会場システムコード';
COMMENT ON COLUMN place_seat_info.place_seat_code IS '会場座席システムコード';
COMMENT ON COLUMN place_seat_info.place_join_group_code IS '結合グループシステムコード';
COMMENT ON COLUMN place_seat_info.seat_floor IS '階';
COMMENT ON COLUMN place_seat_info.seat_row IS '列';
COMMENT ON COLUMN place_seat_info.seat_number IS '番';
COMMENT ON COLUMN place_seat_info.numbered_ticket_number IS '整理券番号';
COMMENT ON COLUMN place_seat_info.show_area IS 'エリア';
COMMENT ON COLUMN place_seat_info.show_block IS 'ブロック';
COMMENT ON COLUMN place_seat_info.other_attribute1 IS 'その他座席属性1';
COMMENT ON COLUMN place_seat_info.other_attribute2 IS 'その他座席属性2';
COMMENT ON COLUMN place_seat_info.other_attribute3 IS 'その他座席属性3';
COMMENT ON COLUMN place_seat_info.other_attribute4 IS 'その他座席属性4';
COMMENT ON COLUMN place_seat_info.other_attribute5 IS 'その他座席属性5';
COMMENT ON COLUMN place_seat_info.seat_line_number IS '連席番号';
COMMENT ON COLUMN pref_mst.pref_code IS '都道府県システムコード';
COMMENT ON COLUMN pref_mst.pref_id IS '都道府県システムID';
COMMENT ON COLUMN pref_mst.pref_name IS '都道府県名称';
COMMENT ON COLUMN pref_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN receive_way_mst.receive_way_code IS '受取方法システムコード';
COMMENT ON COLUMN receive_way_mst.receive_way_id IS '受取方法ID';
COMMENT ON COLUMN receive_way_mst.receive_way_name IS '受取方法名称';
COMMENT ON COLUMN remote_system_auth_log.remote_system_auth_log_id IS '接続元認証ログID';
COMMENT ON COLUMN remote_system_auth_log.remote_system_code IS '接続元コード';
COMMENT ON COLUMN remote_system_auth_log.token_key IS 'トークンキー';
COMMENT ON COLUMN remote_system_auth_log.auth_date IS '認証日時';
COMMENT ON COLUMN remote_system_auth_log.auth_result IS '認証結果　失敗:0、成功:1';
COMMENT ON COLUMN remote_system_auth_log.reason IS '理由';
COMMENT ON COLUMN remote_system_auth_log.user_agent IS 'ユーザーエージェント';
COMMENT ON COLUMN remote_system_mst.remote_system_code IS '接続元コード';
COMMENT ON COLUMN remote_system_mst.sales_player_code IS '販売事業者システムコード';
COMMENT ON COLUMN remote_system_mst.remote_system_name IS '接続元名';
COMMENT ON COLUMN remote_system_mst.token_key IS 'トークンキー';
COMMENT ON COLUMN remote_system_mst.authority_level IS '1=票券、2=販売ASP、3=プレイガイド';
COMMENT ON COLUMN remote_system_mst.ip_address IS '接続元のIPアドレス。現在は使用していない。';
COMMENT ON COLUMN remote_system_mst.deleted_at IS '削除日時';
COMMENT ON COLUMN sales_player_mst.sales_player_code IS '販売事業者システムコード';
COMMENT ON COLUMN sales_player_mst.sales_player_id IS '販売事業者システムID';
COMMENT ON COLUMN sales_player_mst.sales_player_name IS '販売事業者名';
COMMENT ON COLUMN seat_variation_mst.seat_variation_code IS '席種システムコード';
COMMENT ON COLUMN seat_variation_mst.seat_variation_id IS '席種ID';
COMMENT ON COLUMN seat_variation_mst.show_code IS '公演システムコード';
COMMENT ON COLUMN seat_variation_mst.seat_variation_name IS '席種名';
COMMENT ON COLUMN seat_variation_mst.seat_variation_name_print IS '印刷用席種名';
COMMENT ON COLUMN seat_variation_mst.seat_variation_type IS '種別';
COMMENT ON COLUMN seat_variation_mst.seat_variation_display_color IS '席種表示色';
COMMENT ON COLUMN seat_variation_mst.t_content_code IS '券面システムコード';
COMMENT ON COLUMN seat_variation_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN series_mst.series_code IS '興行システムコード';
COMMENT ON COLUMN series_mst.series_id IS '興行システムID';
COMMENT ON COLUMN series_mst.series_name IS '興行名';
COMMENT ON COLUMN series_mst.series_info_text IS 'イベント案内文';
COMMENT ON COLUMN series_mst.series_image IS 'イベント画像(要検討)';
COMMENT ON COLUMN series_mst.sale_site_url IS '直販サイトURL';
COMMENT ON COLUMN series_mst.genre_code IS 'ジャンルシステムコード';
COMMENT ON COLUMN series_mst.season_fiscal_year IS 'シーズン年度';
COMMENT ON COLUMN series_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN settlement_way_mst.settlement_way_code IS '決済方法システムコード';
COMMENT ON COLUMN settlement_way_mst.settlement_way_id IS '決済方法ID';
COMMENT ON COLUMN settlement_way_mst.settlement_way_name IS '決済方法名称';
COMMENT ON COLUMN show_artist.artist_code IS 'アーティストシステムコード';
COMMENT ON COLUMN show_artist.show_code IS '公演システムコード';
COMMENT ON COLUMN show_block.show_code IS '公演システムコード';
COMMENT ON COLUMN show_block.show_layer_code IS '公演レイヤーシステムコード';
COMMENT ON COLUMN show_block.show_block_code IS '公演ブロックシステムコード';
COMMENT ON COLUMN show_block.show_block_name IS '公演ブロック名';
COMMENT ON COLUMN show_block.angle IS '角度';
COMMENT ON COLUMN show_block.priority IS '公演ブロック優先順位';
COMMENT ON COLUMN show_block_info.show_code IS '公演システムコード';
COMMENT ON COLUMN show_block_info.show_block_code IS '公演ブロックシステムコード';
COMMENT ON COLUMN show_block_info.parent_block_code IS '親ブロックシステムコード';
COMMENT ON COLUMN show_block_info.parent_block_disp_x1 IS '親ブロック座標X1';
COMMENT ON COLUMN show_block_info.parent_block_disp_y1 IS '親ブロック座標Y1';
COMMENT ON COLUMN show_block_info.parent_block_disp_x2 IS '親ブロック座標X2';
COMMENT ON COLUMN show_block_info.parent_block_disp_y2 IS '親ブロック座標Y2';
COMMENT ON COLUMN show_block_info.angle IS '角度';
COMMENT ON COLUMN show_genre.show_code IS '公演システムコード';
COMMENT ON COLUMN show_genre.genre_code IS 'ジャンルシステムコード';
COMMENT ON COLUMN show_genre.genre_sub_code IS 'ジャンルシステムコード';
COMMENT ON COLUMN show_group_mst.show_group_code IS '公演グループシステムコード';
COMMENT ON COLUMN show_group_mst.show_group_id IS '公演グループID';
COMMENT ON COLUMN show_group_mst.show_group_name IS '公演グループ名';
COMMENT ON COLUMN show_group_mst.show_code IS '公演システムコード';
COMMENT ON COLUMN show_join_group.show_code IS '公演システムコード';
COMMENT ON COLUMN show_join_group.show_block_code IS '公演ブロックシステムコード';
COMMENT ON COLUMN show_join_group.show_join_group_code IS '公演結合グループシステムコード';
COMMENT ON COLUMN show_join_group.disp_x1 IS '座標X1';
COMMENT ON COLUMN show_join_group.disp_y1 IS '座標Y1';
COMMENT ON COLUMN show_join_group.disp_x2 IS '座標X2';
COMMENT ON COLUMN show_join_group.disp_y2 IS '座標Y2';
COMMENT ON COLUMN show_join_group.angle IS '角度';
COMMENT ON COLUMN show_layer.show_code IS '公演システムコード';
COMMENT ON COLUMN show_layer.show_layer_code IS '公演レイヤーシステムコード';
COMMENT ON COLUMN show_layer.show_layer_name IS '公演レイヤー名';
COMMENT ON COLUMN show_layer.select_flg IS '選択可否';
COMMENT ON COLUMN show_layer.zoom_level IS '公演ブロック優先順位';
COMMENT ON COLUMN show_mst.show_code IS '公演システムコード';
COMMENT ON COLUMN show_mst.show_id IS '公演ID';
COMMENT ON COLUMN show_mst.series_code IS '興行システムコード';
COMMENT ON COLUMN show_mst.place_code IS '会場システムコード';
COMMENT ON COLUMN show_mst.show_name IS '公演名';
COMMENT ON COLUMN show_mst.show_date IS '公演日付';
COMMENT ON COLUMN show_mst.door_open_date IS '開場日付';
COMMENT ON COLUMN show_mst.show_end_date IS '終演日付';
COMMENT ON COLUMN show_mst.show_info_str IS '公演案内文';
COMMENT ON COLUMN show_mst.show_contact_address IS '公演問い合わせ先';
COMMENT ON COLUMN show_mst.sale_site_url IS '直販サイトURL';
COMMENT ON COLUMN show_mst.show_user_info IS '公演担当者情報';
COMMENT ON COLUMN show_mst.box_office_sale_start_date IS '窓口販売開始日付';
COMMENT ON COLUMN show_mst.box_office_sale_end_date IS '窓口販売終了日付';
COMMENT ON COLUMN show_mst.show_time IS '公演日時';
COMMENT ON COLUMN show_mst.door_open_time IS '開場時間';
COMMENT ON COLUMN show_mst.show_end_time IS '終演日時';
COMMENT ON COLUMN show_mst.box_office_sale_start_time IS '窓口販売開始時間';
COMMENT ON COLUMN show_mst.box_office_sale_end_time IS '窓口販売終了時間';
COMMENT ON COLUMN show_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN show_object.show_code IS '公演システムコード';
COMMENT ON COLUMN show_object.show_layer_code IS '公演レイヤーシステムコード';
COMMENT ON COLUMN show_object.show_object_code IS '公演オブジェクトシステムコード';
COMMENT ON COLUMN show_object.object_type IS 'オブジェクト種別';
COMMENT ON COLUMN show_object.disp_x1 IS '座標X1';
COMMENT ON COLUMN show_object.disp_y1 IS '座標Y1';
COMMENT ON COLUMN show_object.disp_x2 IS '座標X2';
COMMENT ON COLUMN show_object.disp_y2 IS '座標Y2';
COMMENT ON COLUMN show_object.angle IS '角度';
COMMENT ON COLUMN show_seat.show_code IS '公演システムコード';
COMMENT ON COLUMN show_seat.show_seat_code IS '公演座席システムコード';
COMMENT ON COLUMN show_seat.show_block_code IS '公演ブロックシステムコード';
COMMENT ON COLUMN show_seat.display_type IS '表示タイプ';
COMMENT ON COLUMN show_seat.disp_x1 IS '座標X1';
COMMENT ON COLUMN show_seat.disp_y1 IS '座標Y1';
COMMENT ON COLUMN show_seat.disp_x2 IS '座標X2';
COMMENT ON COLUMN show_seat.disp_y2 IS '座標Y2';
COMMENT ON COLUMN show_seat_info.show_code IS '公演システムコード';
COMMENT ON COLUMN show_seat_info.show_seat_code IS '公演座席システムコード';
COMMENT ON COLUMN show_seat_info.seat_variation_code IS '席種システムコード';
COMMENT ON COLUMN show_seat_info.show_join_group_code IS '結合グループシステムコード';
COMMENT ON COLUMN show_seat_info.seat_floor IS '階';
COMMENT ON COLUMN show_seat_info.seat_row IS '列';
COMMENT ON COLUMN show_seat_info.seat_number IS '番';
COMMENT ON COLUMN show_seat_info.numbered_ticket_number IS '整理券番号';
COMMENT ON COLUMN show_seat_info.show_area IS 'エリア';
COMMENT ON COLUMN show_seat_info.show_block IS 'ブロック';
COMMENT ON COLUMN show_seat_info.other_attribute1 IS 'その他座席属性1';
COMMENT ON COLUMN show_seat_info.other_attribute2 IS 'その他座席属性2';
COMMENT ON COLUMN show_seat_info.other_attribute3 IS 'その他座席属性3';
COMMENT ON COLUMN show_seat_info.other_attribute4 IS 'その他座席属性4';
COMMENT ON COLUMN show_seat_info.other_attribute5 IS 'その他座席属性5';
COMMENT ON COLUMN show_seat_info.seat_line_number IS '連席番号';
COMMENT ON COLUMN system_role_mst.system_role_code IS 'システム権限システムコード';
COMMENT ON COLUMN system_role_mst.system_role_id IS 'システム権限ID';
COMMENT ON COLUMN system_role_mst.system_role_name IS 'システム権限名';
COMMENT ON COLUMN system_role_setting.system_role_setting_code IS 'system_authority_set_code';
COMMENT ON COLUMN system_role_setting.system_role_code IS 'system_authority_code';
COMMENT ON COLUMN system_role_setting_mst.system_role_setting_code IS 'システム権限設定システムコード';
COMMENT ON COLUMN system_role_setting_mst.system_role_setting_name IS 'system_authority_set_name';
COMMENT ON COLUMN system_role_setting_mst.system_user_code IS 'SystemUserMst_SystemUserCode';
COMMENT ON COLUMN system_user_auth_log.system_user_code IS 'システムユーザーシステムコード';
COMMENT ON COLUMN system_user_auth_log.auth_date IS '認証日時';
COMMENT ON COLUMN system_user_auth_log.auth_result IS '失敗:0、成功:1';
COMMENT ON COLUMN system_user_auth_log.ip_address IS '接続元のIPアドレス';
COMMENT ON COLUMN system_user_auth_log.user_agent IS 'ユーザーエージェント';
COMMENT ON COLUMN system_user_group_mst.system_role_setting_code IS 'システム権限設定システムコード';
COMMENT ON COLUMN system_user_group_mst.deleted IS '削除フラグ';
COMMENT ON COLUMN system_user_mst.system_user_code IS 'システム担当者システムコード';
COMMENT ON COLUMN system_user_mst.system_user_id IS 'システム担当者システムID';
COMMENT ON COLUMN system_user_mst.system_user_name IS 'システム担当者名';
COMMENT ON COLUMN system_user_mst.promoter_code IS '興行主システムコード';
COMMENT ON COLUMN system_user_mst.login_id IS 'ログインID';
COMMENT ON COLUMN system_user_mst.login_password IS 'ログインパスワード';
COMMENT ON COLUMN system_user_mst.email IS 'メールアドレス';
COMMENT ON COLUMN system_user_mst.user_agent IS 'ユーザーエージェント';
COMMENT ON COLUMN system_user_mst.system_role_setting_code IS 'システム権限設定システムコード';
COMMENT ON COLUMN system_user_role.system_user_role_code IS 'システム担当者権限設定システムコード';
COMMENT ON COLUMN system_user_role.system_user_code IS 'システム担当者システムコード';
COMMENT ON COLUMN system_user_role.system_role_code IS 'システム権限システムコード';
COMMENT ON COLUMN ticket_set_details.ticket_set_details_code IS 'セット券詳細システムコード';
COMMENT ON COLUMN ticket_set_details.ticket_set_details_id IS 'セット券詳細ID';
COMMENT ON COLUMN ticket_set_details.ticket_set_code IS 'セット券システムコード';
COMMENT ON COLUMN ticket_set_details.ticket_set_details_name IS 'セット券詳細名';
COMMENT ON COLUMN ticket_set_details.show_code IS '公演システムコード';
COMMENT ON COLUMN ticket_set_details.seat_variation_code IS '席種システムコード';
COMMENT ON COLUMN ticket_set_details.t_variation_code IS '券種システムコード';
COMMENT ON COLUMN ticket_set_details.count IS '枚数';
COMMENT ON COLUMN ticket_set_details.show_appropriation_amount IS '公演計上金額';
COMMENT ON COLUMN ticket_set_details.show_appropriation_seisan_amount IS '公演計上精算金額';
COMMENT ON COLUMN ticket_set_details.t_content_code IS '券面システムコード';
COMMENT ON COLUMN ticket_set_mst.ticket_set_code IS 'セット券システムコード';
COMMENT ON COLUMN ticket_set_mst.ticket_set_id IS 'セット券ID';
COMMENT ON COLUMN ticket_set_mst.series_code IS '興行システムコード';
COMMENT ON COLUMN ticket_set_mst.ticket_set_name IS 'セット券名';
COMMENT ON COLUMN ticket_set_mst.ticket_set_name_print IS 'セット券名印刷用';
COMMENT ON COLUMN ticket_set_mst.ticket_set_price IS '金額';
COMMENT ON COLUMN ticket_set_mst.ticket_set_t_content_price IS '券面金額';
COMMENT ON COLUMN ticket_set_mst.ticket_set_seisan_amount IS '精算金額';
COMMENT ON COLUMN ticket_set_mst.price_print_setting IS '金額印字設定';
COMMENT ON COLUMN t_content_layout.t_content_layout_code IS '券面レイアウトシステムコード';
COMMENT ON COLUMN t_content_layout.t_content_layout_id IS '券面レイアウトID';
COMMENT ON COLUMN t_content_layout.t_content_code IS '券面システムコード';
COMMENT ON COLUMN t_content_layout.item_type IS '項目種別';
COMMENT ON COLUMN t_content_layout.output_str IS '出力文字列';
COMMENT ON COLUMN t_content_layout.disp_x1 IS '項目開始位置X';
COMMENT ON COLUMN t_content_layout.disp_y1 IS '項目開始位置Y';
COMMENT ON COLUMN t_content_layout.disp_x2 IS '項目終了位置X';
COMMENT ON COLUMN t_content_layout.disp_y2 IS '項目終了位置Y';
COMMENT ON COLUMN t_content_layout.font_size_x IS 'フォントサイズX';
COMMENT ON COLUMN t_content_layout.font_size_y IS 'フォントサイズY';
COMMENT ON COLUMN t_content_layout.bold_flg IS '太字設定';
COMMENT ON COLUMN t_content_layout.italic_flg IS '斜体設定';
COMMENT ON COLUMN t_content_layout.underline_flg IS '下線設定';
COMMENT ON COLUMN t_content_layout.alignment_setting IS '項目揃え設定';
COMMENT ON COLUMN t_content_layout.double_size IS '倍角設定';
COMMENT ON COLUMN t_content_layout.image_code IS '画像コード';
COMMENT ON COLUMN t_content_mst.t_content_code IS '券面システムコード';
COMMENT ON COLUMN t_content_mst.t_content_id IS '券面ID';
COMMENT ON COLUMN t_content_mst.t_content_name IS '券面名';
COMMENT ON COLUMN t_content_mst.t_content_type IS '券面種別';
COMMENT ON COLUMN t_content_mst.t_content_template_code IS '券面テンプレートシステムコード';
COMMENT ON COLUMN t_content_template_layout.t_content_template_layout_code IS '券面テンプレートレイアウトシステムコード';
COMMENT ON COLUMN t_content_template_layout.t_content_template_code IS '券面テンプレートシステムコード';
COMMENT ON COLUMN t_content_template_layout.item_type IS '項目種別';
COMMENT ON COLUMN t_content_template_layout.output_str IS '出力文字列';
COMMENT ON COLUMN t_content_template_layout.disp_x1 IS '項目開始位置X';
COMMENT ON COLUMN t_content_template_layout.disp_y1 IS '項目開始位置Y';
COMMENT ON COLUMN t_content_template_layout.disp_x2 IS '項目終了位置X';
COMMENT ON COLUMN t_content_template_layout.disp_y2 IS '項目終了位置Y';
COMMENT ON COLUMN t_content_template_layout.font_size_x IS 'フォントサイズX';
COMMENT ON COLUMN t_content_template_layout.font_size_y IS 'フォントサイズY';
COMMENT ON COLUMN t_content_template_layout.bold_flg IS '太字設定';
COMMENT ON COLUMN t_content_template_layout.italic_flg IS '斜体設定';
COMMENT ON COLUMN t_content_template_layout.underline_flg IS '下線設定';
COMMENT ON COLUMN t_content_template_layout.alignment_setting IS '項目揃え設定';
COMMENT ON COLUMN t_content_template_layout.double_size IS '倍角設定';
COMMENT ON COLUMN t_content_template_layout.image_code IS '画像コード';
COMMENT ON COLUMN t_content_template_mst.t_content_template_code IS '券面テンプレートシステムコード';
COMMENT ON COLUMN t_content_template_mst.t_content_template_id IS '券面テンプレートシステムID';
COMMENT ON COLUMN t_content_template_mst.t_content_name IS '券面テンプレート名';
COMMENT ON COLUMN t_entry_mst.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN t_entry_mst.t_entry_id IS '受付ID';
COMMENT ON COLUMN t_entry_mst.t_entry_name IS '受付名';
COMMENT ON COLUMN t_entry_mst.series_code IS '興行システムコード';
COMMENT ON COLUMN t_entry_mst.t_entry_type IS '受付種別';
COMMENT ON COLUMN t_entry_mst.lottery_hit_type IS '当選種別';
COMMENT ON COLUMN t_entry_mst.display_setting IS '公開設定';
COMMENT ON COLUMN t_entry_mst.member_auth_type IS '会員認証種別';
COMMENT ON COLUMN t_entry_mst.display_start_date IS '公開開始日時';
COMMENT ON COLUMN t_entry_mst.display_end_date IS '公開終了日時';
COMMENT ON COLUMN t_entry_mst.sale_start_date IS '受付開始日時';
COMMENT ON COLUMN t_entry_mst.sale_end_date IS '受付終了日時';
COMMENT ON COLUMN t_entry_mst.sale_site_url IS '直販サイト受付URL';
COMMENT ON COLUMN t_entry_mst.info_str IS '受付案内文';
COMMENT ON COLUMN t_entry_mst.seat_select_setting IS '座席選択設定';
COMMENT ON COLUMN t_entry_mst.cancel_wait_setting IS 'キャンセル待ち設定';
COMMENT ON COLUMN t_entry_mst.order_request_number IS '申込希望数';
COMMENT ON COLUMN t_entry_mst.max_request_maisu IS '申込上限枚数';
COMMENT ON COLUMN t_entry_mst.max_request_count IS '申込上限回数';
COMMENT ON COLUMN t_entry_mst.lottery_date IS '抽選日';
COMMENT ON COLUMN t_entry_mst.lottery_result_date IS '抽選結果発表日';
COMMENT ON COLUMN t_entry_mst.charge_received_end_date_type IS '入金終了日時種別';
COMMENT ON COLUMN t_entry_mst.charge_received_start_date IS '入金開始日時';
COMMENT ON COLUMN t_entry_mst.charge_received_end_date IS '入金終了日時';
COMMENT ON COLUMN t_entry_mst.charge_received_end_days IS '入金終了日数';
COMMENT ON COLUMN t_entry_mst.delivery_start_date IS '配送開始日';
COMMENT ON COLUMN t_entry_mst.ticketing_start_date IS '発券開始日時';
COMMENT ON COLUMN t_entry_mst.ticketing_end_date IS '発券終了日時';
COMMENT ON COLUMN t_entry_mst.system_charge_flg IS 'システム利用料要否';
COMMENT ON COLUMN t_entry_mst.system_charge_name IS 'システム利用料名';
COMMENT ON COLUMN t_entry_mst.system_charge IS 'システム利用料';
COMMENT ON COLUMN t_entry_mst.service_charge_flg IS 'サービス利用料要否';
COMMENT ON COLUMN t_entry_mst.service_charge_name IS 'サービス利用料名';
COMMENT ON COLUMN t_entry_mst.service_charge IS 'サービス利用料';
COMMENT ON COLUMN t_entry_mst.haiken_scheduled_date IS '配券予定日';
COMMENT ON COLUMN t_entry_mst.charge_received_fixed_date IS '入金確定日';
COMMENT ON COLUMN t_entry_mst.genken_flg IS '原券要否';
COMMENT ON COLUMN t_entry_mst.genken_delivery_date IS '原券納品日';
COMMENT ON COLUMN t_entry_receive_way.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN t_entry_receive_way.receive_way_code IS '受取方法システムコード';
COMMENT ON COLUMN t_entry_receive_way.receive_way_name IS '受取方法名';
COMMENT ON COLUMN t_entry_receive_way.settlement_fee IS '決済手数料';
COMMENT ON COLUMN t_entry_receive_way.need_days IS '必要日数';
COMMENT ON COLUMN t_entry_sales_player.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN t_entry_sales_player.sales_player_code IS '販売事業者システムコード';
COMMENT ON COLUMN t_entry_sales_player.sale_fee IS '販売手数料';
COMMENT ON COLUMN t_entry_settlement_way.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN t_entry_settlement_way.settlement_way_code IS '決済方法システムコード';
COMMENT ON COLUMN t_entry_settlement_way.settlement_way_name IS '決済方法名';
COMMENT ON COLUMN t_entry_settlement_way.settlement_fee IS '決済手数料';
COMMENT ON COLUMN t_entry_settlement_way.settlement_fee_change_flg IS '決済手数料変動要否';
COMMENT ON COLUMN t_entry_settlement_way.change_threshold_amount IS '変動閾値金額';
COMMENT ON COLUMN t_entry_settlement_way.settlement_fee_range IS '変動時決済手数料';
COMMENT ON COLUMN t_entry_settlement_way.need_days IS '必要日数';
COMMENT ON COLUMN t_entry_show.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN t_entry_show.show_code IS '公演システムコード';
COMMENT ON COLUMN t_entry_show.sale_start_date IS '受付開始日時';
COMMENT ON COLUMN t_entry_show.sale_end_date IS '受付終了日時';
COMMENT ON COLUMN t_entry_t_variation.t_entry_code IS '受付システムコード';
COMMENT ON COLUMN t_entry_t_variation.show_code IS '公演システムコード';
COMMENT ON COLUMN t_entry_t_variation.seat_variation_code IS '席種システムコード';
COMMENT ON COLUMN t_entry_t_variation.t_variation_code IS '券種システムコード';
COMMENT ON COLUMN t_stub_details.t_stub_details_code IS '副券詳細システムコード';
COMMENT ON COLUMN t_stub_details.t_stub_details_id IS '副券詳細ID';
COMMENT ON COLUMN t_stub_details.t_stub_code IS '副券システムコード';
COMMENT ON COLUMN t_stub_details.t_stub_count IS '副券枚数';
COMMENT ON COLUMN t_stub_details.t_content_code IS '券面システムコード';
COMMENT ON COLUMN t_stub_mst.t_stub_code IS '副券システムコード';
COMMENT ON COLUMN t_stub_mst.t_stub_id IS '副券ID';
COMMENT ON COLUMN t_stub_mst.t_stub_name IS '副券名';
COMMENT ON COLUMN t_variation_mst.t_variation_code IS '券種システムコード';
COMMENT ON COLUMN t_variation_mst.t_variation_id IS '券種ID';
COMMENT ON COLUMN t_variation_mst.show_code IS '公演システムコード';
COMMENT ON COLUMN t_variation_mst.seat_variation_code IS '席種システムコード';
COMMENT ON COLUMN t_variation_mst.t_variation_name IS '券種名';
COMMENT ON COLUMN t_variation_mst.t_variation_name_print IS '印刷用券種名';
COMMENT ON COLUMN t_variation_mst.ticket_price IS '金額';
COMMENT ON COLUMN t_variation_mst.seisan_amount IS '精算金額';
COMMENT ON COLUMN t_variation_mst.sales_unit_count IS '販売単位枚数';
COMMENT ON COLUMN t_variation_mst.t_stub_code IS '副券システムコード';
COMMENT ON COLUMN t_variation_mst.t_content_code IS '券面システムコード';
COMMENT ON COLUMN t_variation_mst.price_print_setting IS '金額印字設定';
COMMENT ON COLUMN t_variation_mst.ticket_t_content_price IS '券面金額';
COMMENT ON COLUMN t_variation_mst.deleted IS '削除フラグ';



