
/* Drop Indexes */

DROP INDEX member_fee_charge_ix_member_code;
DROP INDEX member_fee_charge_received_ix_member_code;
DROP INDEX member_fee_charge_received_ix_member_fee_charge_code;
DROP INDEX member_fee_charge_received_ix_settlement_way_code;
DROP INDEX member_genre_ix_genre_code;
DROP INDEX member_genre_ix_member_code;
DROP INDEX member_mst_ix_member_group_code;
DROP INDEX member_mst_ix_member_type_code;
DROP INDEX member_mst_ix_pref_code;
DROP INDEX member_type_mst_ix_member_group_code;
DROP INDEX purchase_enter_ix_show_seat_code;
DROP INDEX purchase_enter_ix_purchase_code;
DROP INDEX purchase_main_ix_member_code;
DROP INDEX purchase_main_ix_settlement_way_code;
DROP INDEX purchase_main_ix_pref_code;
DROP INDEX purchase_refund_ix_show_seat_code;
DROP INDEX purchase_refund_ix_purchase_code;
DROP INDEX purchase_seat_ix_show_seat_code;
DROP INDEX purchase_seat_ix_seat_variation_code;
DROP INDEX purchase_seat_ix_t_variation_code;



/* Drop Triggers */

DROP TRIGGER TRI_association_mst_association_code;
DROP TRIGGER TRI_member_auth_log_member_auth_log_id;
DROP TRIGGER TRI_member_fee_charge_member_fee_charge_code;
DROP TRIGGER TRI_member_fee_charge_received_member_fee_charge_received_code;
DROP TRIGGER TRI_member_genre_member_genre_code;
DROP TRIGGER TRI_member_mst_member_code;
DROP TRIGGER TRI_member_type_mst_member_type_code;
DROP TRIGGER TRI_purchase_content_purchase_content_code;
DROP TRIGGER TRI_purchase_enter_purchase_enter_code;
DROP TRIGGER TRI_purchase_main_purchase_code;
DROP TRIGGER TRI_purchase_refund_purchase_refund_code;
DROP TRIGGER TRI_purchase_seat_purchase_seat_code;
DROP TRIGGER TRI_purchase_shipping_purchase_shipping_code;



/* Drop Tables */

DROP TABLE association_mst CASCADE CONSTRAINTS;
DROP TABLE member_auth_log CASCADE CONSTRAINTS;
DROP TABLE member_fee_charge CASCADE CONSTRAINTS;
DROP TABLE member_fee_charge_received CASCADE CONSTRAINTS;
DROP TABLE member_genre CASCADE CONSTRAINTS;
DROP TABLE purchase_seat CASCADE CONSTRAINTS;
DROP TABLE purchase_content CASCADE CONSTRAINTS;
DROP TABLE purchase_enter CASCADE CONSTRAINTS;
DROP TABLE purchase_refund CASCADE CONSTRAINTS;
DROP TABLE purchase_shipping CASCADE CONSTRAINTS;
DROP TABLE purchase_main CASCADE CONSTRAINTS;
DROP TABLE member_mst CASCADE CONSTRAINTS;
DROP TABLE member_type_mst CASCADE CONSTRAINTS;
DROP TABLE member_group_mst CASCADE CONSTRAINTS;



/* Drop Sequences */

DROP SEQUENCE SEQ_association_mst_association_code;
DROP SEQUENCE SEQ_member_auth_log_member_auth_log_id;
DROP SEQUENCE SEQ_member_fee_charge_member_fee_charge_code;
DROP SEQUENCE SEQ_member_fee_charge_received_member_fee_charge_received_code;
DROP SEQUENCE SEQ_member_genre_member_genre_code;
DROP SEQUENCE SEQ_member_group_mst_member_group_code;
DROP SEQUENCE SEQ_member_mst_member_code;
DROP SEQUENCE SEQ_member_type_mst_member_type_code;
DROP SEQUENCE SEQ_order_info_lottery_info_code;
DROP SEQUENCE SEQ_order_shipping_shipping_code;
DROP SEQUENCE SEQ_order_uketsuke_order_code;
DROP SEQUENCE SEQ_purchase_content_purchase_content_code;
DROP SEQUENCE SEQ_purchase_enter_purchase_enter_code;
DROP SEQUENCE SEQ_purchase_main_purchase_code;
DROP SEQUENCE SEQ_purchase_refund_purchase_refund_code;
DROP SEQUENCE SEQ_purchase_seat_purchase_seat_code;
DROP SEQUENCE SEQ_purchase_shipping_purchase_shipping_code;
DROP SEQUENCE SEQ_shipping_shipping_code;




/* Create Sequences */

CREATE SEQUENCE SEQ_association_mst_association_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_member_auth_log_member_auth_log_id INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_member_fee_charge_member_fee_charge_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_member_fee_charge_received_member_fee_charge_received_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_member_genre_member_genre_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_member_group_mst_member_group_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_member_mst_member_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_member_type_mst_member_type_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_order_info_lottery_info_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_order_shipping_shipping_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_order_uketsuke_order_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_purchase_content_purchase_content_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_purchase_enter_purchase_enter_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_purchase_main_purchase_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_purchase_refund_purchase_refund_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_purchase_seat_purchase_seat_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_purchase_shipping_purchase_shipping_code INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_shipping_shipping_code INCREMENT BY 1 START WITH 1;



/* Create Tables */

-- 団体マスタ
CREATE TABLE association_mst
(
	-- 団体システムコード : 団体システムコード
	association_code number(19,0) NOT NULL,
	-- 団体システムID : 団体システムID
	association_id varchar2(20 char),
	-- 団体名称 : 団体名称
	association_name nvarchar2(200),
	-- 削除フラグ : 削除フラグ
	deleted number(1) DEFAULT 0 NOT NULL,
	CONSTRAINT association_mst_pkc PRIMARY KEY (association_code)
);


-- 会員認証ログ
CREATE TABLE member_auth_log
(
	-- 会員認証ログID
	member_auth_log_id number(19,0) NOT NULL,
	-- 会員組織システムコード : 会員組織システムコード
	member_group_code number(19,0),
	-- WebログインID : WebログインID
	web_login_id nvarchar2(255),
	-- 認証日時
	auth_date date DEFAULT SYSDATE,
	-- 認証結果 : 失敗:0、成功:1
	result number(10,0) NOT NULL,
	-- 理由
	reason varchar2(300),
	-- IPアドレス
	ip_address varchar2(39),
	-- ユーザーエージェント
	user_agent varchar2(300),
	PRIMARY KEY (member_auth_log_id)
);


-- 会費請求
CREATE TABLE member_fee_charge
(
	-- 会費請求システムコード : 会費請求システムコード
	member_fee_charge_code number(19,0) NOT NULL,
	-- 会員システムコード : 会員システムコード
	member_code number(19,0) NOT NULL,
	-- 会費 : 会費
	member_fee number(10,0),
	-- 請求状態 : 請求状態
	charge_status number(10,0) DEFAULT 0 NOT NULL,
	-- 請求日 : 請求日
	charge_date date NOT NULL,
	-- 請求書コード : 請求書コード
	bill_code varchar2(20 char),
	-- 入金状態 : 入金状態
	charge_received_status number(10,0),
	CONSTRAINT member_fee_charge_pkc PRIMARY KEY (member_fee_charge_code)
);


-- 会費入金
CREATE TABLE member_fee_charge_received
(
	-- 会費入金システムコード : 会費入金システムコード
	member_fee_charge_received_code number(19,0) NOT NULL,
	-- 会員システムコード : 会員システムコード
	member_code number(19,0) NOT NULL,
	-- 会費請求システムコード : 会費請求システムコード
	member_fee_charge_code number(19,0) DEFAULT 0 NOT NULL,
	-- 入金日 : 入金日
	charge_received_date date NOT NULL,
	-- 入金金額 : 入金金額
	charge_received_amount number(10,0) DEFAULT 0 NOT NULL,
	-- 決済方法システムコード : 決済方法システムコード
	settlement_way_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT member_fee_charge_received_pkc PRIMARY KEY (member_fee_charge_received_code)
);


-- 会員興味ジャンル
CREATE TABLE member_genre
(
	-- 会員興味ジャンルシステムコード : 会員興味ジャンルシステムコード
	member_genre_code number(19,0) NOT NULL,
	-- 会員システムコード : 会員システムコード
	member_code number(19,0) NOT NULL,
	-- ジャンルシステムコード : ジャンルシステムコード
	genre_code number(19,0) DEFAULT 0 NOT NULL,
	CONSTRAINT member_genre_pkc PRIMARY KEY (member_genre_code)
);


-- 会員組織
CREATE TABLE member_group_mst
(
	-- 会員組織システムコード : 会員組織システムコード
	member_group_code number(19,0) NOT NULL,
	-- 会員組織ID : 会員組織ID
	member_group_id varchar2(20 char),
	-- 会員組織名 : 会員組織名
	member_group_name nvarchar2(200),
	-- 販売事業者システムコード : 販売事業者システムコード
	sales_player_code number(19,0) NOT NULL,
	CONSTRAINT member_group_mst_pkc PRIMARY KEY (member_group_code)
);


-- 会員
CREATE TABLE member_mst
(
	-- 会員システムコード : 会員システムコード
	member_code number(19,0) NOT NULL,
	-- 会員ID : 会員ID
	member_id varchar2(20 char),
	-- 会員組織システムコード : 会員組織システムコード
	member_group_code number(19,0) DEFAULT 0 NOT NULL,
	-- 会員種別システムコード : 会員種別システムコード
	member_type_code number(19,0) NOT NULL,
	-- 代表者会員システムコード : 代表者会員システムコード
	representative_member_code number(19,0) DEFAULT 0 NOT NULL,
	-- 名前（姓） : 名前（姓）
	family_name_kanji nvarchar2(20),
	-- 名前（名） : 名前（名）
	first_name_kanji nvarchar2(20),
	-- 名前カナ（姓） : 名前カナ（姓）
	family_name_kana nvarchar2(20),
	-- 名前カナ（名） : 名前カナ（名）
	first_name_kana nvarchar2(20),
	-- 郵便番号 : 0010001のハイフンなしの形式で登録
	zip varchar2(10 char) NOT NULL,
	-- 都道府県システムコード : 都道府県システムコード
	pref_code number(19,0) DEFAULT 0 NOT NULL,
	-- 市区町村 : 市区町村
	city nvarchar2(200),
	-- 番地 : 番地
	address nvarchar2(200),
	-- 建物名 : 建物名
	building nvarchar2(200),
	-- 電話番号 : 電話番号
	tel nvarchar2(20),
	-- メールアドレス : メールアドレス
	email nvarchar2(255),
	-- 性別 : 性別
	gender number(10,0) DEFAULT 0 NOT NULL,
	-- 生年月日 : 生年月日
	birthday date NOT NULL,
	-- WebログインID : WebログインID
	web_login_id nvarchar2(255),
	-- Webログインパスワード : Webログインパスワード
	web_login_password varchar2(64 char),
	-- 入会年月日 : 入会年月日
	member_entry_date date DEFAULT SYSDATE NOT NULL,
	-- 更新年月日 : 更新年月日
	updated_at date,
	-- 退会年月日 : 退会年月日
	withdrawal_date date,
	-- ブラック状態 : ブラック状態
	black_status number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT member_mst_pkc PRIMARY KEY (member_code)
);


-- 会員種別
CREATE TABLE member_type_mst
(
	-- 会員種別システムコード : 会員種別システムコード
	member_type_code number(19,0) NOT NULL,
	-- 会員組織ID : 会員組織ID
	member_type_id varchar2(20 char),
	-- 会員組織システムコード : 会員組織システムコード
	member_group_code number(19,0) NOT NULL,
	-- 会員種別名 : 会員種別名
	member_group_name nvarchar2(200),
	-- 会員ID生成用文字列 : 会員ID生成用文字列
	member_id_prefix nvarchar2(20),
	-- 有料区分 : 有料区分
	paid_type number(10,0) DEFAULT 0 NOT NULL,
	-- 有料期間 : 有料期間
	paid_period number(10,0) DEFAULT 0 NOT NULL,
	-- 会員ランク : 会員ランク
	member_rank number(10,0) DEFAULT 0 NOT NULL,
	-- 会費 : 会費
	member_fee number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT member_type_mst_pkc PRIMARY KEY (member_type_code)
);


-- 購入内容 : 抽選申込内容を記録
CREATE TABLE purchase_content
(
	-- 購入内容システムコード
	purchase_content_code number(19,0) NOT NULL,
	-- 購入システムコード : 購入システムコード
	purchase_code number(19,0) NOT NULL,
	-- 受付システムコード : 受付システムコード
	t_entry_code number(19,0) NOT NULL,
	-- 興行システムコード : 興行システムコード
	series_code number(19,0),
	-- 公演システムコード : 公演システムコード
	show_code number(19,0),
	-- 優先順位 : 第一希望、第2希望などの順位
	priority number(10,0),
	-- 発券開始日時 : 発券開始日時
	ticketing_start_date date,
	-- 発券終了日時 : 発券終了日時
	ticketing_end_date date,
	-- 座席割付状態 : 座席割付状態
	seat_waritsuke_status number(10,0) DEFAULT 0,
	-- 抽選状態 : 0=未抽選、111=当選確定、121=落選確定、(21=落選未確定、11=当選未確定 ※票券のみ利用)
	lottery_status number(1),
	PRIMARY KEY (purchase_content_code)
);


-- 入場情報
CREATE TABLE purchase_enter
(
	-- 購入入場システムコード : 購入入場システムコード
	purchase_enter_code number(19,0) NOT NULL,
	-- 購入システムコード : 購入システムコード
	purchase_code number(19,0) NOT NULL,
	-- 公演座席システムコード : 公演座席システムコード
	show_seat_code number(19,0) DEFAULT 0 NOT NULL,
	-- 入場状態 : 入場状態
	enter_status number(10,0) DEFAULT 0,
	-- 入場日時 : 入場日時
	enter_date date,
	-- 電子チケットID : 電子チケットID
	eticket_id nvarchar2(100) DEFAULT '0' NOT NULL,
	CONSTRAINT purchase_enter_pkc PRIMARY KEY (purchase_enter_code)
);


-- 購入情報 : 一般販売、先行販売共通で使用する。
CREATE TABLE purchase_main
(
	-- 購入システムコード : 購入システムコード
	purchase_code number(19,0) NOT NULL,
	-- 購入ID(購入トランザクションID) : 購入ID(購入トランザクションID)
	purchase_id varchar2(20 char),
	-- 販売事業者購入番号 : 販売事業者購入番号
	sales_player_purchase_number nvarchar2(20),
	-- 販売事業者システムコード : 販売事業者システムコード
	sales_player_code number(19,0),
	-- 会員システムコード : 会員システムコード
	member_code number(19,0) NOT NULL,
	-- 名前（姓） : 名前（姓）
	family_name_kanji nvarchar2(20),
	-- 名前（名） : 名前（名）
	first_name_kanji nvarchar2(20),
	-- 名前カナ（姓） : 名前カナ（姓）
	family_name_kana nvarchar2(20),
	-- 名前カナ（名） : 名前カナ（名）
	first_name_kana nvarchar2(20),
	-- 郵便番号 : 0010001のハイフンなしの形式で登録
	zip varchar2(10 char) DEFAULT '0' NOT NULL,
	-- 都道府県システムコード : 都道府県システムコード
	pref_code number(19,0) DEFAULT 0 NOT NULL,
	-- 市区町村 : 市区町村
	city nvarchar2(200),
	-- 番地 : 番地
	address nvarchar2(200),
	-- 建物名 : 建物名
	building nvarchar2(200),
	-- 電話番号 : 電話番号
	tel nvarchar2(20),
	-- メールアドレス : メールアドレス
	email nvarchar2(255),
	-- 性別 : 性別
	gender number(10,0) DEFAULT 0 NOT NULL,
	-- 生年月日 : 生年月日
	birthday date NOT NULL,
	-- 購入日時 : 購入日時
	purchase_date date NOT NULL,
	-- 決済方法システムコード : 決済方法システムコード
	settlement_way_code number(19,0) DEFAULT 0 NOT NULL,
	-- 決済手数料 : 決済手数料
	settlement_fee number(10,0) DEFAULT 0 NOT NULL,
	-- 受取合計手数料 : 受取合計手数料
	receipt_total_fee_price number(10,0) DEFAULT 0 NOT NULL,
	-- 入金開始日時 : 入金開始日時
	charge_received_start_date date NOT NULL,
	-- 入金終了日時 : 入金終了日時
	charge_received_end_date date NOT NULL,
	-- 予約流れ日時 : 予約流れ日時
	reserve_canceled_date date NOT NULL,
	-- 入金状態 : 入金状態
	charge_received_status number(10,0) DEFAULT 0,
	-- 入金日 : 入金日
	charge_received_date date,
	-- 決済代行会社取引番号 : 決済代行会社取引番号
	settlement_agent_order_number nvarchar2(100),
	-- クレジット与信状態 : クレジット与信状態
	credit_auth_status number(10,0),
	-- CVS払込票番号 : CVS払込票番号
	cvs_payment_paper_number nvarchar2(100),
	-- 抽選状態 : 0=未抽選,1=抽選済み
	lottery_status number(1),
	-- 取消状態 : 取消状態
	cancel_status number(10,0) DEFAULT 0,
	-- 取消日時 : 取消日時
	cancel_date date,
	-- 購入合計金額 : 購入合計金額
	total_purchase_amount number(10,0) DEFAULT 0 NOT NULL,
	-- 購入合計枚数 : 購入合計枚数
	total_purchase_count number(10,0) DEFAULT 0 NOT NULL,
	-- システム合計利用料 : システム合計利用料
	total_system_charge number(10,0) DEFAULT 0 NOT NULL,
	-- サービス合計利用料 : サービス合計利用料
	total_service_charge number(10,0) DEFAULT 0 NOT NULL,
	-- 購入フラグ : 0=未購入、1=購入or当選 ※購入履歴に並べる
	purchase_flg number(1) DEFAULT 0 NOT NULL,
	-- 抽選申込フラグ : 0=通常購入、1=抽選申込 ※抽選申込履歴に並べる
	lottery_application_flg number(1) DEFAULT 0 NOT NULL,
	-- ベリトランスクレジットカードID : ベリトランスのワンクリック継続課金サービスで採番されるカードID、決済時に会員NoとカードIDを指定して支払を行う。
	-- 抽選時のみ利用
	veritrans_credit_card_id varchar2(100),
	CONSTRAINT purchase_main_pkc PRIMARY KEY (purchase_code)
);


-- 払戻情報
CREATE TABLE purchase_refund
(
	-- 購入払戻システムコード : 購入払戻システムコード
	purchase_refund_code number(19,0) NOT NULL,
	-- 購入システムコード : 購入システムコード
	purchase_code number(19,0) NOT NULL,
	-- 公演座席システムコード : 公演座席システムコード
	show_seat_code number(19,0) DEFAULT 0 NOT NULL,
	-- 払戻状態 : 払戻状態
	refund_status number(10,0) DEFAULT 0 NOT NULL,
	-- 払戻日時 : 払戻日時
	refund_date date NOT NULL,
	-- 払戻金額 : 払戻金額
	refund_amount number(10,0) DEFAULT 0 NOT NULL,
	CONSTRAINT purchase_refund_pkc PRIMARY KEY (purchase_refund_code)
);


-- 購入座席
CREATE TABLE purchase_seat
(
	-- 購入座席システムコード : 購入座席システムコード
	purchase_seat_code number(19,0) NOT NULL,
	-- 送付先システムコード
	purchase_shipping_code number(19,0) NOT NULL,
	-- 購入内容システムコード
	purchase_content_code number(19,0) NOT NULL,
	-- 販売事業者座席コード : 販売事業者座席コード
	sales_player_seat_code number(19,0) DEFAULT 0 NOT NULL,
	-- 公演座席システムコード : 公演座席システムコード
	show_seat_code number(19,0) DEFAULT 0,
	-- 席種システムコード : 席種システムコード
	seat_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- 券種システムコード : 券種システムコード
	t_variation_code number(19,0) DEFAULT 0 NOT NULL,
	-- CVSバーコード番号（セブン） : CVSバーコード番号（セブン）
	cvs_barcode_number varchar2(20 char) DEFAULT '0' NOT NULL,
	-- システム利用料 : システム利用料
	system_charge number(10,0),
	-- サービス利用料 : サービス利用料
	service_charge number(10,0),
	-- チケット金額 : チケット金額
	ticket_price number(10,0),
	CONSTRAINT purchase_seat_pkc PRIMARY KEY (purchase_seat_code)
);


-- 送付先 : 購入チケットの送付先を記録する
CREATE TABLE purchase_shipping
(
	-- 送付先システムコード
	purchase_shipping_code number(19,0) NOT NULL,
	-- 購入システムコード : 購入システムコード
	purchase_code number(19,0) NOT NULL,
	-- 会員システムコード : 会員システムコード
	member_code number(19,0),
	-- 名前（姓） : 名前（姓）
	family_name_kanji nvarchar2(20),
	-- 名前（名） : 名前（名）
	first_name_kanji nvarchar2(20),
	-- 名前カナ（姓） : 名前カナ（姓）
	family_name_kana nvarchar2(20),
	-- 名前カナ（名） : 名前カナ（名）
	first_name_kana nvarchar2(20),
	-- 郵便番号 : 0010001のハイフンなしの形式で登録
	zip varchar2(10 char),
	-- 都道府県システムコード : 都道府県システムコード
	pref_code number(19,0),
	-- 市区町村 : 市区町村
	city nvarchar2(200),
	-- 番地 : 番地
	address nvarchar2(200),
	-- 建物名 : 建物名
	building nvarchar2(200),
	-- 電話番号 : 電話番号
	tel nvarchar2(20),
	-- メールアドレス : メールアドレス
	email nvarchar2(255),
	-- 発券状態 : 発券状態
	ticketing_status number(10,0) DEFAULT 0,
	-- 発券日 : 発券日
	ticketing_date date,
	-- CVS引換票番号 : CVS引換票番号
	cvs_exchage_paper_number nvarchar2(100),
	-- 配送状態 : 配送状態
	delivery_status number(10,0) DEFAULT 0,
	-- 配送日時 : 配送日時
	delivery_date date,
	-- 配送追跡番号 : 配送追跡番号
	delivery_tracking_number nvarchar2(100),
	-- 受取方法システムコード : 受取方法システムコード
	receive_way_code number(19,0) NOT NULL,
	-- 受取手数料 : 受取手数料
	settlement_fee number(10,0),
	PRIMARY KEY (purchase_shipping_code)
);



/* Create Foreign Keys */

ALTER TABLE member_mst
	ADD FOREIGN KEY (member_group_code)
	REFERENCES member_group_mst (member_group_code)
;


ALTER TABLE member_type_mst
	ADD FOREIGN KEY (member_group_code)
	REFERENCES member_group_mst (member_group_code)
;


ALTER TABLE member_fee_charge
	ADD FOREIGN KEY (member_code)
	REFERENCES member_mst (member_code)
;


ALTER TABLE member_fee_charge_received
	ADD FOREIGN KEY (member_code)
	REFERENCES member_mst (member_code)
;


ALTER TABLE member_genre
	ADD FOREIGN KEY (member_code)
	REFERENCES member_mst (member_code)
;


ALTER TABLE purchase_main
	ADD FOREIGN KEY (member_code)
	REFERENCES member_mst (member_code)
;


ALTER TABLE purchase_shipping
	ADD FOREIGN KEY (member_code)
	REFERENCES member_mst (member_code)
;


ALTER TABLE member_mst
	ADD FOREIGN KEY (member_type_code)
	REFERENCES member_type_mst (member_type_code)
;


ALTER TABLE purchase_seat
	ADD FOREIGN KEY (purchase_content_code)
	REFERENCES purchase_content (purchase_content_code)
;


ALTER TABLE purchase_content
	ADD FOREIGN KEY (purchase_code)
	REFERENCES purchase_main (purchase_code)
;


ALTER TABLE purchase_enter
	ADD FOREIGN KEY (purchase_code)
	REFERENCES purchase_main (purchase_code)
;


ALTER TABLE purchase_refund
	ADD FOREIGN KEY (purchase_code)
	REFERENCES purchase_main (purchase_code)
;


ALTER TABLE purchase_shipping
	ADD FOREIGN KEY (purchase_code)
	REFERENCES purchase_main (purchase_code)
;


ALTER TABLE purchase_seat
	ADD FOREIGN KEY (purchase_shipping_code)
	REFERENCES purchase_shipping (purchase_shipping_code)
;



/* Create Triggers */

CREATE OR REPLACE TRIGGER TRI_association_mst_association_code BEFORE INSERT ON association_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_association_mst_association_code.nextval
	INTO :new.association_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_member_auth_log_member_auth_log_id BEFORE INSERT ON member_auth_log
FOR EACH ROW
BEGIN
	SELECT SEQ_member_auth_log_member_auth_log_id.nextval
	INTO :new.member_auth_log_id
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_member_fee_charge_member_fee_charge_code BEFORE INSERT ON member_fee_charge
FOR EACH ROW
BEGIN
	SELECT SEQ_member_fee_charge_member_fee_charge_code.nextval
	INTO :new.member_fee_charge_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_member_fee_charge_received_member_fee_charge_received_code BEFORE INSERT ON member_fee_charge_received
FOR EACH ROW
BEGIN
	SELECT SEQ_member_fee_charge_received_member_fee_charge_received_code.nextval
	INTO :new.member_fee_charge_received_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_member_genre_member_genre_code BEFORE INSERT ON member_genre
FOR EACH ROW
BEGIN
	SELECT SEQ_member_genre_member_genre_code.nextval
	INTO :new.member_genre_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_member_mst_member_code BEFORE INSERT ON member_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_member_mst_member_code.nextval
	INTO :new.member_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_member_type_mst_member_type_code BEFORE INSERT ON member_type_mst
FOR EACH ROW
BEGIN
	SELECT SEQ_member_type_mst_member_type_code.nextval
	INTO :new.member_type_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_purchase_content_purchase_content_code BEFORE INSERT ON purchase_content
FOR EACH ROW
BEGIN
	SELECT SEQ_purchase_content_purchase_content_code.nextval
	INTO :new.purchase_content_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_purchase_enter_purchase_enter_code BEFORE INSERT ON purchase_enter
FOR EACH ROW
BEGIN
	SELECT SEQ_purchase_enter_purchase_enter_code.nextval
	INTO :new.purchase_enter_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_purchase_main_purchase_code BEFORE INSERT ON purchase_main
FOR EACH ROW
BEGIN
	SELECT SEQ_purchase_main_purchase_code.nextval
	INTO :new.purchase_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_purchase_refund_purchase_refund_code BEFORE INSERT ON purchase_refund
FOR EACH ROW
BEGIN
	SELECT SEQ_purchase_refund_purchase_refund_code.nextval
	INTO :new.purchase_refund_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_purchase_seat_purchase_seat_code BEFORE INSERT ON purchase_seat
FOR EACH ROW
BEGIN
	SELECT SEQ_purchase_seat_purchase_seat_code.nextval
	INTO :new.purchase_seat_code
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_purchase_shipping_purchase_shipping_code BEFORE INSERT ON purchase_shipping
FOR EACH ROW
BEGIN
	SELECT SEQ_purchase_shipping_purchase_shipping_code.nextval
	INTO :new.purchase_shipping_code
	FROM dual;
END;

/




/* Create Indexes */

CREATE INDEX member_fee_charge_ix_member_code ON member_fee_charge (member_code);
CREATE INDEX member_fee_charge_received_ix_member_code ON member_fee_charge_received (member_code);
CREATE INDEX member_fee_charge_received_ix_member_fee_charge_code ON member_fee_charge_received (member_fee_charge_code);
CREATE INDEX member_fee_charge_received_ix_settlement_way_code ON member_fee_charge_received (settlement_way_code);
CREATE INDEX member_genre_ix_genre_code ON member_genre (genre_code);
CREATE INDEX member_genre_ix_member_code ON member_genre (member_code);
CREATE INDEX member_mst_ix_member_group_code ON member_mst (member_group_code);
CREATE INDEX member_mst_ix_member_type_code ON member_mst (member_type_code);
CREATE INDEX member_mst_ix_pref_code ON member_mst (pref_code);
CREATE INDEX member_type_mst_ix_member_group_code ON member_type_mst (member_group_code);
CREATE INDEX purchase_enter_ix_show_seat_code ON purchase_enter (show_seat_code);
CREATE INDEX purchase_enter_ix_purchase_code ON purchase_enter (purchase_code);
CREATE INDEX purchase_main_ix_member_code ON purchase_main (member_code);
CREATE INDEX purchase_main_ix_settlement_way_code ON purchase_main (settlement_way_code);
CREATE INDEX purchase_main_ix_pref_code ON purchase_main (pref_code);
CREATE INDEX purchase_refund_ix_show_seat_code ON purchase_refund (show_seat_code);
CREATE INDEX purchase_refund_ix_purchase_code ON purchase_refund (purchase_code);
CREATE INDEX purchase_seat_ix_show_seat_code ON purchase_seat (show_seat_code);
CREATE INDEX purchase_seat_ix_seat_variation_code ON purchase_seat (seat_variation_code);
CREATE INDEX purchase_seat_ix_t_variation_code ON purchase_seat (t_variation_code);



/* Comments */

COMMENT ON TABLE association_mst IS '団体マスタ';
COMMENT ON COLUMN association_mst.association_code IS '団体システムコード : 団体システムコード';
COMMENT ON COLUMN association_mst.association_id IS '団体システムID : 団体システムID';
COMMENT ON COLUMN association_mst.association_name IS '団体名称 : 団体名称';
COMMENT ON COLUMN association_mst.deleted IS '削除フラグ : 削除フラグ';
COMMENT ON TABLE member_auth_log IS '会員認証ログ';
COMMENT ON COLUMN member_auth_log.member_auth_log_id IS '会員認証ログID';
COMMENT ON COLUMN member_auth_log.member_group_code IS '会員組織システムコード : 会員組織システムコード';
COMMENT ON COLUMN member_auth_log.web_login_id IS 'WebログインID : WebログインID';
COMMENT ON COLUMN member_auth_log.auth_date IS '認証日時';
COMMENT ON COLUMN member_auth_log.result IS '認証結果 : 失敗:0、成功:1';
COMMENT ON COLUMN member_auth_log.reason IS '理由';
COMMENT ON COLUMN member_auth_log.ip_address IS 'IPアドレス';
COMMENT ON COLUMN member_auth_log.user_agent IS 'ユーザーエージェント';
COMMENT ON TABLE member_fee_charge IS '会費請求';
COMMENT ON COLUMN member_fee_charge.member_fee_charge_code IS '会費請求システムコード : 会費請求システムコード';
COMMENT ON COLUMN member_fee_charge.member_code IS '会員システムコード : 会員システムコード';
COMMENT ON COLUMN member_fee_charge.member_fee IS '会費 : 会費';
COMMENT ON COLUMN member_fee_charge.charge_status IS '請求状態 : 請求状態';
COMMENT ON COLUMN member_fee_charge.charge_date IS '請求日 : 請求日';
COMMENT ON COLUMN member_fee_charge.bill_code IS '請求書コード : 請求書コード';
COMMENT ON COLUMN member_fee_charge.charge_received_status IS '入金状態 : 入金状態';
COMMENT ON TABLE member_fee_charge_received IS '会費入金';
COMMENT ON COLUMN member_fee_charge_received.member_fee_charge_received_code IS '会費入金システムコード : 会費入金システムコード';
COMMENT ON COLUMN member_fee_charge_received.member_code IS '会員システムコード : 会員システムコード';
COMMENT ON COLUMN member_fee_charge_received.member_fee_charge_code IS '会費請求システムコード : 会費請求システムコード';
COMMENT ON COLUMN member_fee_charge_received.charge_received_date IS '入金日 : 入金日';
COMMENT ON COLUMN member_fee_charge_received.charge_received_amount IS '入金金額 : 入金金額';
COMMENT ON COLUMN member_fee_charge_received.settlement_way_code IS '決済方法システムコード : 決済方法システムコード';
COMMENT ON TABLE member_genre IS '会員興味ジャンル';
COMMENT ON COLUMN member_genre.member_genre_code IS '会員興味ジャンルシステムコード : 会員興味ジャンルシステムコード';
COMMENT ON COLUMN member_genre.member_code IS '会員システムコード : 会員システムコード';
COMMENT ON COLUMN member_genre.genre_code IS 'ジャンルシステムコード : ジャンルシステムコード';
COMMENT ON TABLE member_group_mst IS '会員組織';
COMMENT ON COLUMN member_group_mst.member_group_code IS '会員組織システムコード : 会員組織システムコード';
COMMENT ON COLUMN member_group_mst.member_group_id IS '会員組織ID : 会員組織ID';
COMMENT ON COLUMN member_group_mst.member_group_name IS '会員組織名 : 会員組織名';
COMMENT ON COLUMN member_group_mst.sales_player_code IS '販売事業者システムコード : 販売事業者システムコード';
COMMENT ON TABLE member_mst IS '会員';
COMMENT ON COLUMN member_mst.member_code IS '会員システムコード : 会員システムコード';
COMMENT ON COLUMN member_mst.member_id IS '会員ID : 会員ID';
COMMENT ON COLUMN member_mst.member_group_code IS '会員組織システムコード : 会員組織システムコード';
COMMENT ON COLUMN member_mst.member_type_code IS '会員種別システムコード : 会員種別システムコード';
COMMENT ON COLUMN member_mst.representative_member_code IS '代表者会員システムコード : 代表者会員システムコード';
COMMENT ON COLUMN member_mst.family_name_kanji IS '名前（姓） : 名前（姓）';
COMMENT ON COLUMN member_mst.first_name_kanji IS '名前（名） : 名前（名）';
COMMENT ON COLUMN member_mst.family_name_kana IS '名前カナ（姓） : 名前カナ（姓）';
COMMENT ON COLUMN member_mst.first_name_kana IS '名前カナ（名） : 名前カナ（名）';
COMMENT ON COLUMN member_mst.zip IS '郵便番号 : 0010001のハイフンなしの形式で登録';
COMMENT ON COLUMN member_mst.pref_code IS '都道府県システムコード : 都道府県システムコード';
COMMENT ON COLUMN member_mst.city IS '市区町村 : 市区町村';
COMMENT ON COLUMN member_mst.address IS '番地 : 番地';
COMMENT ON COLUMN member_mst.building IS '建物名 : 建物名';
COMMENT ON COLUMN member_mst.tel IS '電話番号 : 電話番号';
COMMENT ON COLUMN member_mst.email IS 'メールアドレス : メールアドレス';
COMMENT ON COLUMN member_mst.gender IS '性別 : 性別';
COMMENT ON COLUMN member_mst.birthday IS '生年月日 : 生年月日';
COMMENT ON COLUMN member_mst.web_login_id IS 'WebログインID : WebログインID';
COMMENT ON COLUMN member_mst.web_login_password IS 'Webログインパスワード : Webログインパスワード';
COMMENT ON COLUMN member_mst.member_entry_date IS '入会年月日 : 入会年月日';
COMMENT ON COLUMN member_mst.updated_at IS '更新年月日 : 更新年月日';
COMMENT ON COLUMN member_mst.withdrawal_date IS '退会年月日 : 退会年月日';
COMMENT ON COLUMN member_mst.black_status IS 'ブラック状態 : ブラック状態';
COMMENT ON TABLE member_type_mst IS '会員種別';
COMMENT ON COLUMN member_type_mst.member_type_code IS '会員種別システムコード : 会員種別システムコード';
COMMENT ON COLUMN member_type_mst.member_type_id IS '会員組織ID : 会員組織ID';
COMMENT ON COLUMN member_type_mst.member_group_code IS '会員組織システムコード : 会員組織システムコード';
COMMENT ON COLUMN member_type_mst.member_group_name IS '会員種別名 : 会員種別名';
COMMENT ON COLUMN member_type_mst.member_id_prefix IS '会員ID生成用文字列 : 会員ID生成用文字列';
COMMENT ON COLUMN member_type_mst.paid_type IS '有料区分 : 有料区分';
COMMENT ON COLUMN member_type_mst.paid_period IS '有料期間 : 有料期間';
COMMENT ON COLUMN member_type_mst.member_rank IS '会員ランク : 会員ランク';
COMMENT ON COLUMN member_type_mst.member_fee IS '会費 : 会費';
COMMENT ON TABLE purchase_content IS '購入内容 : 抽選申込内容を記録';
COMMENT ON COLUMN purchase_content.purchase_content_code IS '購入内容システムコード';
COMMENT ON COLUMN purchase_content.purchase_code IS '購入システムコード : 購入システムコード';
COMMENT ON COLUMN purchase_content.t_entry_code IS '受付システムコード : 受付システムコード';
COMMENT ON COLUMN purchase_content.series_code IS '興行システムコード : 興行システムコード';
COMMENT ON COLUMN purchase_content.show_code IS '公演システムコード : 公演システムコード';
COMMENT ON COLUMN purchase_content.priority IS '優先順位 : 第一希望、第2希望などの順位';
COMMENT ON COLUMN purchase_content.ticketing_start_date IS '発券開始日時 : 発券開始日時';
COMMENT ON COLUMN purchase_content.ticketing_end_date IS '発券終了日時 : 発券終了日時';
COMMENT ON COLUMN purchase_content.seat_waritsuke_status IS '座席割付状態 : 座席割付状態';
COMMENT ON COLUMN purchase_content.lottery_status IS '抽選状態 : 0=未抽選、111=当選確定、121=落選確定、(21=落選未確定、11=当選未確定 ※票券のみ利用)';
COMMENT ON TABLE purchase_enter IS '入場情報';
COMMENT ON COLUMN purchase_enter.purchase_enter_code IS '購入入場システムコード : 購入入場システムコード';
COMMENT ON COLUMN purchase_enter.purchase_code IS '購入システムコード : 購入システムコード';
COMMENT ON COLUMN purchase_enter.show_seat_code IS '公演座席システムコード : 公演座席システムコード';
COMMENT ON COLUMN purchase_enter.enter_status IS '入場状態 : 入場状態';
COMMENT ON COLUMN purchase_enter.enter_date IS '入場日時 : 入場日時';
COMMENT ON COLUMN purchase_enter.eticket_id IS '電子チケットID : 電子チケットID';
COMMENT ON TABLE purchase_main IS '購入情報 : 一般販売、先行販売共通で使用する。';
COMMENT ON COLUMN purchase_main.purchase_code IS '購入システムコード : 購入システムコード';
COMMENT ON COLUMN purchase_main.purchase_id IS '購入ID(購入トランザクションID) : 購入ID(購入トランザクションID)';
COMMENT ON COLUMN purchase_main.sales_player_purchase_number IS '販売事業者購入番号 : 販売事業者購入番号';
COMMENT ON COLUMN purchase_main.sales_player_code IS '販売事業者システムコード : 販売事業者システムコード';
COMMENT ON COLUMN purchase_main.member_code IS '会員システムコード : 会員システムコード';
COMMENT ON COLUMN purchase_main.family_name_kanji IS '名前（姓） : 名前（姓）';
COMMENT ON COLUMN purchase_main.first_name_kanji IS '名前（名） : 名前（名）';
COMMENT ON COLUMN purchase_main.family_name_kana IS '名前カナ（姓） : 名前カナ（姓）';
COMMENT ON COLUMN purchase_main.first_name_kana IS '名前カナ（名） : 名前カナ（名）';
COMMENT ON COLUMN purchase_main.zip IS '郵便番号 : 0010001のハイフンなしの形式で登録';
COMMENT ON COLUMN purchase_main.pref_code IS '都道府県システムコード : 都道府県システムコード';
COMMENT ON COLUMN purchase_main.city IS '市区町村 : 市区町村';
COMMENT ON COLUMN purchase_main.address IS '番地 : 番地';
COMMENT ON COLUMN purchase_main.building IS '建物名 : 建物名';
COMMENT ON COLUMN purchase_main.tel IS '電話番号 : 電話番号';
COMMENT ON COLUMN purchase_main.email IS 'メールアドレス : メールアドレス';
COMMENT ON COLUMN purchase_main.gender IS '性別 : 性別';
COMMENT ON COLUMN purchase_main.birthday IS '生年月日 : 生年月日';
COMMENT ON COLUMN purchase_main.purchase_date IS '購入日時 : 購入日時';
COMMENT ON COLUMN purchase_main.settlement_way_code IS '決済方法システムコード : 決済方法システムコード';
COMMENT ON COLUMN purchase_main.settlement_fee IS '決済手数料 : 決済手数料';
COMMENT ON COLUMN purchase_main.receipt_total_fee_price IS '受取合計手数料 : 受取合計手数料';
COMMENT ON COLUMN purchase_main.charge_received_start_date IS '入金開始日時 : 入金開始日時';
COMMENT ON COLUMN purchase_main.charge_received_end_date IS '入金終了日時 : 入金終了日時';
COMMENT ON COLUMN purchase_main.reserve_canceled_date IS '予約流れ日時 : 予約流れ日時';
COMMENT ON COLUMN purchase_main.charge_received_status IS '入金状態 : 入金状態';
COMMENT ON COLUMN purchase_main.charge_received_date IS '入金日 : 入金日';
COMMENT ON COLUMN purchase_main.settlement_agent_order_number IS '決済代行会社取引番号 : 決済代行会社取引番号';
COMMENT ON COLUMN purchase_main.credit_auth_status IS 'クレジット与信状態 : クレジット与信状態';
COMMENT ON COLUMN purchase_main.cvs_payment_paper_number IS 'CVS払込票番号 : CVS払込票番号';
COMMENT ON COLUMN purchase_main.lottery_status IS '抽選状態 : 0=未抽選,1=抽選済み';
COMMENT ON COLUMN purchase_main.cancel_status IS '取消状態 : 取消状態';
COMMENT ON COLUMN purchase_main.cancel_date IS '取消日時 : 取消日時';
COMMENT ON COLUMN purchase_main.total_purchase_amount IS '購入合計金額 : 購入合計金額';
COMMENT ON COLUMN purchase_main.total_purchase_count IS '購入合計枚数 : 購入合計枚数';
COMMENT ON COLUMN purchase_main.total_system_charge IS 'システム合計利用料 : システム合計利用料';
COMMENT ON COLUMN purchase_main.total_service_charge IS 'サービス合計利用料 : サービス合計利用料';
COMMENT ON COLUMN purchase_main.purchase_flg IS '購入フラグ : 0=未購入、1=購入or当選 ※購入履歴に並べる';
COMMENT ON COLUMN purchase_main.lottery_application_flg IS '抽選申込フラグ : 0=通常購入、1=抽選申込 ※抽選申込履歴に並べる';
COMMENT ON COLUMN purchase_main.veritrans_credit_card_id IS 'ベリトランスクレジットカードID : ベリトランスのワンクリック継続課金サービスで採番されるカードID、決済時に会員NoとカードIDを指定して支払を行う。
抽選時のみ利用';
COMMENT ON TABLE purchase_refund IS '払戻情報';
COMMENT ON COLUMN purchase_refund.purchase_refund_code IS '購入払戻システムコード : 購入払戻システムコード';
COMMENT ON COLUMN purchase_refund.purchase_code IS '購入システムコード : 購入システムコード';
COMMENT ON COLUMN purchase_refund.show_seat_code IS '公演座席システムコード : 公演座席システムコード';
COMMENT ON COLUMN purchase_refund.refund_status IS '払戻状態 : 払戻状態';
COMMENT ON COLUMN purchase_refund.refund_date IS '払戻日時 : 払戻日時';
COMMENT ON COLUMN purchase_refund.refund_amount IS '払戻金額 : 払戻金額';
COMMENT ON TABLE purchase_seat IS '購入座席';
COMMENT ON COLUMN purchase_seat.purchase_seat_code IS '購入座席システムコード : 購入座席システムコード';
COMMENT ON COLUMN purchase_seat.purchase_shipping_code IS '送付先システムコード';
COMMENT ON COLUMN purchase_seat.purchase_content_code IS '購入内容システムコード';
COMMENT ON COLUMN purchase_seat.sales_player_seat_code IS '販売事業者座席コード : 販売事業者座席コード';
COMMENT ON COLUMN purchase_seat.show_seat_code IS '公演座席システムコード : 公演座席システムコード';
COMMENT ON COLUMN purchase_seat.seat_variation_code IS '席種システムコード : 席種システムコード';
COMMENT ON COLUMN purchase_seat.t_variation_code IS '券種システムコード : 券種システムコード';
COMMENT ON COLUMN purchase_seat.cvs_barcode_number IS 'CVSバーコード番号（セブン） : CVSバーコード番号（セブン）';
COMMENT ON COLUMN purchase_seat.system_charge IS 'システム利用料 : システム利用料';
COMMENT ON COLUMN purchase_seat.service_charge IS 'サービス利用料 : サービス利用料';
COMMENT ON COLUMN purchase_seat.ticket_price IS 'チケット金額 : チケット金額';
COMMENT ON TABLE purchase_shipping IS '送付先 : 購入チケットの送付先を記録する';
COMMENT ON COLUMN purchase_shipping.purchase_shipping_code IS '送付先システムコード';
COMMENT ON COLUMN purchase_shipping.purchase_code IS '購入システムコード : 購入システムコード';
COMMENT ON COLUMN purchase_shipping.member_code IS '会員システムコード : 会員システムコード';
COMMENT ON COLUMN purchase_shipping.family_name_kanji IS '名前（姓） : 名前（姓）';
COMMENT ON COLUMN purchase_shipping.first_name_kanji IS '名前（名） : 名前（名）';
COMMENT ON COLUMN purchase_shipping.family_name_kana IS '名前カナ（姓） : 名前カナ（姓）';
COMMENT ON COLUMN purchase_shipping.first_name_kana IS '名前カナ（名） : 名前カナ（名）';
COMMENT ON COLUMN purchase_shipping.zip IS '郵便番号 : 0010001のハイフンなしの形式で登録';
COMMENT ON COLUMN purchase_shipping.pref_code IS '都道府県システムコード : 都道府県システムコード';
COMMENT ON COLUMN purchase_shipping.city IS '市区町村 : 市区町村';
COMMENT ON COLUMN purchase_shipping.address IS '番地 : 番地';
COMMENT ON COLUMN purchase_shipping.building IS '建物名 : 建物名';
COMMENT ON COLUMN purchase_shipping.tel IS '電話番号 : 電話番号';
COMMENT ON COLUMN purchase_shipping.email IS 'メールアドレス : メールアドレス';
COMMENT ON COLUMN purchase_shipping.ticketing_status IS '発券状態 : 発券状態';
COMMENT ON COLUMN purchase_shipping.ticketing_date IS '発券日 : 発券日';
COMMENT ON COLUMN purchase_shipping.cvs_exchage_paper_number IS 'CVS引換票番号 : CVS引換票番号';
COMMENT ON COLUMN purchase_shipping.delivery_status IS '配送状態 : 配送状態';
COMMENT ON COLUMN purchase_shipping.delivery_date IS '配送日時 : 配送日時';
COMMENT ON COLUMN purchase_shipping.delivery_tracking_number IS '配送追跡番号 : 配送追跡番号';
COMMENT ON COLUMN purchase_shipping.receive_way_code IS '受取方法システムコード : 受取方法システムコード';
COMMENT ON COLUMN purchase_shipping.settlement_fee IS '受取手数料 : 受取手数料';



