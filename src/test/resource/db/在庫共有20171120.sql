
/* Drop Tables */

DROP TABLE haiken_seat CASCADE CONSTRAINTS;
DROP TABLE seat_operation_hisotry CASCADE CONSTRAINTS;




/* Create Tables */

CREATE TABLE haiken_seat
(
	-- 配券時に在庫共有側で採番する。
	-- 配券座席の座席IDとは、仮確保時に結びつく。
	seat_id varchar2(20 char) NOT NULL,
	organizer_code number(19,0) NOT NULL,
	haiken_code number(19,0) NOT NULL,
	t_entry_code number(19,0) NOT NULL,
	show_code number(19,0) NOT NULL,
	area_code number(19,0) NOT NULL,
	block_code number(19,0) NOT NULL,
	sales_player_code number(19,0) NOT NULL,
	-- 仮確保時に採番する。PG名+座席ID
	sales_player_seat_code number(19,0),
	seat_variation_code number(19,0) NOT NULL,
	t_variation_code number(19,0) NOT NULL,
	show_seat_code number(19,0),
	seat_status number(10,0),
	PRIMARY KEY (seat_id)
);


-- 在庫共有内でのチケット操作が行われた際に逐次記録する。
CREATE TABLE seat_operation_hisotry
(
	operation_log_id number(10,0) DEFAULT 0 NOT NULL,
	ticket_operator clob NOT NULL,
	operation_date date NOT NULL,
	t_entry_code number(19,0) DEFAULT 0 NOT NULL,
	show_code number(19,0) NOT NULL,
	seat_variation_code number(19,0) NOT NULL,
	t_variation_code number(19,0),
	ticket_operation clob,
	deleted number(1),
	PRIMARY KEY (operation_log_id)
);



/* Comments */

COMMENT ON COLUMN haiken_seat.seat_id IS '配券時に在庫共有側で採番する。
配券座席の座席IDとは、仮確保時に結びつく。';
COMMENT ON COLUMN haiken_seat.sales_player_seat_code IS '仮確保時に採番する。PG名+座席ID';
COMMENT ON TABLE seat_operation_hisotry IS '在庫共有内でのチケット操作が行われた際に逐次記録する。';



