
/* Drop Triggers */

DROP TRIGGER TRI_mail_template_mail_template_code;



/* Drop Tables */

DROP TABLE attachment_file CASCADE CONSTRAINTS;
DROP TABLE mail_template CASCADE CONSTRAINTS;
DROP TABLE send_to CASCADE CONSTRAINTS;
DROP TABLE send_log CASCADE CONSTRAINTS;



/* Drop Sequences */

DROP SEQUENCE SEQ_mail_template_mail_template_code;




/* Create Sequences */

CREATE SEQUENCE SEQ_mail_template_mail_template_code INCREMENT BY 1 START WITH 1;



/* Create Tables */

CREATE TABLE attachment_file
(
	send_log_id number(10,0) DEFAULT 0 NOT NULL,
	-- base64エンコード？
	attachment_file clob
);


CREATE TABLE mail_template
(
	mail_template_code number(19,0) NOT NULL,
	-- メールタイトル
	subject varchar2(2000),
	body_html clob,
	body_text clob,
	-- 送信元はFromとReturn-Pathに設定される
	-- 
	mail_from varchar2(100),
	-- 送信元名
	name_from varchar2(2000) DEFAULT '' NOT NULL,
	-- 登録者
	organizer_code number(19,0) NOT NULL,
	PRIMARY KEY (mail_template_code)
);


CREATE TABLE send_log
(
	send_log_id number(10,0) DEFAULT 0 NOT NULL,
	mail_from varchar2(100) NOT NULL,
	subject varchar2(2000) NOT NULL,
	-- SHA256でハッシュ化?
	body_html clob,
	-- SHA256でハッシュ化？
	body_text clob,
	send_time timestamp with time zone,
	-- 0:保留,1:送信済み
	-- などを規定
	send_status number(10,0),
	PRIMARY KEY (send_log_id)
);


CREATE TABLE send_to
(
	send_log_id number(10,0) DEFAULT 0 NOT NULL,
	mail_to varchar2(100) NOT NULL,
	-- To:1,CC:2,BCC:3
	to_type number(10,0) DEFAULT 1 NOT NULL,
	name_to varchar2(2000) DEFAULT '' NOT NULL
);



/* Create Foreign Keys */

ALTER TABLE attachment_file
	ADD FOREIGN KEY (send_log_id)
	REFERENCES send_log (send_log_id)
;


ALTER TABLE send_to
	ADD FOREIGN KEY (send_log_id)
	REFERENCES send_log (send_log_id)
;



/* Create Triggers */

CREATE OR REPLACE TRIGGER TRI_mail_template_mail_template_code BEFORE INSERT ON mail_template
FOR EACH ROW
BEGIN
	SELECT SEQ_mail_template_mail_template_code.nextval
	INTO :new.mail_template_code
	FROM dual;
END;

/




/* Comments */

COMMENT ON COLUMN attachment_file.attachment_file IS 'base64エンコード？';
COMMENT ON COLUMN mail_template.subject IS 'メールタイトル';
COMMENT ON COLUMN mail_template.mail_from IS '送信元はFromとReturn-Pathに設定される
';
COMMENT ON COLUMN mail_template.name_from IS '送信元名';
COMMENT ON COLUMN mail_template.organizer_code IS '登録者';
COMMENT ON COLUMN send_log.body_html IS 'SHA256でハッシュ化?';
COMMENT ON COLUMN send_log.body_text IS 'SHA256でハッシュ化？';
COMMENT ON COLUMN send_log.send_status IS '0:保留,1:送信済み
などを規定';
COMMENT ON COLUMN send_to.to_type IS 'To:1,CC:2,BCC:3';



