package jp.co.bbtower.tdc;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import jp.co.bbtower.tdc.error.ApiError;

public class Util4Test {

	/**
	 * オブジェクトをリフレクションで取得するメソッドです。 <br/>
	 * 失敗した場合は処理を停止するためにRuntimeExceptionを発生させます。<br/>
	 * <br/>
	 * 使用例(ResponseEntity<?>からApiError内のエラーコードを取得する場合):<br/>
	 * Util4Test.reflect("jp.co.bbtower.tdc.error.ApiError$ErrorStructure",
	 * ((ApiError) responseEntity.getBody()).getErrors().get(0), "getCode", null,
	 * null);
	 *
	 * @param qualifiedClassNm
	 *            クラスの完全修飾名
	 * @param target
	 *            操作対象オブジェクト
	 * @param methodNm
	 *            メソッド名
	 * @param methodParametertypes
	 *            メソッド引数型
	 * @param methodParameters
	 *            メソッド引数
	 * @return メソッド戻り値
	 */
	public static Object reflect(String qualifiedClassNm, Object target, String methodNm, Class[] methodParametertypes,
			Object[] methodParameters) {
		try {
			Class errorStructure = ClassLoader.getSystemClassLoader().loadClass(qualifiedClassNm);

			// 名前でメソッドを指定、アクセス許可にする
			Method getCode = errorStructure.getDeclaredMethod(methodNm, methodParametertypes);
			getCode.setAccessible(true);
			return getCode.invoke(target, methodParameters);

		} catch (Exception e) {
			System.out.println(e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * ApiError.ErrorStructure.getCode()の値を返します。
	 *
	 * @param target
	 * @return
	 */
	public static String getApiErrorCode(ResponseEntity target) {
		return (String) reflect("jp.co.bbtower.tdc.error.ApiError$ErrorStructure",
				((ApiError) target.getBody()).getErrors().get(0), "getCode", null, null);
	}

	/**
	 * ApiError.ErrorStructure.getMessage()の値を返します。
	 *
	 * @param target
	 * @return
	 */
	public static String getApiErrorMessage(ResponseEntity target) {
		return (String) reflect("jp.co.bbtower.tdc.error.ApiError$ErrorStructure",
				((ApiError) target.getBody()).getErrors().get(0), "getMessage", null, null);
	}

	/**
	 * DBに入っているblobデータを比較用Byte[]に変換します。
	 * 例："07 0D EA 4E"→{7,13,-22,78}
	 * @return
	 *
	 */
	public static byte[] makeBlobData(String arg) {
		String[] argArr = arg.split(" ");
		List<Byte> byteList = new ArrayList<>();
		for (String a : argArr) {
			byteList.add((byte) Integer.parseInt(a, 16));
		}
		byte[] result = new byte[byteList.size()];
		for (int i = 0; i < byteList.size(); i++) {
			result[i] = byteList.get(i);
		}
		return result;
	}

}
