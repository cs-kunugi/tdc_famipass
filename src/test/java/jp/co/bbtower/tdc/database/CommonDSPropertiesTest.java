/*
 * Copyright.
 */
package jp.co.bbtower.tdc.database;

import java.lang.reflect.Field;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * CommonDSPropertiesのテスト。
 * 
 * @author cs
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CommonDSPropertiesTest {

	/**
	 * getDatasource() メソッドの正常系テスト。
	 * 
	 * @throws java.lang.NoSuchFieldException
	 * @throws java.lang.IllegalAccessException
	 */
	@Test
	public void testGetDatasource() throws NoSuchFieldException, IllegalAccessException {

		CommonDSProperties instance = new CommonDSProperties();
		Field field = instance.getClass().getDeclaredField("datasource");

		DataSource expResult = new DataSource();

		field.setAccessible(true);
		field.set(instance, expResult);

		DataSource result = instance.getDatasource();

		assertEquals(expResult, result);
	}

	/**
	 * setDatasource() メソッドの正常系テスト
	 * 
	 * @throws java.lang.NoSuchFieldException
	 * @throws java.lang.IllegalAccessException
	 */
	@Test
	public void testSetDatasource() throws NoSuchFieldException, IllegalAccessException {

		DataSource datasource = new DataSource();
		CommonDSProperties instance = new CommonDSProperties();
		instance.setDatasource(datasource);

		Field field = instance.getClass().getDeclaredField("datasource");
		field.setAccessible(true);

		assertEquals(datasource, (DataSource) field.get(instance));
	}

}
