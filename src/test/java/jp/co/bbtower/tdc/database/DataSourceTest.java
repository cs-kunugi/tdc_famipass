/*
 * Copyright.
 */
package jp.co.bbtower.tdc.database;

import java.lang.reflect.Field;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * DataSourceクラステスト。
 * 
 * @author cs
 */
public class DataSourceTest {

	/**
	 * getSchema()の正常系テスト。
	 * 
	 * @throws java.lang.NoSuchFieldException
	 * @throws java.lang.IllegalAccessException
	 */
	@Test
	public void testGetSchema() throws NoSuchFieldException, IllegalAccessException {

		DataSource instance = new DataSource();
		Field field = instance.getClass().getDeclaredField("schema");

		String expResult = "test";

		field.setAccessible(true);
		field.set(instance, expResult);

		String result = instance.getSchema();

		assertEquals(expResult, result);
	}

	/**
	 * getData()の正常系テスト。
	 * 
	 * @throws java.lang.NoSuchFieldException
	 * @throws java.lang.IllegalAccessException
	 */
	@Test
	public void testGetData() throws NoSuchFieldException, IllegalAccessException {

		DataSource instance = new DataSource();
		Field field = instance.getClass().getDeclaredField("data");

		String expResult = "test";

		field.setAccessible(true);
		field.set(instance, expResult);

		String result = instance.getData();

		assertEquals(expResult, result);
	}

	/**
	 * setSchema()の正常系テスト。
	 * 
	 * @throws java.lang.NoSuchFieldException
	 * @throws java.lang.IllegalAccessException
	 */
	@Test
	public void testSetSchema() throws NoSuchFieldException, IllegalAccessException {

		String schema = "test";
		DataSource instance = new DataSource();
		instance.setSchema(schema);

		Field field = instance.getClass().getDeclaredField("schema");
		field.setAccessible(true);

		assertEquals(schema, (String) field.get(instance));
	}

	/**
	 * setData()の正常系テスト。
	 * 
	 * @throws java.lang.NoSuchFieldException
	 * @throws java.lang.IllegalAccessException
	 */
	@Test
	public void testSetData() throws NoSuchFieldException, IllegalAccessException {

		String data = "test";
		DataSource instance = new DataSource();
		instance.setData(data);

		Field field = instance.getClass().getDeclaredField("data");
		field.setAccessible(true);

		assertEquals(data, (String) field.get(instance));
	}

}
