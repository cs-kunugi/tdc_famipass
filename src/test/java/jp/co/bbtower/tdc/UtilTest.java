package jp.co.bbtower.tdc;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import org.junit.Test;

/**
 * Utilのテストクラス。
 *
 */
public class UtilTest {

	/**
	 * changeStrToDateの正常系テストを行います。
	 *
	 * @throws ParseException
	 */
	@Test
	public void changeStrToDate_OK() throws ParseException {
		assertEquals(new SimpleDateFormat("yyyy年M月d日 H時m分s秒").parse("2017年10月3日 3時52分10秒"),
				Util.changeStrToDate("2017年10月3日 3時52分10秒", "yyyy年M月d日 H時m分s秒"));
	}

	/**
	 * changeStrToDateのエラーテストを行います。
	 *
	 * @throws ParseException
	 */
	@Test
	public void changeStrToDate_NG() throws ParseException {
		assertEquals(null, Util.changeStrToDate("2017年10月3日 3時52分10秒", "yyyy/M/d H/m/s"));
	}

	/**
	 * checkStrToDateの正常系テストを行います。
	 *
	 * @throws ParseException
	 */
	@Test
	public void checkStrToDate_true() throws ParseException {
		assertEquals(true, Util.checkStrToDate("2017年10月3日 3時52分10秒", "yyyy年M月d日 H時m分s秒"));
	}

	/**
	 * checkStrToDateのエラーテストを行います。
	 *
	 * @throws ParseException
	 */
	@Test
	public void checkStrToDate_false() throws ParseException {
		assertEquals(false, Util.checkStrToDate("2017年10月3日 3時52分10秒", "yyyy/M/d H/m/s"));
	}

	/**
	 * changeDateToStrのテストを行います。
	 *
	 * @throws ParseException
	 */
	@Test
	public void changeDateToStr_OK() throws ParseException {
		assertEquals("2017年10月3日 3時52分10秒", Util.changeDateToStr(
				new SimpleDateFormat("yyyy年M月d日 H時m分s秒").parse("2017年10月3日 3時52分10秒"), "yyyy年M月d日 H時m分s秒"));
	}

	/**
	 * nullToEmptyがnullの場合のテストを行います。
	 */
	@Test
	public void nullToEmpty_null() {
		assertEquals("", Util.nullToEmpty(null));
	}

	/**
	 * nullToEmptyがnullでない場合のテストを行います。
	 */
	@Test
	public void nullToEmpty_notNull() {
		assertEquals("notNull", Util.nullToEmpty("notNull"));
	}

	/**
	 * emptyToNullが空文字の場合のテストを行います。
	 */
	@Test
	public void emptyToNull_empty() {
		assertEquals(null, Util.emptyToNull(""));
	}

	/**
	 * nullToEmptyがnullでない場合のテストを行います。
	 */
	@Test
	public void emptyToNull_notNull() {
		assertEquals("notNull", Util.emptyToNull("notNull"));
	}

	/**
	 * nullToEmptyがnullでない場合のテストを行います。
	 */
	@Test
	public void emptyToNull_null() {
		assertEquals(null, Util.emptyToNull(null));
	}

	/**
	 * isEmptyOrNullListがnullである場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullList_null() {
		assertEquals(true, Util.isEmptyOrNullList(null));
	}

	/**
	 * isEmptyOrNullListが空文字である場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullList_empty() {
		assertEquals(true, Util.isEmptyOrNullList(new ArrayList<>()));
	}

	/**
	 * isEmptyOrNullListが空でない場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullList_notEmpty() {
		assertEquals(false, Util.isEmptyOrNullList(new ArrayList<>(Arrays.asList("notEmpty"))));
	}

	/**
	 * isEmptyOrNullSetがnullである場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullSet_null() {
		assertEquals(true, Util.isEmptyOrNullSet(null));
	}

	/**
	 * isEmptyOrNullSetが空文字である場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullSet_empty() {
		assertEquals(true, Util.isEmptyOrNullSet(new HashSet<>()));
	}

	/**
	 * isEmptyOrNullSetが空でない場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullSet_notEmpty() {
		assertEquals(false, Util.isEmptyOrNullSet(new HashSet<>(Arrays.asList("notEmpty"))));
	}

	/**
	 * isEmptyOrNullStrがnullである場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullStr_null() {
		assertEquals(true, Util.isEmptyOrNullStr(null));
	}

	/**
	 * isEmptyOrNullStrが空文字である場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullStr_empty() {
		assertEquals(true, Util.isEmptyOrNullStr(""));
	}

	/**
	 * isEmptyOrNullStrが空でない文字列である場合のテストを行います。
	 */
	@Test
	public void isEmptyOrNullStr_notEmpty() {
		assertEquals(false, Util.isEmptyOrNullStr("notEmpty"));
	}

	/**
	 * fixMailのテストを行います。
	 */
	@Test
	public void fixMail() {
		assertEquals("fixmail_uppercase@gmail.com", Util.fixMail("fixMail_UPPERCASE@gmail.com"));
	}

	/**
	 * fixGmailがgmail.comの形であり、編集する場合のテストを行います
	 */
	@Test
	public void fixGmail_gmail() {
		assertEquals("fixGmailDot@gmail.com", Util.fixGmail("fixGmail.D.o.t.+plusString@gmail.com"));
	}

	/**
	 * fixGmailがgmail.comの形であり、編集しない場合のテストを行います
	 */
	@Test
	public void fixGmail_gmail_noEdit() {
		assertEquals("fixGmail@gmail.com", Util.fixGmail("fixGmail@googlemail.com"));
	}

	/**
	 * fixGmailがgmail.comの形でない場合のテストを行います
	 */
	@Test
	public void fixGmail_notGmail() {
		assertEquals("fixGmail.d.o.t.+plusString@docomo.ne.jp",
				Util.fixGmail("fixGmail.d.o.t.+plusString@docomo.ne.jp"));
	}

	/**
	 * isGmailがgmail.comの形である場合のテストを行います
	 */
	@Test
	public void isGmail_gmail_true() {
		assertEquals(true, Util.isGmail("isGmail@gmail.com"));
	}

	/**
	 * isGmailがgooglemail.comの形である場合のテストを行います
	 */
	@Test
	public void isGmail_googlemail_true() {
		assertEquals(true, Util.isGmail("isGmail@googlemail.com"));
	}

	/**
	 * isGmailがgoogleのメールでない場合のテストを行います
	 */
	@Test
	public void isGmail_false() {
		assertEquals(false, Util.isGmail("isGmail@docomo.ne.jp"));
	}

	/**
	 *isEmptyのテストを行います
	 */
	@Test
	public void isEmpty_null() {
		assertEquals(true, Util.isEmpty(null));
	}

	/**
	 *isEmptyのテストを行います
	 */
	@Test
	public void isEmpty_empty() {
		assertEquals(true, Util.isEmpty(""));
	}

	/**
	 *isEmptyのテストを行います
	 */
	@Test
	public void isEmpty_false() {
		assertEquals(false, Util.isEmpty("notNull"));
	}

	/**
	 *encodePasswordのテストを行います
	 */
	@Test
	public void encodePassword() {
		String pass = "pass";
		Date key = new Date();
		String result1 = Util.encodePassword(pass, key);
		String result2 = Util.encodePassword(pass, key);
		assertEquals(result1, result2);
		System.out.println(result1);
		System.out.println(result2);

		Calendar cl = Calendar.getInstance();
		cl.setTime(key);
		cl.add(Calendar.MONTH, 1);
		key = cl.getTime();
		String result3 = Util.encodePassword(pass, key);
		assertNotEquals(result1, result3);
		System.out.println(result3);
	}

	/**
	 *isIntegerのテストを行います
	 */
	@Test
	public void isInteger_true() {
		assertEquals(true, Util.isInteger("0"));
	}

	/**
	 *isIntegerのテストを行います
	 */
	@Test
	public void isInteger_false() {
		assertEquals(false, Util.isInteger("-10"));
	}

	/**
	 *isIntegerのテストを行います
	 */
	@Test
	public void isInteger_false_2() {
		assertEquals(false, Util.isInteger("***"));
	}

	@Test
	public void divide() {
		assertEquals(
				Arrays.asList(
						Arrays.asList(1, 2, 3),
						Arrays.asList(4, 5, 6),
						Arrays.asList(7, 8, 9),
						Arrays.asList(10)),
				Util.divide(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), 3));
	}
}
