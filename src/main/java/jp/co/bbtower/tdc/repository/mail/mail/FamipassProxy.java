/*
 * Copyright.
 */
package jp.co.bbtower.tdc.repository.mail.mail;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * メール操作用リポジトリークラスです。
 *
 * @author cs
 */
@Repository
@Mapper
public interface FamipassProxy {
	//TODO
	/**
	 * ログを書き込みます。
	 *
	 * @param sendLog
	 * @return 登録件数
	 */
	public int addLog(String sendLog);
}
