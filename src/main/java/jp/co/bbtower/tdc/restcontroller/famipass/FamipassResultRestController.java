/*
 * Copyright.
 */
package jp.co.bbtower.tdc.restcontroller.famipass;

import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

/**
 * ファミパス結果取得APIコントローラークラスです。
 *
 * @author cs
 */
@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class FamipassResultRestController {

	/**
	 * ファミパスから非同期で返却されてくる通知データを取得します。
	 * @return
	 */
	@RequestMapping(path = "famipass-result/{receiptNo}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get(@PathVariable(value = "receiptNo") String receiptNo,
			@RequestParam(value = "checked", defaultValue = "false") Boolean checked) {
		//TODO　未実装
		throw new UnsupportedOperationException("Not supported yet.");
	}

	/**
	 * ファミパスから非同期で返却されてくる通知データを一覧で取得します。
	 * @return
	 */
	@RequestMapping(path = "settlement-result", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getList(@RequestParam(value = "organizerCode", required = false) Integer organizerCode,
			@RequestParam(value = "salesPlayerCode", required = false) Integer salesPlayerCode,
			@RequestParam(value = "checked", defaultValue = "true") Boolean checked,
			@RequestParam(value = "includeChecked", defaultValue = "false") Boolean includeChecked,
			@RequestParam(value = "responseAtFrom", required = false) String responseAtFrom,
			@RequestParam(value = "responseAtTo", required = false) String responseAtTo,
			@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "offset", defaultValue = "0") Integer offset,
			@RequestParam(value = "orderby", defaultValue = "receiptDate desc") String orderby) {
		//TODO　未実装
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
