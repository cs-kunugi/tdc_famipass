/*
 * Copyright.
 */
package jp.co.bbtower.tdc.restcontroller.famipass;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import jp.co.bbtower.tdc.dto.FamipassProxyDto;
import jp.co.bbtower.tdc.service.FamipassProxyService;

/**
 * ファミパス送信コントローラークラスです。
 *
 * @author cs
 */
@RestController
@Scope(WebApplicationContext.SCOPE_REQUEST)
@RequestMapping(path = "famipass-proxy")
public class FamipassProxyRestController {

	@Autowired
	private FamipassProxyService service;

	/**
	 * ファミパスへのリクエストを中継し、ファミパスの処理結果を同期で返却します。
	 * リクエスト内容は、認証情報以外はファミパスチケット連携に完全に準拠します。
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> post(@RequestBody(required = true) @Valid FamipassProxyDto dto, Errors errors) {
		//TODO　未実装
		throw new UnsupportedOperationException("Not supported yet.");
		//return service.relayFamipass(dto);
	}

}
