/**
 * コントローラールートパッケージ。
 *
 * ファミパス連携における各機能のコントローラーを集めたパッケージです。
 */
package jp.co.bbtower.tdc.restcontroller;