/*
 * Copyright.
 */
package jp.co.bbtower.tdc.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * ファミパスプレビューコントローラークラス データ転送オブジェクト。
 *
 * @author cs
 */
@Data
public class FamipassPreviewMasterDto implements Serializable {
	/**使用APIコード*/
	@NotNull
	private Integer apiCode;
	/**クエリ*/
	@NotNull
	private List<FamipassPreviewQueryDto> query;
	/**興行主システムコード*/
	private Integer organizerCode;
}
