/*
 * Copyright.
 */
package jp.co.bbtower.tdc.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * ファミパス送信コントローラークラス データ転送オブジェクト。
 *
 * @author cs
 */
@Data
public class FamipassProxyDto implements Serializable {
	/**使用APIコード*/
	@NotNull
	private Integer apiCode;
	/**クエリ*/
	@NotNull
	private List<FamipassProxyQueryDto> query;
	/**興行主システムコード*/
	private Integer organizerCode;
	/**販売事業者システムコード*/
	private Integer salesPlayerCode;
}
