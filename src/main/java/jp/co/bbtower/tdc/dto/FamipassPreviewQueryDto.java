package jp.co.bbtower.tdc.dto;

import java.util.List;

import lombok.Data;

/**
 * ファミパス送信コントローラー 内query データ転送オブジェクト。
 *
 * @author cs
 */
@Data
public class FamipassPreviewQueryDto {
	/**デフォルト値マスタを登録する対象のテンプレート*/
	private String ticketTemplate;
	/**マスタ名*/
	private String ticketMaster;
	/** 内容 */
	private List<FamipassTiketField> tiketField;
}
