/*
 * Copyright.
 */
package jp.co.bbtower.tdc.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * ファミパス送信コントローラークラス 内query。
 *
 * @author cs
 */
@Data
public class FamipassProxyQueryDto implements Serializable {
	/**サービス区分*/
	@Max(1)
	@NotNull
	private Integer serviceType;
	/**登録区分*/
	@Max(1)
	@NotNull
	private Integer registType;
	/**収納番号*/
	@Max(20)
	private String receiptNo;
	/**問合せ先名称*/
	@Max(60)
	private String inquiryName;
	/**問合せ先電話番号*/
	@Max(13)
	private String inquiryTel;
	/**問合せ受付時間(開始)*/
	@Max(5)
	private String inquiryStartTime;
	/**問合せ受付時間(終了)*/
	@Max(5)
	private String inquiryEndTime;
	/**領収証フリー記述１*/
	@Max(120)
	private String inquiryFree1;
	/**領収証フリー記述２*/
	@Max(120)
	private String inquiryFree2;
	/**お客様氏名*/
	@Max(40)
	private String name;
	/**フリガナ*/
	@Max(30)
	private String kana;
	/**電話番号*/
	@Max(13)
	private String phoneNo;
	/**お支払い金額*/
	@Max(999999)
	private Integer payment;
	/**お支払い期限*/
	private String dateOfExpiry;
	/**発券開始日*/
	@JsonFormat(pattern = "yyyyMMdd")
	private String dateOfTicketStart;
	/**発券終了日*/
	@JsonFormat(pattern = "yyyyMMdd")
	private String dateOfTicketEnd;
	/**予約番号*/
	@Max(20)
	private String reservationNo;
	/**メールアドレス*/
	@Max(50)
	private String mailAddress;
	/**会員番号*/
	@Max(20)
	private String membershipNo;
	/**お支払い内容１*/
	@Max(38)
	private String free1;
	/**お支払い内容２*/
	@Max(38)
	private String free2;
	/**お支払い内容３*/
	@Max(38)
	private String free3;
	/**お支払い内容４*/
	@Max(38)
	private String free4;
	/**お支払い内容５*/
	@Max(38)
	private String free5;
	/**お支払い内容６*/
	@Max(38)
	private String free6;
	/**お支払い内容7*/
	@Max(38)
	private String free7;
	/**お支払い内容８*/
	@Max(38)
	private String free8;
	/**ご案内１*/
	@Max(100)
	private String notice1;
	/**ご案内２*/
	@Max(100)
	private String notice2;
	/**ご案内３*/
	@Max(100)
	private String notice3;
	/**ご案内４*/
	@Max(100)
	private String notice4;
	/**ご案内５*/
	@Max(100)
	private String notice5;
	/**ご案内６*/
	@Max(100)
	private String notice6;
	/**ご案内７*/
	@Max(100)
	private String notice7;
	/**ご案内８*/
	@Max(100)
	private String notice8;
	/**ご案内９*/
	@Max(100)
	private String notice9;
	/**ご案内１０*/
	@Max(100)
	private String notice10;

	/**チケットの総枚数(<=22)*/
	private Integer ticketNum;
	/**チケット情報*/
	private List<FamipassTiketRequestDto> ticketRequest;

}
