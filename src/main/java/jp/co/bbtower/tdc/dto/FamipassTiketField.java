package jp.co.bbtower.tdc.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * ファミパス送信コントローラークラス 内query 内TiketRequest 内TiketField。
 *
 * @author cs
 */
@Data
public class FamipassTiketField {
	/**フィールド名*/
	@NotNull
	private String fieldName;
	/**フィールド値*/
	@NotNull
	private String fieldValue;
	/**フィールドアライン*/
	private String fieldAlign;

}
