/*
 * Copyright.
 */
package jp.co.bbtower.tdc.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * ファミパステンプレ―トマスタ送信コントローラークラス データ転送オブジェクト。
 *
 * @author cs
 */
@Data
public class FamipassProxyMasterDto implements Serializable {
	/**チケットテンプレート*/
	@Max(40)
	private String ticketTemplate;
	/**マスタ名*/
	@Max(40)
	private String masterName;
	/**登録区分*/
	@Max(1)
	private String registType;
	/**内容*/
	@NotNull
	private List<FamipassTiketField> ticketField;
}
