package jp.co.bbtower.tdc.dto;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * ファミパス送信コントローラークラス 内query 内TiketRequest。
 *
 * @author cs
 */
@Data
public class FamipassTiketRequestDto {
	/**登録区分*/
	@Max(40)
	@NotNull
	private String ticketTemplate;
	/**収納番号*/
	@Max(40)
	@NotNull
	private String ticketMaster;
	/**内容*/
	@NotNull
	private List<FamipassTiketField> ticketField;
}
