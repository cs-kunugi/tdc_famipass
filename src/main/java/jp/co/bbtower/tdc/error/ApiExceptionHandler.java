/*
 * Copyright.
 */
package jp.co.bbtower.tdc.error;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * APIエラーハンドラーです。
 *
 * 各エラーの挙動を定義します。 APIで問題が発生するエラーは全てこのクラスにて内包します。
 *
 * @author cs
 */
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * メッセージソース。
	 */
	@Autowired
	MessageSource messageSource;

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		ApiError error;
		if (body != null && body instanceof ApiError) {
			error = (ApiError) body;
		} else {
			error = new ApiError("E9999", ex.getMessage());
		}

		return super.handleExceptionInternal(ex, error, headers, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		final BindingResult bindingResult = ex.getBindingResult();
		final ApiError error = new ApiError();

		bindingResult.getGlobalErrors().forEach((globalError) -> {
			error.put("E4000", globalError.getDefaultMessage());
		});

		bindingResult.getFieldErrors().forEach((fieldError) -> {
			error.put("E4000",
					"\"" + fieldError.getField() + "\" - " + fieldError.getDefaultMessage());
		});

		return super.handleExceptionInternal(ex, error, headers, HttpStatus.OK, request);
	}

	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		final BindingResult bindingResult = ex.getBindingResult();
		final ApiError error = new ApiError();

		bindingResult.getGlobalErrors().forEach((globalError) -> {
			error.put("E4000", globalError.getDefaultMessage());
		});

		bindingResult.getFieldErrors().forEach((fieldError) -> {
			error.put("E4000",
					"\"" + fieldError.getField() + "\" - " + fieldError.getDefaultMessage());
		});

		return super.handleExceptionInternal(ex, error, headers, HttpStatus.OK, request);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		final Throwable cause = ex.getCause();

		if (cause instanceof JsonMappingException) {

			final JsonMappingException jmex = (JsonMappingException) cause;
			final ApiError error = new ApiError("E4000",
					"\"" + getFieldInJsonMappingException(jmex) + "\" - " + jmex.getOriginalMessage());

			return super.handleExceptionInternal(ex, error, headers, HttpStatus.OK, request);

		} else {

			final ApiError error = new ApiError("E4000", ex.getMessage());
			return super.handleExceptionInternal(ex, error, headers, HttpStatus.OK, request);

		}
	}

	/**
	 * JsonMappingExceptionからエラーとなったフィールドを取得します。
	 *
	 * @param jmex
	 * @return エラーとなったフィールド名
	 */
	private String getFieldInJsonMappingException(final JsonMappingException jmex) {
		String field = "";
		Matcher m = Pattern.compile("\\[\"(.+)\"\\]").matcher(jmex.getPathReference());
		if (m.find()) {
			field = m.group(m.groupCount());
		}
		return field;
	}

	/**
	 * 変換エラーハンドラー。
	 *
	 * @param ex MethodArgumentTypeMismatchException
	 * @param request リクエストデータ
	 * @return エラーデータ。
	 */
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
			WebRequest request) {
		final String message = "\"" + ex.getName() + "\" - should be of type " + ex.getRequiredType().getSimpleName();
		final ApiError error = new ApiError("E4000", message);
		return handleExceptionInternal(ex, error, null, HttpStatus.BAD_REQUEST, request);
	}
}
