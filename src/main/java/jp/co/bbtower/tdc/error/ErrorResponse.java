package jp.co.bbtower.tdc.error;

/**
 * APIエラーを定義するEnumです。
 *
 */
public enum ErrorResponse {
	/** 必須項目 */
	REQUIRED_ITEM(400, "4001", "{0} must be specified.", "{0}は必須項目です。"),
	/** カナ */
	KATAKANA(400, "4002", "{0} must be kata-kana characters. ", "{0}はカタカナで指定してください。"),
	/** かな */
	HIRAGANA(400, "4003", "{0} must be hira-kana characters. ", "{0}はひらがなで指定してください。"),
	/** 数字英数 */
	NUMERIC_ALPHANUMERIC(400, "4004", "{0} must be alphanumeric characters.", "{0}は英数字で入力してください。"),
	/** 数字 */
	NUMERIC(400, "4005", "{0} must be numeric characters.", "{0}は数字で入力してください。"),
	/** 文字数 */
	LENGTH(400, "4006", "{0} must be in {1} characters or less.", "{0}は{1}文字以内で入力してください。"),
	/** 桁数 */
	DIGIT_NUMBER(400, "4007", "{0} must be in {1} digits or less.", "{0}は{1}桁以内で入力してください。"),
	/** 数値範囲 */
	NUMBER_RANGE(400, "4008", "{0} must be between {1} and {2}.", "{0}は{1}以上、{2}以下で入力してください。"),
	/** 文字形式 */
	CHARACTER_FORMAT(400, "4009", "{0} does not match character pattern.", "{0}は形式が一致しません。"),
	/** 外部キー確認 */
	EXTERNAL_KEY_CONFIRMATION(400, "4010", "{0} does not exists in foreign key.", "{0}は外部キーに存在しません。"),
	/** 会員認証 */
	MEMBERS_AUTHORIZATION(200, "4011", "Login id or password are invalid.", "ログインIDまたはパスワードが正しくありません。"),
	/** 多重登録確認 */
	MULTIPLE_REGISTRATION_CONFIRMATION(200, "4012", "The specified user already exists", "指定された会員はすでに存在しています。"),
	/** メールアドレス */
	MAIL_ADDRESS(200, "4013", "The specified e-mail already exists", "指定されたメールアドレスはすでに存在します。"),
	/** webログインID */
	WEB_LOGIN_ID(200, "4014", "The specified web login id already exists", "指定されたWEBログインIDはすでに存在します。"),
	/** 権限確認 */
	AUTHORITY_CONFIRMATION(403, "4015", "The specified salesPlayerCode and token key do not match.",
			"指定された販売事業者システムコードとトークンキーが一致しません。"),
	/** トークン認証 */
	TOKEN_AUTHORIZATION(401, "4016", "The token key is invalid", "トークンキーが正しくありません。"),
	/** メンテナンス中 */
	MAINTENANCE(503, "4017", "Service Unavailable.", "メンテナンス中です。"),
	/** 存在確認 */
	NOT_EXIST(200, "4018", "The specified {0} does not exist.", "指定された{0}は存在しません。"),
	/** 整合性確認 */
	NOT_MATCH_TEMP_SEAT_CODE(200, "4019", "TemporarySeatCode are not match.", "仮座席コードが一致しません。"),
	/** 制限オーバー */
	OVER_LIMIT_PROVISIONAL_RESERVATION_LIMIT(200, "4020",
			"The number of issued provisionalReservationToken exceeded the limit.", "仮押さえトークンの発行数が制限を超えました。"),
	/** 仮押さえトークン無効 */
	OVER_LIMIT_PROVISIONAL_RESERVATION_INVALID(200, "4021",
			"The specified provisionalReservationToken is invalid.", "指定された仮押さえトークンが無効です。"),
	/** トークン確定処理中 */
	OVER_LIMIT_PROVISIONAL_RESERVATION_SETTLEMENT(200, "4022",
			"The specified provisionalReservationToken is in confirming.", "指定された仮押さえトークンが確定処理中です。"),
	/** トークン異常状態 */
	OVER_LIMIT_PROVISIONAL_RESERVATION_STATE(200, "4023",
			"The specified provisionalReservationToken is in an abnormal state.", "指定された仮押さえトークンが異常状態です。"),
	/** 整合性確認 */
	NOT_MATCH_T_ENTRY_CODE(200, "4024", "tEntryCode between purchaseSettlement and purchaseTEntry is not match.",
			"購入決済情報と購入受付の受付システムコード(tEntryCode)が一致しません。"),
	/** 整合性確認 */
	NOT_MATCH_SALES_PLAYER_SEAT_ID(200, "4025",
			"salesPlayerSeatId between purchaseContent and purchaseReceive is not match.",
			"購入内容と購入受取情報の販売事業者座席ID(salesPlayerSeatId)が一致しません。"),
	/** 確定処理中→仮押さえに変更 */
	INVALID_SEATSTATUS(401, "4026", "The specified seatStatus is invalid.", "指定された座席状態は無効です。"),
	/** 想定外の値 */
	UNEXPECTED_VALUE(200, "4027", "{0} is not {1}.", "{0}が{1}ではありません。"),
	/** 期間外 */
	OUT_OF_TERM(200, "4028", "{0} is not within the time period.", "{0}が期間内ではありません。"),
	/**通路指定不可 */
	NOT_AISLE_SEAT(200, "4028", "AisleSeat flag is not avalirable for this tEntry.", "この受付では通路指定は利用できません。"),
	/**データ不整合 */
	DATA_INCONSISTENCY(500, "4030", "Data inconsistency exists in {0}.", "データ不整合が{0}に存在します。");

	/**
	 * APIエラーを定義するEnumです。メッセージはMessageFormat対応形式です。
	 *
	 * @param statusCode
	 * @param code
	 * @param englishMessage
	 * @param japaneseMessage
	 */
	private ErrorResponse(final int statusCode, final String code, final String englishMessage,
			final String japaneseMessage) {
		this.statusCode = statusCode;
		this.code = code;
		this.englishMessage = englishMessage;
		this.japaneseMessage = japaneseMessage;
	}

	/**
	 * ステータスコード
	 */
	private final int statusCode;

	/**
	 * エラーコード
	 */
	private final String code;
	/**
	 * 英語エラーメッセージ
	 */
	private final String englishMessage;
	/**
	 * 日本語エラーメッセージ
	 */
	private final String japaneseMessage;

	public int getStatusCode() {
		return statusCode;
	}

	public String getCode() {
		return code;
	}

	public String getEnglishMessage() {
		return englishMessage;
	}

	public String getJapaneseMessage() {
		return japaneseMessage;
	}

}
