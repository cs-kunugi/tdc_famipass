/*
 * Copyright.
 */
package jp.co.bbtower.tdc.error;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;

/**
 * エラー情報リソースクラスです。
 *
 * エラー情報としてtDC在庫共有システム WEBAPI仕様書を元とした構造を持ちます。
 *
 * <pre>
 * {
 *     "errors" : [
 *         {"code": xxxx, "message": xxxx},
 *         {"code": xxxx, "message": xxxx},
 *         ...
 *     ]
 * }
 * </pre>
 *
 * @author cs
 */
public final class ApiError implements Serializable {

	/**
	 * エラーの構造クラス。
	 */
	@Data
	private class ErrorStructure implements Serializable {

		/**
		 * エラーコードを格納します。
		 */
		private final String code;

		/**
		 * エラーメッセージを格納します。
		 */
		private final String message;
	}

	/**
	 * エラーを格納します。
	 */
	@Getter
	private final List<ErrorStructure> errors;

	/**
	 * エラー情報リソースを作成します。
	 *
	 */
	public ApiError() {

		this.errors = new ArrayList<>();
	}

	/**
	 * エラー情報リソースを作成します。
	 *
	 * @param code エラーコード
	 * @param message エラーメッセージ
	 */
	public ApiError(String code, String message) {

		this.errors = new ArrayList<>();

		put(code, message);
	}

	/**
	 * エラー情報を追記します。
	 *
	 * @param code エラーコード
	 * @param message エラーメッセージ
	 */
	public void put(String code, String message) {
		this.errors.add(new ErrorStructure(code, message));
	}
}
