/*

 * Copyright.
 */
package jp.co.bbtower.tdc.error;

import org.springframework.data.redis.ClusterRedirectException;
import org.springframework.data.redis.ClusterStateFailureException;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.RedisSystemException;
import org.springframework.data.redis.TooManyClusterRedirectionsException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * Redis周りの例外ハンドラークラスです。
 *
 * @author cs
 */
@ControllerAdvice
public class RedisExceptionHandler extends ApiExceptionHandler {

	/**
	 * Redis例外ハンドラーメソッドです。
	 *
	 * @param ex
	 * @param request
	 * @return ApiExceptionHandler#handleExceptionInternal() の戻り値。
	 */
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, WebRequest request) {

		String code = ex.getClass().getName();

		return super.handleExceptionInternal(ex, ex.getStackTrace(), new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	/**
	 * ClusterRedirectException の例外ハンドラーメソッドです。
	 *
	 * @param ex
	 *            ClusterRedirectException
	 * @param request
	 * @return
	 */
	@ExceptionHandler(ClusterRedirectException.class)
	protected ResponseEntity<Object> handleClusterRedirectException(ClusterRedirectException ex, WebRequest request) {

		return handleExceptionInternal(ex, request);

	}

	/**
	 * ClusterStateFailureException の例外ハンドラーメソッドです。
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(ClusterStateFailureException.class)
	protected ResponseEntity<Object> handleClusterStateFailureException(ClusterStateFailureException ex,
			WebRequest request) {

		return handleExceptionInternal(ex, request);

	}

	/**
	 * RedisConnectionFailureException の例外ハンドラーメソッドです。
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(RedisConnectionFailureException.class)
	protected ResponseEntity<Object> handleRedisConnectionFailureException(RedisConnectionFailureException ex,
			WebRequest request) {

		return handleExceptionInternal(ex, request);

	}

	/**
	 * RedisSystemException の例外ハンドラーメソッドです。
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(RedisSystemException.class)
	protected ResponseEntity<Object> handleRedisSystemException(RedisSystemException ex, WebRequest request) {

		return handleExceptionInternal(ex, request);

	}

	/**
	 * TooManyClusterRedirectionsException の例外ハンドラーメソッドです。
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(TooManyClusterRedirectionsException.class)
	protected ResponseEntity<Object> handleTooManyClusterRedirectionsException(TooManyClusterRedirectionsException ex,
			WebRequest request) {

		return handleExceptionInternal(ex, request);

	}

}
