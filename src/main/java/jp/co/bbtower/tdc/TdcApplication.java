/*
 * Copyright.
 */
package jp.co.bbtower.tdc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

import jp.co.bbtower.tdc.database.CommonDSProperties;

/**
 * アプリケーション起動クラスです。
 *
 * @author cs
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableConfigurationProperties({
		DataSourceProperties.class,
		CommonDSProperties.class })
@EnableScheduling
@EnableCaching
public class TdcApplication {

	/**
	 * エントリーポイント。
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(TdcApplication.class, args);
	}
}
