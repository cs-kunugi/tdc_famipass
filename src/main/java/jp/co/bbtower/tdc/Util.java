package jp.co.bbtower.tdc;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * 汎用的な処理を行うクラスです。
 *
 */
public class Util {

	/**
	 * 例：2000/01/01 13:00:00
	 */
	public static final String DATE_FORMAT_1 = "yyyy/MM/dd HH:mm:ss";
	/**
	 * 例：2014-10-10 13:50:40
	 */
	public static final String DATE_FORMAT_2 = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 例：2014-10-10
	 */
	public static final String DATE_FORMAT_3 = "yyyy-MM-dd";
	/**
	 * 例：20141010
	 */
	public static final String DATE_FORMAT_4 = "yyyyMMdd";

	/**
	 * 文字列日付を指定フォーマットでDateに変換します。
	 *
	 * @param strDate 変換対象
	 * @param formatStr フォーマット
	 * @return 変換後日付。変換できなかった場合はnullを返します。
	 */
	public static Date changeStrToDate(String strDate, String formatStr) {
		Date result = null;
		SimpleDateFormat sdFormat = null;
		try {
			sdFormat = new SimpleDateFormat(formatStr);
			result = sdFormat.parse(strDate);
			return result;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 文字列日付が指定フォーマットかをチェックします。
	 *
	 * @param strDate 判定対象
	 * @param formatStr フォーマット
	 * @return 変換可能な場合はtrue。
	 */
	public static boolean checkStrToDate(String strDate, String formatStr) {
		try {
			new SimpleDateFormat(formatStr).parse(strDate);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	/**
	 * 日付を指定フォーマットで文字列に変換します。
	 *
	 * @param date 日付
	 * @param formatStr フォーマット
	 * @return 変換後文字列。
	 */
	public static String changeDateToStr(Date date, String formatStr) {
		return new SimpleDateFormat(formatStr).format(date);
	}

	/**
	 * nulを空文字に変換します。
	 *
	 * @param str 変換対象
	 * @return nullの場合は空文字、それ以外の場合は入力文字列を返します。
	 */
	public static String nullToEmpty(String str) {
		if (str == null) {
			return "";
		} else {
			return str;
		}
	}

	/**
	 * nulを空文字に変換します。
	 *
	 * @param str 変換対象
	 * @return 空文字の場合はnull、それ以外の場合は入力文字列を返します。
	 */
	public static String emptyToNull(String str) {
		if (str == null) {
			return null;
		}
		if (str.length() == 0) {
			return null;
		} else {
			return str;
		}
	}

	/**
	 * Listがnullもしくは空であるか検査します。
	 *
	 * @param list 検査対象
	 * @return nullもしくは空の場合true。
	 */
	public static boolean isEmptyOrNullList(List<?> list) {
		if (list == null) {
			return true;
		} else if (list.size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Setがnullもしくは空であるか検査します。
	 *
	 * @param list 検査対象
	 * @return nullもしくは空の場合true。
	 */
	public static boolean isEmptyOrNullSet(Set<?> list) {
		if (list == null) {
			return true;
		} else if (list.size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * Stringがnullもしくは空文字であるか検査します。
	 *
	 * @param str 検査対象
	 * @return nullもしくは空の場合true。
	 */
	public static boolean isEmptyOrNullStr(String str) {
		if (str == null) {
			return true;
		} else if (str.length() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * メールアドレスを整形します。
	 *
	 * @param email
	 *            メールアドレス
	 * @return 整形されたメールアドレス
	 */
	public static String fixMail(final String email) {
		return email.toLowerCase();
	}

	/**
	 * GMailのメールアドレスを整形します。
	 *
	 * GMail以外の整形は行いません。
	 *
	 * @param email
	 *            メールアドレス
	 * @return Gmail以外: そのまま返却します。 Gmail: ローカル部にある.は除去 ローカル部にある+以下は+を含み除去
	 *         ドメイン部はgmail.comへ置換
	 */
	public static String fixGmail(final String email) {

		if (isGmail(email)) {

			final StringBuilder sb = new StringBuilder(email.length());
			final String at = "@";

			final String[] tmp = email.split(at);
			final String local = tmp[0];

			sb.append(local.replaceAll("\\+.+", "").replaceAll("\\.", "")).append(at).append("gmail.com");

			return sb.toString();
		}
		return email;
	}

	/**
	 * GMailのメールアドレスかどうか判断します。
	 *
	 * domain部が
	 * <ul>
	 * <li>gmail.com</li>
	 * <li>googlemail.com</li>
	 * </ul>
	 * のどちらかであればGMailと判断します。
	 *
	 * @param email
	 *            メールアドレス
	 * @return true: GMailのメールアドレス / false: GMail以外のメールアドレス
	 */
	public static boolean isGmail(final String email) {
		return email.matches("[^@]+@(g|google)mail\\.com");
	}

	/**
	 * nullまたは0文字であるときにtrueを返します。
	 *
	 * @param str 判定対象
	 * @return
	 */
	public static boolean isEmpty(String str) {
		if (str == null) {
			return true;
		} else {
			return str.isEmpty();
		}
	}

	/**
	 * 整数（[0-9]*）であるときにtrueを返します。
	 * @param val 判定対象
	 * @return
	 */
	public static boolean isInteger(String val) {
		return matches(val, "[0-9]*");
	}

	/**
	 * 正規表現のチェック結果（Matcher.matches()）を返します。
	 * @param val 判定対象
	 * @param regex 正規表現
	 * @return
	 */
	public static boolean matches(String val, String regex) {
		Pattern p = Pattern.compile(regex);
		Matcher m1 = p.matcher(val);
		return m1.matches();
	}

	/**
	 * 暗号化します。
	 *
	 * @param pass パスワード
	 * @param memberEntryDate ユーザ登録日
	 * @return
	 */
	public static String encodePassword(String pass, Date memberEntryDate) {
		String outStr = pass;
		StringBuilder sb = new StringBuilder();
		String saltBase = Util.changeDateToStr(memberEntryDate, "YYYYMMDD");// yyyyMMddではない

		// salt生成
		String salt = saltBase;
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// ベタ書きなので発生し得ない。
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		md.update(saltBase.getBytes());
		byte[] hashBytes = md.digest();
		int[] hashInts = new int[hashBytes.length];
		for (int i = 0; i < hashBytes.length; i++) {
			hashInts[i] = hashBytes[i] & 0xff;
			if (hashInts[i] <= 15) {
				sb.append("0");
			}
			sb.append(Integer.toHexString(hashInts[i]));
		}
		salt = sb.toString();

		// 中間処理
		outStr += salt;

		// HMAC
		SecretKeySpec sk = new SecretKeySpec(salt.getBytes(), "HmacSHA256");
		Mac mac = null;
		try {
			mac = Mac.getInstance("HmacSHA256");
			mac.init(sk);
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			// ベタ書きなので発生し得ない。
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		byte[] mac_bytes = mac.doFinal(outStr.getBytes());
		sb = new StringBuilder(2 * mac_bytes.length);
		for (byte b : mac_bytes) {
			sb.append(String.format("%02x", b & 0xff));
		}
		outStr = sb.toString();

		return outStr;
	}

	public static <T> List<List<T>> divide(List<T> origin, int size) {
		if (origin == null || origin.isEmpty() || size <= 0) {
			return Collections.emptyList();
		}

		int block = origin.size() / size + (origin.size() % size > 0 ? 1 : 0);

		return IntStream.range(0, block)
				.boxed()
				.map(i -> {
					int start = i * size;
					int end = Math.min(start + size, origin.size());
					return origin.subList(start, end);
				})
				.collect(Collectors.toList());
	}

}
