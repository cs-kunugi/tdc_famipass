package jp.co.bbtower.tdc;

import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 共通的なログ出力を行うクラスです。
 *
 */
@Aspect
@Component
public class TraceInterceptor {

	/**
	 * ロガー。
	 */
	private static Logger logger = LoggerFactory.getLogger(TraceInterceptor.class);

	/**
	 * 出力ポイントを設定します。
	 */
	@Pointcut("execution(public * jp.co.bbtower.tdc..*.*(..))")
	public void pointCut() {
	}

	/**
	 * メソッドの実行前に行う処理です。
	 *
	 * @param joinPoint
	 */
	@Before("pointCut()")
	public void invokeBefore(JoinPoint joinPoint) {
		log(joinPoint, "start");
	}

	/**
	 * メソッドの実行後（正否を問わず）に行う処理です。
	 *
	 * @param joinPoint
	 */
	@After("pointCut()")
	public void invokeAfter(JoinPoint joinPoint) {
		log(joinPoint, "end");
	}

	// TODO ログレベルなどが未定のためprintfで出力中。決定次第loggerに変更すること。
	/**
	 * ログを出力します。<br/>
	 * [E M D HH:mm:dd JST yyyy]クラス名.メソッド名(引数,引数...)+messageの形式で出力
	 *
	 * @param joinPoint
	 * @param message
	 *            追加表示文言。
	 */
	private void log(JoinPoint joinPoint, String message) {
		String date = new Date().toString();

		String clazzNm = joinPoint.getTarget().getClass().getSimpleName();
		String methodNm = joinPoint.getSignature().getName();
		Object[] argArr = joinPoint.getArgs();
		methodNm += "(";
		if (argArr.length > 0) {
			String args = "";
			for (Object obj : argArr) {
				String arg = "";
				if (obj == null) {
					arg = "null";
				} else {
					arg = obj.toString();
				}
				args += arg + ",";
			}
			args = args.substring(0, args.length() - 1);
			methodNm += args;
		}
		methodNm += ")";
		System.out.print("[" + date + "]" + clazzNm + "." + methodNm + message + "\n");
	}

}