package jp.co.bbtower.tdc;

/**
 * 共通定数クラスです。
 *
 */
public class Const {

	/**
	 * 権限レベル 票券
	 */
	public static final int AUTHORITY_LEVEL_VOTES_TICKETS = 1;

	/**
	 * 権限レベル 販売ASP
	 */
	public static final int AUTHORITY_LEVEL_SALES_ASP = 2;

	/**
	 * 権限レベル プレイガイド
	 */
	public static final int AUTHORITY_LEVEL_PLAY_GUIDE = 3;
}
