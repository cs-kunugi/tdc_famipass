package jp.co.bbtower.tdc.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import jp.co.bbtower.tdc.dto.FamipassProxyDto;

/**
 * ファミパス送信インターフェースです。
 * @author nouser
 *
 */
@Service
public interface FamipassProxyService {
	/**
	 * ファミパスへのリクエストを中継し、ファミパスの処理結果を同期で返却します。
	 */
	public ResponseEntity<?> relayFamipass(FamipassProxyDto dto);
}
