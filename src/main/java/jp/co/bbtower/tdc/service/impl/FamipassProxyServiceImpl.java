package jp.co.bbtower.tdc.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.famima.cpc.fds.client.BillingRequestData;

import jp.co.bbtower.tdc.dto.FamipassProxyDto;
import jp.co.bbtower.tdc.dto.FamipassProxyQueryDto;
import jp.co.bbtower.tdc.service.FamipassProxyService;
import lombok.Getter;
import lombok.Setter;

/**
 * ファミパス送信サービス実装クラスです。
 * @author nouser
 *
 */
@Service
@Transactional(readOnly = true)
@ConfigurationProperties("famipass.proxy")
public class FamipassProxyServiceImpl implements FamipassProxyService {

	/**
	 * ファミパスログインID.
	 */
	@Getter
	@Setter
	private String loginUserId;

	/**
	 * ファミパスログインパスワード.
	 */
	@Getter
	@Setter
	private String loginPassword;

	/**
	 * 送信先URL.
	 */
	@Getter
	@Setter
	private String ticket;

	/**
	 * ロガー。
	 */
	private static Logger logger = LoggerFactory.getLogger(FamipassProxyServiceImpl.class);

	/**
	 * ファミパス送信API
	 */
	private static final int API_SEND_FAMIPASS = 1;
	/**
	 * ファミパス結果取得API
	 */
	private static final int API_GET_RESULT = 2;
	/**
	 * ファミパスマスタ送信API
	 */
	private static final int API_SEND_MASTER_FAMIPASS = 3;
	/**
	 * ファミパスプレビューAPI
	 */
	private static final int API_PREVIEW = 4;

	@Override
	public ResponseEntity<?> relayFamipass(FamipassProxyDto dto) {
		List<BillingRequestData> querylist = new ArrayList<>();
		for (FamipassProxyQueryDto query : dto.getQuery()) {
			BillingRequestData br = new BillingRequestData();
			br.setDateOfExpiry(query.getDateOfExpiry());
			br.setDateOfTicketEnd(query.getDateOfTicketEnd());
			br.setDateOfTicketStart(query.getDateOfTicketStart());
			br.setFree1(query.getFree1());
			br.setFree2(query.getFree2());
			br.setFree3(query.getFree3());
			br.setFree4(query.getFree4());
			br.setFree5(query.getFree5());
			br.setFree6(query.getFree6());
			br.setFree7(query.getFree7());
			br.setFree8(query.getFree8());
			br.setInquiryEndTime(query.getInquiryEndTime());
			br.setInquiryFree1(query.getInquiryFree1());
			br.setInquiryFree2(query.getInquiryFree2());
			br.setInquiryName(query.getInquiryName());
			br.setInquiryStartTime(query.getInquiryStartTime());
			br.setInquiryTel(query.getInquiryTel());
			br.setKana(query.getKana());
			br.setMailAddress(query.getMailAddress());
			br.setMembershipNo(query.getMembershipNo());
			br.setName(query.getName());
			br.setNotice1(query.getNotice1());
			br.setNotice2(query.getNotice2());
			br.setNotice3(query.getNotice3());
			br.setNotice4(query.getNotice4());
			br.setNotice5(query.getNotice5());
			br.setNotice6(query.getNotice6());
			br.setNotice7(query.getNotice7());
			br.setNotice8(query.getNotice8());
			br.setNotice9(query.getNotice9());
			br.setNotice10(query.getNotice10());
			br.setPayment(query.getPayment());
			br.setPhoneNo(query.getPhoneNo());
			br.setReceiptNo(query.getReceiptNo());
			br.setRegistType(query.getRegistType());
			br.setReservationNo(query.getReservationNo());
			br.setServiceType(query.getServiceType());
			br.setLoginUserId(loginUserId);
			br.setPassword(loginPassword);
			querylist.add(br);
		}

		// TODO 自動生成されたメソッド・スタブ
		switch (dto.getApiCode()) {
		//1	ファミパス送信API
		case API_SEND_FAMIPASS:
			return send(dto);
		//2	ファミパス結果取得API
		case API_GET_RESULT:
			return getResult(dto);
		//3	ファミパスマスタ送信API
		case API_SEND_MASTER_FAMIPASS:
			return sendMaster(dto);
		//4	ファミパスプレビューAPI
		case API_PREVIEW:
			return preview(dto);
		}

		return null;
	}

	private ResponseEntity<?> send(FamipassProxyDto dto) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	private ResponseEntity<?> getResult(FamipassProxyDto dto) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	private ResponseEntity<?> sendMaster(FamipassProxyDto dto) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	private ResponseEntity<?> preview(FamipassProxyDto dto) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
}
