package jp.co.bbtower.tdc;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.embedded.jetty.JettyServerCustomizer;
import org.springframework.boot.web.embedded.jetty.JettyServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * Jettyのロギング設定を実施するクラスです。
 *
 * @author cs
 */
@Configuration
@ConfigurationProperties("logging")
public class JettyLoggingConfigure {

	/**
	 * ログのパス。
	 */
	@Getter
	@Setter
	private String path;

	/**
	 * ログ出力用。
	 *
	 * @return EmbeddedServletContainerFactory
	 */
	@Bean
	public ServletWebServerFactory jettyEmbeddedServletContainerFactory() {

		JettyServletWebServerFactory factory = new JettyServletWebServerFactory();

		factory.addServerCustomizers((JettyServerCustomizer) (Server server) -> {
			HandlerCollection handlers = new HandlerCollection();
			RequestLogHandler requestLogHandler = new RequestLogHandler();
			for (Handler handler : server.getHandlers()) {
				handlers.addHandler(handler);
			}

			NCSARequestLog requestLog = new NCSARequestLog(
					(this.getPath() != null ? this.getPath() + "/" : "") + "jetty-yyyy_mm_dd.access.log");
			requestLog.setRetainDays(90);
			requestLog.setAppend(true);
			requestLog.setExtended(false);
			requestLog.setLogTimeZone("JST");
			requestLogHandler.setRequestLog(requestLog);
			handlers.addHandler(requestLogHandler);
			server.setHandler(handlers);
		});

		return factory;
	}
}
