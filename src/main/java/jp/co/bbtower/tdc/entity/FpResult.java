package jp.co.bbtower.tdc.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import lombok.Data;

/**
 * ファミパス連携テーブルエンティティです。
 */
@Data
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE)
public class FpResult {
	/** ファミパス応答システムコード。 */
	private Integer fpResultCode;

	/** 販売事業者システムコード。 */
	private Integer salesPlayerCode;

	/** 興行主システムコード。 */
	private Integer organizerCode;

	/** ファミパス収納番号。 */
	private String receiptNo;

	/** 通知区分。 */
	private Integer status;

	/** 入金日時。 */
	private Integer receiptDate;

	/** 収納金額。 */
	private Integer payment;

	/** 予約番号。 */
	private String reservationNo;

	/** 会員番号。 */
	private String membershipNo;

	/** 通知日時。 */
	private Date responseAt;

	/** チェックしたかどうか。 */
	private Integer checked;

	/** チェック日時。 */
	private Date checkedAt;
}
