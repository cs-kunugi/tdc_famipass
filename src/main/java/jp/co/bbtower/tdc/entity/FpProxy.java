package jp.co.bbtower.tdc.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import lombok.Data;

/**
 * ファミパス連携テーブルエンティティです。
 */
@Data
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE)
public class FpProxy {

	/** ファミパス通信システムコード。 */
	private Integer fpProxyCode;

	/** 販売事業者システムコード。 */
	private Integer salesPlayerCode;

	/** 興行主システムコード。 */
	private Integer organizerCode;

	/** ファミパス収納番号。 */
	private String receiptNo;

	/** 受付日時。 */
	private Date timestamp;

}
