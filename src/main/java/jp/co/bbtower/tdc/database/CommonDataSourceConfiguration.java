package jp.co.bbtower.tdc.database;

import java.util.Optional;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

/**
 * 共通DBデータソース設定クラスです。
 *
 * @author cs
 */
@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = "jp.co.bbtower.tdc.repository.common", sqlSessionTemplateRef = "commonSqlSessionTemplate")
public class CommonDataSourceConfiguration implements TransactionManagementConfigurer {

	/**
	 * トランザクションマネージャー名
	 */
	public static final String TX_MANAGER = "commonTransactionManager";

	/**
	 * 共通DBデータソースプロパティ。
	 */
	@Autowired
	private CommonDSProperties commonDSProperties;

	/**
	 * コンテキスト。
	 */
	@Autowired
	private ApplicationContext context;

	/**
	 * dataSourceを取得します。
	 *
	 * @return commonDataSource
	 */
	@Bean(name = { "dataSource", "commonDataSource" })
	@Autowired
	@Primary
	public DataSource dataSource() {
		return commonDSProperties.getDatasource();
	}

	/**
	 * sqlSessionTemplateを取得します。
	 *
	 * @param dataSource commonDataSource
	 * @return commonSqlSessionTemplate
	 * @throws Exception
	 */
	@Bean(name = "commonSqlSessionTemplate")
	public SqlSessionTemplate sqlSessionTemplate(@Qualifier("commonDataSource") DataSource dataSource)
			throws Exception {

		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource);

		// MyBatis のコンフィグレーションファイル
		Optional.ofNullable(context.getEnvironment().getProperty("mybatis.config-location"))
				.map(config -> context.getResource(config))
				.ifPresent(resource -> bean.setConfigLocation(resource));

		return new SqlSessionTemplate(bean.getObject());
	}

	/**
	 * DBのイニシャライズを実施します。
	 *
	 * @param dataSource commonDataSource
	 * @return commonDataSourceInitializer
	 */
	@Bean(name = "commonDataSourceInitializer")
	public DataSourceInitializer dataSourceInitializer(@Qualifier("commonDataSource") DataSource dataSource) {
		DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
		dataSourceInitializer.setDataSource(dataSource);
		ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();

		Optional.ofNullable(commonDSProperties.getDatasource().getSchema())
				.map(schema -> context.getResource(schema))
				.ifPresent(resource -> databasePopulator.addScript(resource));
		Optional.ofNullable(commonDSProperties.getDatasource().getData())
				.map(data -> context.getResource(data))
				.ifPresent(resource -> databasePopulator.addScript(resource));

		dataSourceInitializer.setDatabasePopulator(databasePopulator);
		dataSourceInitializer.setEnabled(true);

		return dataSourceInitializer;
	}

	/**
	 * トランザクションマネージャーを作成します。
	 *
	 * @return PlatformTransactionManager
	 */
	@Bean(name = TX_MANAGER)
	@Autowired
	protected PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}

	@Override
	public PlatformTransactionManager annotationDrivenTransactionManager() {
		return transactionManager();
	}
}
