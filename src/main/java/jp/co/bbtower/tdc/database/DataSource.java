package jp.co.bbtower.tdc.database;

import lombok.Getter;
import lombok.Setter;

/**
 * データソース拡張クラスです。
 *
 * @author cs
 */
@Getter
@Setter
public class DataSource extends org.apache.tomcat.jdbc.pool.DataSource {
	private String schema;
	private String data;
}
