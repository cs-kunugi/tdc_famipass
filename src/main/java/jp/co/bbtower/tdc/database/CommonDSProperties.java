package jp.co.bbtower.tdc.database;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * 共通DBデータソースプロパティクラスです。
 *
 * @author cs
 */
@ConfigurationProperties("spring.common")
public class CommonDSProperties {

	/**
	 * データソース。
	 */
	@Getter
	@Setter
	@NestedConfigurationProperty
	private DataSource datasource;
}
